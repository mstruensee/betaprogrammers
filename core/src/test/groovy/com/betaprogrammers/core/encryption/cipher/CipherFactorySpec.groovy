package com.betaprogrammers.core.encryption.cipher

import spock.lang.Specification
import spock.lang.Subject

class CipherFactorySpec extends Specification {

    @Subject
    CipherFactory sut = new CipherFactory()

    def "default trait methods and variables"() {
        setup:
        def key = "_"
        def decrypted = "_"
        key = randomKey(16)
        decrypted = randomKey(16)

        when:
        def encrypted = sut.encrypt(key, decrypted)
        println "key: ${key}\ndecrypted: ${decrypted}\nencrypted: ${encrypted}"

        then:
        sut.decrypt(key, encrypted) == old(decrypted)
    }

    def randomKey(length) {
        def generator = { alphabet, n ->
            new Random().with {
                (1..n).collect { alphabet[nextInt(alphabet.length())] }.join()
            }
        }
        def key = ''
        length.times {
            def random = new Random().nextInt(3) + 1
            if (random == 1) {
                key += generator((('A'..'Z')).join(), 1)
            } else if (random == 2) {
                key += generator((('a'..'z')).join(), 1)
            } else {
                key += generator((('0'..'9')).join(), 1)
            }
        }
        new StringBuilder(key).toString()
    }

}
