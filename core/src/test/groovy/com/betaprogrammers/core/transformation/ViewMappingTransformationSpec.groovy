package com.betaprogrammers.core.transformation

import groovy.text.GStringTemplateEngine
import groovy.text.Template
import spock.lang.Ignore
import spock.lang.Specification

class ViewMappingTransformationSpec extends Specification {
    @Ignore
    def 'stop point'() {
        setup:
        File physicalFile = new File(new File("").canonicalFile.toString() + "/src/main/groovy/com/betaprogrammers/home/controller/HomeController.groovy")
        final Template template = new GStringTemplateEngine().createTemplate(physicalFile?.text)
        def instance = new GroovyClassLoader().parseClass(template.make([:]).toString()).newInstance()

        expect:
        instance
    }

//    def "test"() {
//        when:
//        def instance = new GroovyClassLoader().parseClass(template.make([:]).toString()).newInstance()
//        then:
//        instance
//    }

}
