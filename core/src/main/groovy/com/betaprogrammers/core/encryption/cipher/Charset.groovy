package com.betaprogrammers.core.encryption.cipher

enum Charset {
    ASCII("US-ASCII"),
    ISO("ISO-8859-1"),
    UTF8("UTF-8"),
    UTF16("UTF-16"),
    UTF16BE("UTF-16BE"),
    UTF16LE("UTF-16LE")

    String value

    Charset(value) {
        this.value = value
    }

    String getValue() {
        value
    }

    @Override
    String toString() {
        value
    }

}
