package com.betaprogrammers.core.encryption.cipher

enum Padding {
    NOPADDING, PKCS5PADDING, ISO10126PADDING
}