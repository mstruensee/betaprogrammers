package com.betaprogrammers.core.encryption.cipher

import org.apache.commons.codec.binary.Base64

import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class CipherFactory {
    def charset = Charset.UTF8;
    def mode = Mode.CBC;
    def algorithm = Algorithm.AES;
    def padding = Padding.PKCS5PADDING;

    def encrypt(key, decrypted) throws Exception {
        def cipher = Cipher.getInstance(type())
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key.getBytes(), algorithm.toString()), new IvParameterSpec(new byte[16]))
        new String(new Base64().encode(cipher.doFinal(decrypted.getBytes(charset.toString()))))
    }

    def decrypt(key, encrypted) throws Exception {
        def encryptedBytes = new Base64().decode(encrypted.getBytes(charset.toString()))
        def cipher = Cipher.getInstance(type())
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key.getBytes(charset.toString()), algorithm.toString()), new IvParameterSpec(new byte[16]))
        def decryptedBytes = cipher.doFinal(encryptedBytes)
        new String(decryptedBytes, charset.toString())
    }

    def type() {
        "${algorithm}/${mode}/${padding}"
    }
}