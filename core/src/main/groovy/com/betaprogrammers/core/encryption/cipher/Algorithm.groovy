package com.betaprogrammers.core.encryption.cipher

enum Algorithm {
    AES, DES, DESede, RSA
}