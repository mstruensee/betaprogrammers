package com.betaprogrammers.core.liveconfig.service

import com.betaprogrammers.core.liveconfig.domain.Property
import com.betaprogrammers.core.liveconfig.exception.LiveConfigException
import com.betaprogrammers.core.liveconfig.repository.LiveConfigRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.core.io.support.ResourcePropertySource
import org.springframework.stereotype.Service

@Service
class LiveConfigService {
    @Autowired
    ApplicationContext applicationContext

    @Autowired
    LiveConfigRepository liveConfigRepository

    def fetchProperty(property, type) {
        def entity = liveConfigRepository.findByName(property)
        "cast${type.name.tokenize(".").last().capitalize()}"(entity?.fetchValue())
    }

    def castInt(value) { value.toInteger() }

    def castInteger(value) { value.toInteger() }

    def castBoolean(value) { value.toBoolean() }

    def castString(value) { value.toString() }

    def castObject(value) { value }

    def fetchOne(entity) {
        def exists = liveConfigRepository.findById(entity.id)
        if (exists) {
            return exists
        }
        throw new LiveConfigException("Entity (${entity.id}) not found.")
    }

    def fetchAll() {
        liveConfigRepository.findAll()
    }

    def update(entity) {
        def exists = fetchOne(entity)
        exists.liveValue = entity.liveValue
        liveConfigRepository.save(exists)
        exists
    }

    // this has to be in the service because the repository is null in the context.
    void loadAtLiveConfigProperties() {
        liveConfigRepository.deleteAll()
        def fields = applicationContext.liveConfigFields()
        fields.each { field ->
            def entity = new Property(name: field)
            applicationContext?.opFilenames?.each { filename ->
                def serverValue = entity.serverValue ?: applicationContext.findPropertyValueInSource(filename, field)
                entity.serverValue = convertOtherVariables(serverValue)
            }
            applicationContext?.environmentFilenames?.each { filename ->
                def environmentValue = entity.environmentValue ?: applicationContext.findPropertyValueInSource(filename, field)
                entity.environmentValue = convertOtherVariables(environmentValue)
            }
            applicationContext?.defaultFilenames?.each { filename ->
                def defaultValue = entity.defaultValue ?: applicationContext.findPropertyValueInSource(filename, field)
                entity.defaultValue = convertOtherVariables(defaultValue)
            }
            liveConfigRepository.save(entity)
        }
    }

    def convertOtherVariables(value) {
        def matcher = (value =~ /\$\{(?<another>.*?)\}/)
        if (matcher.find()) {
            def key = matcher.group("another")
            def sources = applicationContext.environment.propertySources.propertySourceList.findAll { it instanceof ResourcePropertySource }.source.findAll { it.size() }.collect { it[(key)] }
            sources = sources - null //remove nulls
            if (sources.size()) {
                def another = sources.first()//highest by loaded priority

                def entity = liveConfigRepository.findByName(another.replace("\${", "").replace("}", ""))
                if (entity) {
                    another = entity.liveValue
                }
                value = value.replace("\${${key}}", another)

                def more = (value =~ /\$\{(?<another>.*)\}/)
                if (more.find()) {
                    value = convertOtherVariables(value)
                }
            }
        }
        return value
    }


}
