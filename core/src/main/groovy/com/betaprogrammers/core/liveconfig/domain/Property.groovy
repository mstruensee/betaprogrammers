package com.betaprogrammers.core.liveconfig.domain

import javax.persistence.*

@Entity
class Property {

    @Id
    @SequenceGenerator(name = "property_sequence", sequenceName = "hibernate_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "property_sequence")
    Long id

    String name

    String serverValue

    String liveValue

    String environmentValue

    String defaultValue

    def fetchValue() {
        (liveValue ?: (serverValue ?: (environmentValue ?: defaultValue)))
    }
}
