package com.betaprogrammers.core.liveconfig.context

import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext

class LiveConfigContextInitializer implements ApplicationContextInitializer {
    void initialize(ConfigurableApplicationContext applicationContext) {
        if (applicationContext instanceof LiveConfigAnnotationConfigEmbeddedWebApplicationContext) {
            applicationContext.addPropertySources()
        }
    }
}