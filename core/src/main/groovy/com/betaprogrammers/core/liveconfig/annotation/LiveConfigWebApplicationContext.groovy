package com.betaprogrammers.core.liveconfig.annotation

import org.codehaus.groovy.transform.GroovyASTTransformationClass

import java.lang.annotation.Documented
import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target


@Retention(RetentionPolicy.SOURCE)
@Target([ElementType.TYPE])
@Documented
@GroovyASTTransformationClass("com.betaprogrammers.core.liveconfig.transformation.LiveConfigWebApplicationContextTransformation")
@interface LiveConfigWebApplicationContext {
    String displayName()

    String[] configLocation()

    String extension() default "properties"

    String defaultLocation()// default ""

    String[] defaultFilenames()// default ["default", "datasource"]

    String environmentLocation()// default ""

    String[] environmentFilenames()// default ["<environment>"]

    String[] opFilenames()
}