package com.betaprogrammers.core.liveconfig.annotation

import org.codehaus.groovy.transform.GroovyASTTransformationClass

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

@Retention(RetentionPolicy.RUNTIME)
@Target([ElementType.FIELD])
@GroovyASTTransformationClass("com.betaprogrammers.core.liveconfig.transformation.TestTransformation")
@interface Test {
    String value()
}
