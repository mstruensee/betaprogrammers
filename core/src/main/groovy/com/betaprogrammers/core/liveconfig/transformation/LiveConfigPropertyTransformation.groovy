package com.betaprogrammers.core.liveconfig.transformation

import com.betaprogrammers.core.liveconfig.service.LiveConfigService
import org.codehaus.groovy.ast.*
import org.codehaus.groovy.ast.expr.*
import org.codehaus.groovy.ast.stmt.ReturnStatement
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.AbstractASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation
import org.springframework.beans.factory.annotation.Autowired

import java.lang.reflect.Modifier

@GroovyASTTransformation(phase = CompilePhase.SEMANTIC_ANALYSIS)
class LiveConfigPropertyTransformation extends AbstractASTTransformation {
    final static String PROPERTY_GETTER_METHOD_NAME = "fetchProperty"

    @Override
    void visit(ASTNode[] nodes, SourceUnit source) {
        FieldNode fieldNode = nodes.getAt(1)
        ClassNode classNode = fieldNode.declaringClass
        Expression value = nodes.getAt(0).getMember("value")

        autowireLiveConfigService(source, classNode)
        classNode.addMethod(getterMethod(fieldNode, new ArgumentListExpression([value, new ClassExpression(fieldNode.type)])))

        //HACK THAT WILL FORCE GROOVY TO USE THE "GETTER" METHOD ADDED BY THE AST TRANSFORMATION
        fieldNode.rename("_${fieldNode.name}")
    }

    //class Foo {
    //   @Autowired
    //   LiveConfigService LiveConfigService
    //}
    void autowireLiveConfigService(SourceUnit source, ClassNode classNode) {
        importLiveConfigService(source)
        if (!classNode.fields.name.contains(LiveConfigService.simpleName)) {
            FieldNode fieldNode = new FieldNode(LiveConfigService.simpleName, ACC_PUBLIC, new ClassNode(LiveConfigService), classNode, new EmptyExpression())
            fieldNode.addAnnotation new AnnotationNode(new ClassNode(Autowired))
            classNode.addField(fieldNode)
        }
    }

    //import Autowired
    //
    //class Foo{ }
    void importAutowired(SourceUnit source) {
        if (!imported(source, Autowired)) {
            source.AST.addImport(Autowired.simpleName, new ClassNode(Autowired))
        }
    }

    //import LiveConfigService
    //
    //class Foo{ }
    void importLiveConfigService(SourceUnit source) {
        importAutowired(source)
        if (!imported(source, LiveConfigService)) {
            source.AST.addImport(LiveConfigService.simpleName, new ClassNode(LiveConfigService))
        }
    }

    //getField() {
    //     LiveConfigService.PROPERTY_GETTER_METHOD_NAME(Field.name, Field.type)
    //}
    MethodNode getterMethod(FieldNode fieldNode, ArgumentListExpression arguments) {
        new MethodNode("get${fieldNode.name.capitalize()}", Modifier.PUBLIC, fieldNode.type, new Parameter[0], new ClassNode[0],
            new ReturnStatement(
                new MethodCallExpression(new VariableExpression(LiveConfigService.simpleName), PROPERTY_GETTER_METHOD_NAME, arguments)
            )
        )
    }

    Boolean imported(SourceUnit source, Class<?> clazz) {
        source.AST.imports.alias.contains(clazz.simpleName)
    }
}
