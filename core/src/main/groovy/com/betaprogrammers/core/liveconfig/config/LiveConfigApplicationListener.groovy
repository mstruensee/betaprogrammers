package com.betaprogrammers.core.liveconfig.config

import com.betaprogrammers.core.liveconfig.context.LiveConfigAnnotationConfigEmbeddedWebApplicationContext
import com.betaprogrammers.core.liveconfig.service.LiveConfigService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component

@Component
class LiveConfigApplicationListener implements ApplicationListener<ApplicationReadyEvent> {
    @Autowired
    LiveConfigService liveConfigService

    @Override
    void onApplicationEvent(ApplicationReadyEvent event) {
        if (event.applicationContext instanceof LiveConfigAnnotationConfigEmbeddedWebApplicationContext) {
            liveConfigService?.loadAtLiveConfigProperties()
        }
    }
}
