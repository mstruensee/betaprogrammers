package com.betaprogrammers.core.liveconfig.exception

class LiveConfigException extends Exception {
    LiveConfigException(String message) {
        super(message)
    }
}