package com.betaprogrammers.core.liveconfig.config

import com.betaprogrammers.core.liveconfig.exception.LiveConfigException
import groovy.util.logging.Slf4j
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@Slf4j
@ControllerAdvice
class LiveConfigExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(LiveConfigException)
    protected ResponseEntity<Object> handleLiveConfigException(LiveConfigException ex, WebRequest request) {
        log.error("${ex.class.name}: ${ex.message}", ex)
        HttpHeaders headers = new HttpHeaders()
        headers.setContentType(MediaType.APPLICATION_JSON)
        return handleExceptionInternal(ex, ex, headers, HttpStatus.INTERNAL_SERVER_ERROR, request)
    }
}
