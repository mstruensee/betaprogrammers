package com.betaprogrammers.core.liveconfig.repository

import com.betaprogrammers.core.liveconfig.domain.Property
import org.springframework.data.repository.CrudRepository

interface LiveConfigRepository extends CrudRepository<Property, Long> {
    //cannot groovy'tize these methods, must have return type
    Property findById(id)

    Property findByName(name)
}