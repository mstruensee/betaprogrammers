package com.betaprogrammers.core.liveconfig.context

import com.betaprogrammers.core.liveconfig.annotation.LiveConfigProperty
import com.betaprogrammers.core.liveconfig.repository.LiveConfigRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.support.ResourcePropertySource
import org.stringtemplate.v4.ST

class LiveConfigAnnotationConfigEmbeddedWebApplicationContext extends AnnotationConfigEmbeddedWebApplicationContext {
    @Autowired
    LiveConfigRepository liveConfigRepository

    def extension = "properties"
    def defaultLocation = "properties"
    def defaultFilenames = ["default"]
    def environmentLocation = "properties"
    def environmentFilenames = ["<environment>"]
    def opsLocation = System.getProperty("app.config") ?: ""
    def opFilenames = ["application"]

    def addPropertySources() {
        opFilenames.each {
            addPhysicalPropertyFile(it)
        }

        environmentFilenames.each {
            def location = transformEnvironmentVariable(environmentLocation)
            def filename = transformEnvironmentVariable(it)
            addClassPathPropertyFile(location, filename)
        }

        defaultFilenames.each {
            addClassPathPropertyFile(defaultLocation, it)
        }
    }

    def addPhysicalPropertyFile(filename) {
        filename = "${opsLocation}/${filename}.${extension}"
        def file = new File(filename)
        if (file.exists()) {
            environment.propertySources.addLast(new ResourcePropertySource("${file.name}", "file:${file.path}"))
            println "${file.name} phyiscal file loaded."
        } else {
            println "${filename} phyiscal file does not exist, skipping."
        }
    }

    def addClassPathPropertyFile(location, filename) {
        def resource = new ClassPathResource("${location}/${filename}.${extension}")
        if (resource.exists()) {
            environment.propertySources?.addLast(new ResourcePropertySource("${resource.filename}", resource.path))
            println "${resource.filename} classpath file loaded."
        } else {
            throw new FileNotFoundException("${resource.filename} classpath file not found.")
        }
    }

    def environmentFromVMOptions() {
        environment.getProperty("environment", "")
    }

    def transformEnvironmentVariable(template) {
        def st = new ST(template)
        st.add("environment", environmentFromVMOptions())
        st.render()
    }

    def findPropertyValueInSource(source, property) {
        source = transformEnvironmentVariable(source)
        environment.propertySources.propertySourceList.find { it.name == "${source}.${extension}" }?.source?.find { it.key == property }?.value
    }

    def liveConfigFields() {
        def results = []
        beanDefinitionNames.each { beanName ->
            try {
                getBean(beanName).class.declaredFields.each { field ->
                    def annotation = field.getAnnotation(LiveConfigProperty)
                    if (annotation) {
                        results << annotation.value()
                    }
                }
            } catch (e) {
                e
            }
        }
        results.unique()
    }
}
