package com.betaprogrammers.core.liveconfig.transformation

import com.betaprogrammers.core.liveconfig.context.LiveConfigAnnotationConfigEmbeddedWebApplicationContext
import org.codehaus.groovy.ast.*
import org.codehaus.groovy.ast.expr.*
import org.codehaus.groovy.ast.stmt.BlockStatement
import org.codehaus.groovy.ast.stmt.ExpressionStatement
import org.codehaus.groovy.ast.stmt.Statement
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.syntax.SyntaxException
import org.codehaus.groovy.syntax.Token
import org.codehaus.groovy.syntax.Types
import org.codehaus.groovy.transform.AbstractASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation
import org.springframework.web.context.ContextLoaderListener

@GroovyASTTransformation(phase = CompilePhase.SEMANTIC_ANALYSIS)
class LiveConfigWebApplicationContextTransformation extends AbstractASTTransformation {
    final static String ROOT_CONTEXT = "rootContext"

    @Override
    void visit(ASTNode[] nodes, SourceUnit source) {
        AnnotationNode annotations = nodes.getAt(0)

        MethodNode methodNode = nodes[1].methods.find { it.name == "onStartup" }
        if (methodNode) {
            methodNode.code.statements.add(addStatements(annotations))
        } else {
            source.addError(new SyntaxException("${LiveConfigAnnotationConfigWebApplicationContext.simpleName} annotation can only be applied to a child that contains an onStartup method.", 0, 0))
        }
    }

    BlockStatement addStatements(AnnotationNode annotations) {
        List<Statement> statements = []
        statements << rootContext()
        annotations.members.each {
            //method names are named the same as the annotation variable name
            //variables that contain a default value will not be here unless they are set in the annotation
            //due to this we only want to add the "statements" to set them on the context if they are not the defaults
            statements << "${it.key}"(it.value)
        }
        statements << addPropertySources()
        statements << addListener()
        statements << registerShutdownHook()

        return new BlockStatement(scope: new VariableScope(), statements: statements)
    }

    //LiveConfigAnnotationConfigWebApplicationContext rootContext = new LiveConfigAnnotationConfigWebApplicationContext()
    Statement rootContext() {
        return new ExpressionStatement(
            new DeclarationExpression(
                new VariableExpression(ROOT_CONTEXT, new ClassNode(LiveConfigAnnotationConfigEmbeddedWebApplicationContext)),
                Token.newSymbol(Types.EQUAL, 0, 0),
                new ConstructorCallExpression(new ClassNode(LiveConfigAnnotationConfigEmbeddedWebApplicationContext), new ArgumentListExpression())
            )
        )
    }

    //rootContext.displayName = "displayName"
    Statement displayName(Expression displayName) {
        return new ExpressionStatement(
            new BinaryExpression(
                new PropertyExpression(new VariableExpression(ROOT_CONTEXT), "displayName"),
                Token.newSymbol(Types.EQUAL, 0, 0),
                new ConstantExpression(new String(displayName.value))
            )
        )
    }

    //rootContext.configLocations = "configLocation"
    Statement configLocation(Expression configLocation) {
        return new ExpressionStatement(
            new BinaryExpression(
                new PropertyExpression(new VariableExpression(ROOT_CONTEXT), "configLocations"),
                Token.newSymbol(Types.EQUAL, 0, 0),
                new ConstantExpression(new String(configLocation.value))
            )
        )
    }

    //rootContext.configLocations = ["configLocation"]
    Statement configLocation(ListExpression configLocation) {
        return new ExpressionStatement(
            new BinaryExpression(
                new PropertyExpression(new VariableExpression(ROOT_CONTEXT), "configLocations"),
                Token.newSymbol(Types.EQUAL, 0, 0),
                configLocation
            )
        )
    }

    //rootContext.extension = "extension"
    Statement extension(Expression extension) {
        return new ExpressionStatement(
            new BinaryExpression(
                new PropertyExpression(new VariableExpression(ROOT_CONTEXT), "extension"),
                Token.newSymbol(Types.EQUAL, 0, 0),
                new ConstantExpression(new String(extension?.value))
            )
        )
    }

    //rootContext.defaultLocation = "defaultLocation"
    Statement defaultLocation(Expression defaultLocation) {
        return new ExpressionStatement(
            new BinaryExpression(
                new PropertyExpression(new VariableExpression(ROOT_CONTEXT), "defaultLocation"),
                Token.newSymbol(Types.EQUAL, 0, 0),
                new ConstantExpression(new String(defaultLocation.value))
            )
        )
    }

    //rootContext.defaultFilenames = "defaultFilenames"
    Statement defaultFilenames(Expression defaultFilenames) {
        return new ExpressionStatement(
            new BinaryExpression(
                new PropertyExpression(new VariableExpression(ROOT_CONTEXT), "defaultFilenames"),
                Token.newSymbol(Types.EQUAL, 0, 0),
                new ListExpression([defaultFilenames])
            )
        )
    }

    //rootContext.defaultFilenames = ["defaultFilenames"]
    Statement defaultFilenames(ListExpression defaultFilenames) {
        return new ExpressionStatement(
            new BinaryExpression(
                new PropertyExpression(new VariableExpression(ROOT_CONTEXT), "defaultFilenames"),
                Token.newSymbol(Types.EQUAL, 0, 0),
                defaultFilenames
            )
        )
    }

    //rootContext.environmentLocation = "environmentLocation"
    Statement environmentLocation(Expression environmentLocation) {
        return new ExpressionStatement(
            new BinaryExpression(
                new PropertyExpression(new VariableExpression(ROOT_CONTEXT), "environmentLocation"),
                Token.newSymbol(Types.EQUAL, 0, 0),
                new ConstantExpression(new String(environmentLocation.value))
            )
        )
    }

    //rootContext.environmentFilenames = "environmentFilenames"
    Statement environmentFilenames(Expression environmentFilenames) {
        return new ExpressionStatement(
            new BinaryExpression(
                new PropertyExpression(new VariableExpression(ROOT_CONTEXT), "environmentFilenames"),
                Token.newSymbol(Types.EQUAL, 0, 0),
                new ListExpression([environmentFilenames])
            )
        )
    }

    //rootContext.environmentFilenames = ["environmentFilenames"]
    Statement environmentFilenames(ListExpression environmentFilenames) {
        return new ExpressionStatement(
            new BinaryExpression(
                new PropertyExpression(new VariableExpression(ROOT_CONTEXT), "environmentFilenames"),
                Token.newSymbol(Types.EQUAL, 0, 0),
                environmentFilenames
            )
        )
    }

    //rootContext.opFilenames = "opFilenames"
    Statement opFilenames(Expression opFilenames) {
        return new ExpressionStatement(
            new BinaryExpression(
                new PropertyExpression(new VariableExpression(ROOT_CONTEXT), "opFilenames"),
                Token.newSymbol(Types.EQUAL, 0, 0),
                new ListExpression([opFilenames])
            )
        )
    }

    //rootContext.opFilenames = ["opFilenames"]
    Statement opFilenames(ListExpression opFilenames) {
        return new ExpressionStatement(
            new BinaryExpression(
                new PropertyExpression(new VariableExpression(ROOT_CONTEXT), "opFilenames"),
                Token.newSymbol(Types.EQUAL, 0, 0),
                opFilenames
            )
        )
    }

    //rootContext.addPropertySources()
    Statement addPropertySources() {
        return new ExpressionStatement(
            new MethodCallExpression(
                new VariableExpression(ROOT_CONTEXT),
                "addPropertySources",
                new ArgumentListExpression()
            )
        )
    }

    //servletContext.addListener(new ContextLoaderListener(rootContext))
    Statement addListener() {
        return new ExpressionStatement(
            new MethodCallExpression(
                new VariableExpression("servletContext"),
                "addListener",
                new ArgumentListExpression(new ConstructorCallExpression(new ClassNode(ContextLoaderListener), new ArgumentListExpression(new VariableExpression(ROOT_CONTEXT))))
            )
        )
    }

    //rootContext.registerShutdownHook()
    Statement registerShutdownHook() {
        return new ExpressionStatement(
            new MethodCallExpression(
                new VariableExpression(ROOT_CONTEXT),
                "registerShutdownHook",
                new ArgumentListExpression()
            )
        )
    }
}