package com.betaprogrammers.core.liveconfig.transformation

import org.codehaus.groovy.ast.ASTNode
import org.codehaus.groovy.ast.builder.AstBuilder
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.AbstractASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation

@GroovyASTTransformation(phase = CompilePhase.SEMANTIC_ANALYSIS)
class TestTransformation extends AbstractASTTransformation {
    @Override
    void visit(ASTNode[] nodes, SourceUnit source) {
        debug()
    }

    def debug() {
        List<ASTNode> astNodes = new AstBuilder().buildFromString("""
import org.springframework.web.bind.annotation.RequestMapping

@RequestMapping(value = "yyyyyyyy")
class Foo{

@RequestMapping(value = "xxxxxxxxxxxxxxxxxxxxxxxxxxxx")
    def xxxxxxxxxxxxxxxxxxxxxxxxxxxx(){
    [:]
    }

}

""")
        println "here"
    }

}
