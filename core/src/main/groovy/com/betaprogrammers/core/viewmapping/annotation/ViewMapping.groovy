package com.betaprogrammers.core.viewmapping.annotation

import org.codehaus.groovy.transform.GroovyASTTransformationClass

import java.lang.annotation.Documented
import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

/**
 * Indicates the String viewName for new ModelAndView. ("classNode.name/methodNode.name")
 * empty method -> new ModelAndView("classNode.name/methodNode.name", [:])
 * def model = [:] -> new ModelAndView("classNode.name/methodNode.name", model)
 * [:] -> new ModelAndView("classNode.name/methodNode.name", [:])
 *
 * Uses the controllers RequestMapping as its root URL.
 * If controller has no RequestMapping, one will be added as /controllerName (removing the word "Controller")
 *
 * @author Matthew Struensee
 * @since 1.0
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@GroovyASTTransformationClass("com.betaprogrammers.core.viewmapping.transformation.ViewMappingTransformation")
@interface ViewMapping {}