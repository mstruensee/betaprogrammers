package com.betaprogrammers.core.viewmapping.transformation

import com.betaprogrammers.core.viewmapping.service.ViewMappingService
import groovy.util.logging.Slf4j
import org.codehaus.groovy.ast.ASTNode
import org.codehaus.groovy.ast.AnnotationNode
import org.codehaus.groovy.ast.ClassNode
import org.codehaus.groovy.ast.FieldNode
import org.codehaus.groovy.ast.MethodNode
import org.codehaus.groovy.ast.Parameter
import org.codehaus.groovy.ast.VariableScope
import org.codehaus.groovy.ast.expr.ArgumentListExpression
import org.codehaus.groovy.ast.expr.ConstantExpression
import org.codehaus.groovy.ast.expr.ConstructorCallExpression
import org.codehaus.groovy.ast.expr.DeclarationExpression
import org.codehaus.groovy.ast.expr.EmptyExpression
import org.codehaus.groovy.ast.expr.MapExpression
import org.codehaus.groovy.ast.expr.MethodCallExpression
import org.codehaus.groovy.ast.expr.VariableExpression
import org.codehaus.groovy.ast.stmt.BlockStatement
import org.codehaus.groovy.ast.stmt.ReturnStatement
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.AbstractASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView

import java.lang.reflect.Modifier

@GroovyASTTransformation(phase = CompilePhase.SEMANTIC_ANALYSIS)
class ViewMappingTransformation extends AbstractASTTransformation {
    @Override
    void visit(ASTNode[] nodes, SourceUnit source) {
        def node = nodes.getAt(1)
        if (!node instanceof MethodNode) {
            return
        }
        def methodNode = node
        def classNode = methodNode.declaringClass
        def statements = methodNode.code.statements

        //empty method
        //return new ModelAndView("classNode.name/methodNode.name", new Map<String, Object>)
        if (!statements) {
            methodNode.code = returnModelView(classNode, methodNode, new MapExpression())
        } else {
            def last = statements?.last()

            //[:]
            //return new ModelAndView("classNode.name/methodNode.name", [:])
            if (last.expression instanceof MapExpression) {
                methodNode.code.statements.add(returnModelView(classNode, methodNode, last.expression))
            }

            //def model = [:]
            //model.put("foo", "bar")
            //return new ModelAndView("classNode.name/methodNode.name", model)
            else if (last.expression instanceof VariableExpression) {
                methodNode.code.addStatement(returnModelView(classNode, methodNode, last.expression))

            }

            //def model = [:]
            //return new ModelAndView("classNode.name/methodNode.name", model)
            else if (last.expression instanceof DeclarationExpression) {
                methodNode.code.statements.add(returnModelView(classNode, methodNode, last.expression.rightExpression))
            }
        }
        methodNode.annotations.removeAll { it.classNode.name.contains(GetMapping.simpleName) }

        def getMapping = new AnnotationNode(new ClassNode(GetMapping))
        //getMapping.addMember("value", new ConstantExpression(methodNode.name))
        methodNode.addAnnotation(getMapping)

        //add cleaned RequestMapping("class.simpleName") if it does not have a request mapping.
        if (!classNode.annotations.find { it.classNode.name.contains(RequestMapping.simpleName) }) {
            def requestMapping = new AnnotationNode(new ClassNode(RequestMapping))
            requestMapping.addMember("value", new ConstantExpression(cleanSimpleName(classNode.name)))
            classNode.addAnnotation(requestMapping)
        }

        // add @RestController to the class if it does not exist
        if (!classNode.annotations.find { it.classNode.name.contains(RestController.simpleName) }) {
            classNode.addAnnotation(new AnnotationNode(new ClassNode(RestController)))
        }

        // add @Slf4j to the class if it does not exist
        if (!classNode.annotations.find { it.classNode.name.contains(Slf4j.simpleName) }) {
            classNode.addAnnotation(new AnnotationNode(new ClassNode(Slf4j)))
        }

        addGlobalsModelAttribute(classNode)
    }

    def returnModelView = { classNode, methodNode, map ->
        new BlockStatement(variableScope: new VariableScope(), statements: [
            new ReturnStatement(new ConstructorCallExpression(new ClassNode(ModelAndView), new ArgumentListExpression([new ConstantExpression(new String("${cleanSimpleName(classNode.name)}/${cleanSimpleName(methodNode.name)}")), map])))
        ])
    }

    def cleanSimpleName(given) {
        given = given.tokenize(".").last()
        given = given.replaceAll("Controller", "")
        //given ? given.substring(0, 1).toLowerCase() + given.substring(1) : given
        given.toLowerCase()
    }

    // @ModelAttribute("global")
    // def global(){
    //      viewMappingService.globals()
    // }
    def addGlobalsModelAttribute(classNode) {
        addViewMappingServiceField(classNode)
        def global = new MethodNode("globals", Modifier.PUBLIC, new ClassNode(Map), new Parameter[0], new ClassNode[0],
            new ReturnStatement(
                new MethodCallExpression(new VariableExpression(ViewMappingService.simpleName), "globals", new ArgumentListExpression())
            )
        )
        def annotation = new AnnotationNode(new ClassNode(ModelAttribute))
        annotation.addMember("value", new ConstantExpression("_global"))
        global.addAnnotation(annotation)
        classNode.methods.add(global)
    }

    // @Autowired
    // ViewMappingService viewMappingService
    def addViewMappingServiceField(classNode) {
        if (!classNode.fields.name.contains(ViewMappingService.simpleName)) {
            def fieldNode = new FieldNode(ViewMappingService.simpleName, ACC_PUBLIC, new ClassNode(ViewMappingService), classNode, new EmptyExpression())
            fieldNode.addAnnotation(new AnnotationNode(new ClassNode(Autowired)))
            classNode.addField(fieldNode)
        }
    }
}