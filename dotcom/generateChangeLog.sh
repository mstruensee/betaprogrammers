cd ~/code/liquibase/3.5.3/
./liquibase /
--driver=oracle.jdbc.OracleDriver \
--changeLogFile=/Users/mstruensee/code/betaprogrammers/dotcom/src/main/resources/db/changelog/generated-change-log.json /
--url="jdbc:oracle:thin:@localhost:1521/orcl" /
--username=system /
--password=oracle /
generateChangeLog