package com.betaprogrammers.dotcom.statistics.repository

import com.betaprogrammers.dotcom.statistics.domain.Statistics
import org.springframework.data.repository.CrudRepository

interface StatisticsRepository extends CrudRepository<Statistics, Long> {}
