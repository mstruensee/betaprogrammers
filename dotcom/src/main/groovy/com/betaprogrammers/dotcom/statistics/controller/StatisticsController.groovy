package com.betaprogrammers.dotcom.statistics.controller

import com.betaprogrammers.core.viewmapping.annotation.ViewMapping
import com.betaprogrammers.dotcom.statistics.service.StatisticsService
import org.springframework.beans.factory.annotation.Autowired

class StatisticsController {

    @Autowired
    StatisticsService statisticsService

    @ViewMapping
    def statistics() {}

}
