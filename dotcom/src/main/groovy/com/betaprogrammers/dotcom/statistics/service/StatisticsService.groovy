package com.betaprogrammers.dotcom.statistics.service

import com.betaprogrammers.dotcom.statistics.repository.StatisticsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class StatisticsService {

    @Autowired
    StatisticsRepository statisticsRepository

    def findAll(){
        statisticsRepository.findAll()
    }
}
