package com.betaprogrammers.dotcom.statistics.domain

import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Statistics {

    @Id
    String tableName

    String count

    Long timestamp
}
