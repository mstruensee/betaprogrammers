package com.betaprogrammers.dotcom.statistics.schedule

import com.betaprogrammers.dotcom.config.service.ConfigurationService
import com.betaprogrammers.dotcom.statistics.repository.StatisticsRepository
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
@Slf4j
class StatisticsScheduledTask {

    @Autowired
    ConfigurationService configurationService

    @Autowired
    StatisticsRepository statisticsRepository

    @Scheduled(fixedRate = 86400000L, initialDelay = 60000L)
    def task(){
//        def database = mongo.usedDatabases.find { it.name == configurationService.mongodbDatabase }
//        database.collectionNames.findAll{ it.toUpperCase() != "STATISTICSDOMAIN"}.each {
//            statisticsRepository.save(new Statistics(name: it, count: database.getCollection(it).count, timestamp: new DateTime().toDate().time))
//        }
        log.info "StatisticsScheduledTask.task executed"
    }
}
