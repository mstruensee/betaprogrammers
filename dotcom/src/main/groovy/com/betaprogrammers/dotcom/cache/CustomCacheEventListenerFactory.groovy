package com.betaprogrammers.dotcom.cache

import net.sf.ehcache.event.CacheEventListener
import net.sf.ehcache.event.CacheEventListenerFactory

class CustomCacheEventListenerFactory extends CacheEventListenerFactory {

    @Override
    CacheEventListener createCacheEventListener(Properties properties) {
        new CustomCacheEventListener()
    }

}