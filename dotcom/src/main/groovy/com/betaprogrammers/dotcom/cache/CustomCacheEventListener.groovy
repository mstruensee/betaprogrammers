package com.betaprogrammers.dotcom.cache

import groovy.util.logging.Slf4j
import net.sf.ehcache.CacheException
import net.sf.ehcache.Ehcache
import net.sf.ehcache.Element
import net.sf.ehcache.event.CacheEventListener

@Slf4j
class CustomCacheEventListener implements CacheEventListener {

    Object clone() throws CloneNotSupportedException {
        new CustomCacheEventListener()
    }

    void dispose() {
        log.info("Cache disposed.")
    }

    void notifyElementEvicted(Ehcache cache, Element cacheMap) {
        log.info("Cache evicted: '${cache.name}', key: ${cacheMap.objectKey.toString()}")
    }

    void notifyElementExpired(Ehcache cache, Element cacheMap) {
        log.info("Cache expired: '${cache.name}', key: ${cacheMap.objectKey.toString()}")
    }

    void notifyElementPut(Ehcache cache, Element cacheMap) throws CacheException {
        log.info("Added to cache: '${cache.name}', key: ${cacheMap.objectKey.toString()}")
    }

    void notifyElementRemoved(Ehcache cache, Element cacheMap) throws CacheException {
        log.info("Removed from cache: '${cache.name}', key: ${cacheMap.objectKey.toString()}")
    }

    void notifyElementUpdated(Ehcache cache, Element cacheMap) throws CacheException {
        log.info("Updated in cache: '${cache.name}', key: ${cacheMap.objectKey.toString()}")
    }

    void notifyRemoveAll(Ehcache cache) {
        log.info("Removing all cached elements: '${cache.name}'")
    }
}

