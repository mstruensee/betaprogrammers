package com.betaprogrammers.dotcom.coverage.domain

import com.betaprogrammers.dotcom.businessunit.domain.BusinessUnit
import com.betaprogrammers.dotcom.dynamicenum.domain.DynamicEnumValue
import com.betaprogrammers.dotcom.product.domain.Product

import javax.persistence.*

@Entity
class OfferedCoverage {

    @Id
    @SequenceGenerator(name = "offeredcoverage_sequence", sequenceName = "hibernate_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "offeredcoverage_sequence")
    Long id

    Date inception

    Date expiration

    @OneToOne
    Product product

    String description

    @OneToOne
    DynamicEnumValue businessType

    @OneToOne
    DynamicEnumValue groupOrIndividual

    @OneToOne
    DynamicEnumValue department

    @OneToOne
    DynamicEnumValue coverageCategory

    @OneToOne
    DynamicEnumValue lineOfBusiness

    @OneToOne
    DynamicEnumValue statLineOfBusiness

    @OneToOne
    DynamicEnumValue reinsuranceMethod

    @OneToOne
    DynamicEnumValue productLine

    @OneToOne
    DynamicEnumValue healthCategory

    @OneToOne
    DynamicEnumValue uploadCoverageCode

    @OneToOne
    BusinessUnit businessUnit
}
