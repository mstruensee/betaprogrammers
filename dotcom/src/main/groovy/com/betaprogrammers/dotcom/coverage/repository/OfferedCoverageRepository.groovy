package com.betaprogrammers.dotcom.coverage.repository

import com.betaprogrammers.dotcom.coverage.domain.OfferedCoverage
import org.springframework.data.repository.CrudRepository

interface OfferedCoverageRepository extends CrudRepository<OfferedCoverage, Long> {
}
