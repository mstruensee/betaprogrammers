package com.betaprogrammers.dotcom.coverage.controller

import com.betaprogrammers.core.viewmapping.annotation.ViewMapping
import com.betaprogrammers.dotcom.coverage.domain.OfferedCoverage
import com.betaprogrammers.dotcom.coverage.service.OfferedCoverageService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody

@Slf4j
class OfferedCoverageController {

    @Autowired
    OfferedCoverageService offeredCoverageService

    @ViewMapping
    def offeredCoverage() {}

    @GetMapping("initialize")
    def initialize() {
        def coverageCategoryEnum = []

        coverageCategoryEnum = offeredCoverageService.coverageCategoryEnum()

        [
            ("coverageCategoryEnum"): coverageCategoryEnum
        ]
    }

    @GetMapping("offeredcoverages")
    def offeredCoverages() {
        offeredCoverageService.findAll()
    }

    @PostMapping
    def create(@RequestBody OfferedCoverage offeredCoverage) {
        offeredCoverageService.create(offeredCoverage)
    }

    @PutMapping(value = "{id}")
    def update(@RequestBody OfferedCoverage offeredCoverage) {
        offeredCoverageService.update(offeredCoverage)
    }

}
