package com.betaprogrammers.dotcom.coverage.service

import com.betaprogrammers.dotcom.coverage.repository.OfferedCoverageRepository
import com.betaprogrammers.dotcom.dynamicenum.service.DynamicEnumService
import com.betaprogrammers.dotcom.exception.EntityConflictException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class OfferedCoverageService {

    @Autowired
    OfferedCoverageRepository offeredCoverageRepository

    @Autowired
    DynamicEnumService dynamicEnumService

    def coverageCategoryEnum() {
        dynamicEnumService.findByName("Coverage Category")
    }

    def findAll() {
        offeredCoverageRepository.findAll()
    }

    def findByName(name) {
        offeredCoverageRepository.findByName(name)
    }

    def findById(id) {
        offeredCoverageRepository.findById(id)
    }

    def exists(offeredCoverage) {
        findByName(offeredCoverage.name) != null
    }

    def create(offeredCoverage) {
        if (exists(offeredCoverage)) {
            throw new EntityConflictException(".form-group-name", "Offered coverage already exists.")
        }

        offeredCoverageRepository.save(offeredCoverage)
    }

    def update(offeredCoverage) {
        offeredCoverageRepository.save(offeredCoverage)
    }
}
