package com.betaprogrammers.dotcom.security.controller

import com.betaprogrammers.core.viewmapping.annotation.ViewMapping
import com.betaprogrammers.dotcom.security.domain.HttpSecurity
import com.betaprogrammers.dotcom.security.service.HttpSecurityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

class HttpSecurityController {

    @Autowired
    HttpSecurityService httpSecurityService

    @ViewMapping
    def httpSecurity() {}

    @GetMapping("security")
    def security() {
        httpSecurityService.findAll()
    }

    @GetMapping("types")
    def types() {
        httpSecurityService.types()
    }

    @PutMapping(value = "{id}")
    def update(@RequestBody HttpSecurity entity) {
        httpSecurityService.updateSecurity(entity)
    }

    @PostMapping()
    def create(@RequestBody HttpSecurity entity) {
        httpSecurityService.createSecurity(entity)
    }

    @DeleteMapping(value = "{id}")
    def delete(@PathVariable id) {
        httpSecurityService.deleteSecurity(id)
    }
}
