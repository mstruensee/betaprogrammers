package com.betaprogrammers.dotcom.security.domain

import javax.persistence.*

@Entity
class HttpSecurity {

    @Id
    @SequenceGenerator(name = "httpsecurity_sequence", sequenceName = "hibernate_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "httpsecurity_sequence")
    Long id

    String matcher

    String authority

    AntMatcherType type
}
