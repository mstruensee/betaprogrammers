package com.betaprogrammers.dotcom.security.repository

import com.betaprogrammers.dotcom.security.domain.HttpSecurity
import org.springframework.data.repository.CrudRepository

interface HttpSecurityRepository extends CrudRepository<HttpSecurity, Long> {}
