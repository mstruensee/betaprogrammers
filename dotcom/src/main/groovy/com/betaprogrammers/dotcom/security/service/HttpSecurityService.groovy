package com.betaprogrammers.dotcom.security.service

import com.betaprogrammers.dotcom.security.domain.AntMatcherType
import com.betaprogrammers.dotcom.security.repository.HttpSecurityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.stereotype.Service

@Service
class HttpSecurityService {

    @Autowired
    HttpSecurityRepository httpSecurityRepository

    @Autowired
    ApplicationContext applicationContext

    def findAll() {
        httpSecurityRepository.findAll()
    }

    def createSecurity(entity) {
        def created = httpSecurityRepository.save(entity)
        //TODO this does not work at runtime, figure out how to make it work...
//        if (created) {
//            addSecurity(created)
//        }
        created
    }

    def updateSecurity(entity) {
        createSecurity(entity)
    }

    def deleteSecurity(id) {
        def entity = httpSecurityRepository.findOne(id)
        if (entity) {
            httpSecurityRepository.delete(entity)
        }
        entity
    }

    def types() {
        AntMatcherType.enumConstants
    }

    def addAllSecurities(http) {
        def securities = httpSecurityRepository.findAll()
        securities?.each {
            addSecurity(it)
        }
    }

    def addSecurity(entity) {
        def http = applicationContext.getBean(WebSecurityConfigurerAdapter).http

        switch (entity.type) {
            case AntMatcherType.PERMIT_ALL:
                http.authorizeRequests().antMatchers("${entity.matcher}").permitAll()
                break
            case AntMatcherType.HAS_AUTHORITY:
                http.authorizeRequests().antMatchers("${entity.matcher}").hasAuthority(entity.authority)
                break
            case AntMatcherType.ROLE:
                http.authorizeRequests().antMatchers("${entity.matcher}").hasRole(entity.authority)
                break
            default:
                http.authorizeRequests().antMatchers("${entity.matcher}").permitAll()
                break
        }
    }
}
