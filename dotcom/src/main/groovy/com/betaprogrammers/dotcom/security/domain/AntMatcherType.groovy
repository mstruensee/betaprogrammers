package com.betaprogrammers.dotcom.security.domain

enum AntMatcherType {
    PERMIT_ALL,
    HAS_AUTHORITY,
    ROLE
}