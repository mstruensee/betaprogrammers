package com.betaprogrammers.dotcom.home.service

import com.betaprogrammers.dotcom.config.service.ConfigurationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class HomeService {
    @Autowired
    ConfigurationService configurationService

    def time = new Date()

    def user() {
        SecurityContextHolder.getContext().getAuthentication()
    }
}
