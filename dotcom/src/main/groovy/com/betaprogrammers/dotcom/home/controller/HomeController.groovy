package com.betaprogrammers.dotcom.home.controller

import com.betaprogrammers.core.viewmapping.annotation.ViewMapping
import com.betaprogrammers.dotcom.home.service.HomeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("home")
class HomeController {

    @Autowired
    HomeService homeService

    @ViewMapping
    def index() {
        [
            time                 : homeService.time.toString(),
            springApplicationName: homeService.configurationService.applicationName
        ]
    }

    @GetMapping("time")
    def time() { homeService.time }
}
