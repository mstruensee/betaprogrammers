package com.betaprogrammers.dotcom.config

import com.betaprogrammers.dotcom.exception.EntityConflictException
import com.betaprogrammers.dotcom.exception.EntityNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class GlobalExceptionHandler {

    @ExceptionHandler(EntityNotFoundException)
    def entityNotFound() {
        return new ResponseEntity(HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler(EntityConflictException)
    def entityConfict(exception) {
        return new ResponseEntity(exception.toMap(), HttpStatus.CONFLICT)
    }
}
