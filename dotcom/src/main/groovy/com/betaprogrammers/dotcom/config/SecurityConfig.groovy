package com.betaprogrammers.dotcom.config

import com.betaprogrammers.dotcom.security.service.HttpSecurityService
import com.betaprogrammers.dotcom.user.service.CurrentUserDetailsService
import com.betaprogrammers.dotcom.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@EnableWebSecurity
class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    CurrentUserDetailsService currentUserDetailsService

    @Autowired
    UserService userService

    @Autowired
    HttpSecurityService httpSecurityService

    protected void configure(HttpSecurity http) throws Exception {
        //applySecuritiesFromDatabase(http)

        // @formatter:off
        http
            .authorizeRequests()
        //bootstrap > 3.1 includes .map for the min files
            .antMatchers("/*/**/*.css", "/*/**/*.js", "/*/**/*.map").permitAll()
            .antMatchers("/httpsecurity/**").hasAuthority("admin")
            .antMatchers("/h2-console/**").permitAll()
        //only these 2 need to be here, the rest can be configured via database
            .anyRequest()
            .authenticated()
            .and()
            .formLogin()
            .loginPage("/login").permitAll()
            .defaultSuccessUrl("/home")
            .and().logout()
            .deleteCookies("remember-me")
            .permitAll()
            .and()
            .requestCache()
            .and()
            .rememberMe()
            .and()
            .csrf().disable()
        // @formatter:off

    }

    def applySecuritiesFromDatabase(http) {
        httpSecurityService.addAllSecurities(http)
    }

    @Autowired
    void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .userDetailsService(currentUserDetailsService)
    }
}
