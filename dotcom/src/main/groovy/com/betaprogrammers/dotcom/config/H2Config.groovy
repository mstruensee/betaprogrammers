package com.betaprogrammers.dotcom.config

//import org.h2.tools.Server
//import org.hibernate.jpa.HibernatePersistenceProvider
//import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//import org.springframework.context.annotation.Profile
//import org.springframework.jdbc.datasource.DriverManagerDataSource
//import org.springframework.orm.jpa.JpaTransactionManager
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
//import org.springframework.transaction.PlatformTransactionManager
//
//import javax.persistence.EntityManagerFactory
//import javax.sql.DataSource

//@Configuration
//@Profile("h2")
class H2Config {

//    @Bean
//    DataSource dataSource() {
//        def dataSource = new DriverManagerDataSource()
//        dataSource.driverClassName = "org.h2.Driver"
//        dataSource.url = "jdbc:h2:mem:dotcom;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE"
//        dataSource.username = "sa"
//        dataSource
//    }
//
//    @Bean
//    Properties jpaProperties() {
//        def properties = new Properties()
//        properties.setProperty("hibernate.hbm2ddl.auto", "create-drop")
//        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect")
//        properties
//    }
//
//    @Bean
//    PlatformTransactionManager transactionManager() {
//        def manager = new JpaTransactionManager()
//        manager.entityManagerFactory = entityManagerFactory()
//        manager
//    }
//
//    @Bean
//    EntityManagerFactory entityManagerFactory() {
//        def factory = new LocalContainerEntityManagerFactoryBean()
//        factory.dataSource = dataSource()
//        factory.jpaProperties = jpaProperties()
//        factory.jpaVendorAdapter = new HibernateJpaVendorAdapter()
//        factory.packagesToScan = "com.betaprogrammers.**.**.repository"
//        factory.persistenceProviderClass = HibernatePersistenceProvider
//        factory.afterPropertiesSet()
//        factory.object
//    }
//
//    @Bean
//    Server h2Server() {
//        def server = new Server()
//        server.runTool("-tcp")
//        server.runTool("-tcpAllowOthers")
//
//        return server
//
//    }
}
