package com.betaprogrammers.dotcom.config

import oracle.jdbc.pool.OracleDataSource
import org.h2.tools.Server
import org.hibernate.jpa.HibernatePersistenceProvider
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.datasource.DriverManagerDataSource
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.PlatformTransactionManager

import javax.persistence.EntityManagerFactory
import javax.sql.DataSource
import java.sql.SQLException

@Configuration
class DatabaseConfig {

    @Bean(name = "dataSource")
    @Profile("oracle")
    DataSource oracleDataSource() throws SQLException {
        OracleDataSource dataSource = new OracleDataSource()
        dataSource.serviceName = "orcl"
        dataSource.serverName = "localhost"
        dataSource.user = "dotcom"
        dataSource.password = "dotcom1"
        dataSource.portNumber = 1521
        dataSource.driverType = "thin"
        dataSource
    }

    @Bean(name = "dataSource")
    @Profile("h2")
    DataSource h2DataSource() {
        def dataSource = new DriverManagerDataSource()
        dataSource.driverClassName = "org.h2.Driver"
        dataSource.url = "jdbc:h2:mem:dotcom;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE"
        dataSource.username = "sa"
        dataSource
    }

    @Bean
    @Profile("h2")
    Server h2Server() {
        def server = new Server()
        server.runTool("-tcp")
        server.runTool("-tcpAllowOthers")

        server
    }

    @Bean
    DataSource dataSource() {
        return null
    }

    @Bean
    Properties jpaProperties() {
        def properties = new Properties()
        //properties.setProperty("hibernate.hbm2ddl.auto", "drop")
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle12cDialect")
        properties
    }

    @Bean
    PlatformTransactionManager transactionManager() {
        def manager = new JpaTransactionManager()
        manager.entityManagerFactory = entityManagerFactory()
        manager
    }

    @Bean
    EntityManagerFactory entityManagerFactory() {
        def factory = new LocalContainerEntityManagerFactoryBean()
        factory.dataSource = dataSource()
        factory.jpaProperties = jpaProperties()
        factory.jpaVendorAdapter = new HibernateJpaVendorAdapter()
        factory.packagesToScan = "com.betaprogrammers.**.**.repository"
        factory.persistenceProviderClass = HibernatePersistenceProvider
        factory.afterPropertiesSet()
        factory.object
    }
}
