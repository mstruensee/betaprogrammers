package com.betaprogrammers.dotcom.config

import com.betaprogrammers.dotcom.cache.CustomCacheEventListenerFactory
import groovy.util.logging.Slf4j
import net.sf.ehcache.CacheManager
import net.sf.ehcache.config.CacheConfiguration
import net.sf.ehcache.config.FactoryConfiguration
import net.sf.ehcache.distribution.RMICacheManagerPeerListenerFactory
import net.sf.ehcache.distribution.RMICacheManagerPeerProviderFactory
import net.sf.ehcache.distribution.RMICacheReplicatorFactory
import net.sf.ehcache.store.MemoryStoreEvictionPolicy
import org.springframework.beans.factory.annotation.Value
import org.springframework.cache.ehcache.EhCacheCacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Slf4j
@Configuration
class CacheConfig {

    static final String PRODUCT_CACHE = "product-cache"
    static final String USER_CACHE = "user-cache"

    @Value('${cache.manager.rmi.port}')
    int rmiPort

    @Value('${cache.manager.socket.timeout}')
    int socketTimeout

    @Value('''#{'${cache.manager.hosts.to.replicate.cache}'.split(',')}''')
    List hostsToReplicateCache

    @Bean
    EhCacheCacheManager cacheManager() {
        //level 1 caching
        def configuration = new net.sf.ehcache.config.Configuration()
        configuration.addCache(hourlyCache(PRODUCT_CACHE))
        configuration.addCache(hourlyCache(USER_CACHE))

        // level 2 caching - replication server caching using RMI
        // http://www.ehcache.org/documentation/2.7/replication/rmi-replicated-caching.html
        configuration.cacheManagerPeerListenerFactory(rmiCachePeerListener())
        def providers = rmiCachePeerProviders(configuration)
        providers.collect { provider ->
            configuration.cacheManagerPeerProviderFactory(provider)
        }

        new EhCacheCacheManager(CacheManager.newInstance(configuration))
    }

    def replicationListener() {
        def clusterCachingListener = new CacheConfiguration.CacheEventListenerFactoryConfiguration()
        clusterCachingListener.class = RMICacheReplicatorFactory.name

        clusterCachingListener
    }

    /**
     @see com.betaprogrammers.dotcom.cache.CustomCacheEventListener
     */
    def customListener(properties = null) {
        def listenerFactoryConfiguration = new CacheConfiguration.CacheEventListenerFactoryConfiguration()
        listenerFactoryConfiguration.class = CustomCacheEventListenerFactory.name
        // use properties to pass variables to the listener for debugging/logging purposes (CacheListener.cacheName)
        listenerFactoryConfiguration.properties = properties
        listenerFactoryConfiguration
    }

    def rmiCachePeerListener() {
        // this will resolve the issue when Registery.loopup(x) returns localhost ... do not delete
        System.setProperty("java.rmi.server.hostname", InetAddress.localHost.hostAddress)

        // create 1 listener per server
        def rmiCachePeerListener = new FactoryConfiguration()
        rmiCachePeerListener.className(RMICacheManagerPeerListenerFactory.name)
        rmiCachePeerListener.properties = "port=${rmiPort},socketTimeoutMillis=${socketTimeout}"
        rmiCachePeerListener
    }

    def rmiCachePeerProviders(configuration) {
        def providers = []

        // do not include self, include all others ... create 1 provider per server
        hostsToReplicateCache.each { hostName ->
            if (!InetAddress.localHost.hostName.toLowerCase().contains(hostName.toString().toLowerCase())) {
                def rmiUrls = "rmiUrls="
                def provider = new FactoryConfiguration()
                provider.className(RMICacheManagerPeerProviderFactory.name)

                configuration.cacheConfigurations.each { cache ->
                    rmiUrls += "//${hostName}:${rmiPort}/${cache.key}|"
                    cache.value.cacheEventListenerConfigurations << replicationListener()
                }

                provider.properties = "peerDiscovery=manual,${rmiUrls}"
                log.info("Peer Provider created: ${provider.fullyQualifiedClassPath} / ${provider.properties}")
                providers << provider
            }

        }
        providers
    }

    def hourlyCache(name) {
        new CacheConfiguration(
            name: name,
            maxElementsInMemory: 1,
            overflowToDisk: false,
            memoryStoreEvictionPolicy: MemoryStoreEvictionPolicy.LFU,
            timeToIdleSeconds: 0,
            timeToLiveSeconds: 3600,
            cacheEventListenerConfigurations: [customListener("cacheName=${name}")]
        )
    }
}
