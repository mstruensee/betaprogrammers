package com.betaprogrammers.dotcom.config.service

import com.betaprogrammers.core.viewmapping.service.ViewMappingService
import com.betaprogrammers.dotcom.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service('viewMappingService')
class GlobalsService implements ViewMappingService {
    @Value('${globals.application.name}')
    def applicationName

    @Value('${globals.navbar.brand}')
    def navbarBrand

    @Autowired
    UserService userService

    def globals() {
        def globals = [:]
        this.class.declaredFields.findAll { it.name =~ /^((?!\$|metaClass|__|log).)*$/ }.each {
            globals.put(it.name, this."${it.name}")
        }

        def springUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal()
        if (springUser.hasProperty('username')) {
            def user = userService.findUserByUsername(springUser.username)
            globals.put("roles", user.roles.name.collect())
        }

        globals
    }

}
