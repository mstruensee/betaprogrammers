package com.betaprogrammers.dotcom.config.service

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class ConfigurationService {

    @Value('${globals.application.name}')
    def applicationName

}
