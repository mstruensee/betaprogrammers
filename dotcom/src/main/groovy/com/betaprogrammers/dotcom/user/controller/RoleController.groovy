package com.betaprogrammers.dotcom.user.controller

import com.betaprogrammers.core.viewmapping.annotation.ViewMapping
import com.betaprogrammers.dotcom.user.domain.Role
import com.betaprogrammers.dotcom.user.service.RoleService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody

@Slf4j
class RoleController {
    @Autowired
    RoleService roleService

    @ViewMapping
    def role() {}

    @GetMapping("roles")
    def roles() {
        roleService.findAll()
    }

    @PostMapping
    def create(@RequestBody Role role) {
        roleService.createRole(role)
    }

    @PutMapping(value = "{id}")
    def update(@RequestBody Role role) {
        roleService.updateRole(role)
    }
}
