package com.betaprogrammers.dotcom.user.repository

import com.betaprogrammers.dotcom.config.CacheConfig
import com.betaprogrammers.dotcom.user.domain.User
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.repository.CrudRepository

interface UserRepository extends CrudRepository<User, Long> {

    static final String CACHE_KEY = "user-cache-key"

    User findOneByEmail(email)

    User findOneByUsername(username)

    @Cacheable(value = CacheConfig.USER_CACHE, key = "#root.target.CACHE_KEY")
    List findAll()

    @CacheEvict(value = CacheConfig.USER_CACHE, key = "#root.target.CACHE_KEY")
    User save(user)
}
