package com.betaprogrammers.dotcom.user.repository

import com.betaprogrammers.dotcom.user.domain.Role
import org.springframework.data.repository.CrudRepository

interface RoleRepository extends CrudRepository<Role, Long> {
    Role findOneByName(name)
}
