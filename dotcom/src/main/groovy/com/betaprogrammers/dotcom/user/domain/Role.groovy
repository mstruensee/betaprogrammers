package com.betaprogrammers.dotcom.user.domain

import javax.persistence.*

@Entity
class Role {

    @Id
    @SequenceGenerator(name = "role_sequence", sequenceName = "hibernate_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_sequence")
    Long id

    String name

    String description
}
