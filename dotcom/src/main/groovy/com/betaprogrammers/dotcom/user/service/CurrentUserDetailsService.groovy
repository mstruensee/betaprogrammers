package com.betaprogrammers.dotcom.user.service

import com.betaprogrammers.dotcom.encryption.service.CipherService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class CurrentUserDetailsService implements UserDetailsService {
    @Autowired
    UserService userService

    @Autowired
    CipherService cipherService

    @Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        def user = userService.findUserByUsername(username)
        if (!user) {
            throw new UsernameNotFoundException("User with username=${username} was not found.")
        }
        if (user.disabled) {
            throw new UsernameNotFoundException("User with username=${username} is disabled.")
        }
        new User(user.username, cipherService.decrypt(user.password), !user.disabled, true, true, true, AuthorityUtils.createAuthorityList(user.authorities()))
    }
}
