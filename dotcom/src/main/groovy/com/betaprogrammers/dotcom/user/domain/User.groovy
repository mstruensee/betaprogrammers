package com.betaprogrammers.dotcom.user.domain

import javax.persistence.*

@Entity
class User {

    @Id
    @SequenceGenerator(name = "user_sequence", sequenceName = "hibernate_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_sequence")
    Long id

    String username

    String firstname

    String lastname

    String password

    String email

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "user_role",
        joinColumns = @JoinColumn(name = "user_id"),
        inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    List<Role> roles

    boolean disabled = false

    // todo add user preferences

    def authorities() {
        def authorities = []
        roles.each {
            authorities << it.name
        }
        authorities as String[]
    }
}
