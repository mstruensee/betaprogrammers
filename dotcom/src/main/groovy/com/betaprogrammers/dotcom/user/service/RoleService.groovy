package com.betaprogrammers.dotcom.user.service

import com.betaprogrammers.dotcom.exception.EntityConflictException
import com.betaprogrammers.dotcom.user.repository.RoleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RoleService {

    @Autowired
    RoleRepository roleRepository

    def roleExists(name) {
        roleRepository.findOneByName(name) != null
    }

    def createRole(role) {
        if (roleExists(role.name)) {
            throw new EntityConflictException(".form-group-name", "Role already exists.")
        }
        roleRepository.save(role)
    }

    def updateRole(role) {
        roleRepository.save(role)
    }

    def findAll() {
        roleRepository.findAll()
    }
}
