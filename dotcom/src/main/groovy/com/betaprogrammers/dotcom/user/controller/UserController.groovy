package com.betaprogrammers.dotcom.user.controller

import com.betaprogrammers.core.viewmapping.annotation.ViewMapping
import com.betaprogrammers.dotcom.user.domain.User
import com.betaprogrammers.dotcom.user.service.RoleService
import com.betaprogrammers.dotcom.user.service.UserService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@Slf4j
class UserController {

    @Autowired
    UserService userService

    @Autowired
    RoleService roleService

    @ViewMapping
    def user() {}

    @GetMapping("users")
    def users() {
        userService.findAll()
    }

    @GetMapping("initialize")
    def initialize() {
        [
            roles: roleService.findAll()
        ]
    }

    @PostMapping
    def create(@RequestBody User user) {
        userService.createUser(user)
    }

    @PutMapping(value = "{id}")
    def update(@RequestBody User user) {
        userService.updateUser(user)
    }

    @GetMapping(value = "{id}")
    def findUser(@PathVariable(value = "id") Long id) {
        userService.findUserById(id)
    }
}
