package com.betaprogrammers.dotcom.user.service

import com.betaprogrammers.dotcom.encryption.service.CipherService
import com.betaprogrammers.dotcom.exception.EntityConflictException
import com.betaprogrammers.dotcom.exception.EntityNotFoundException
import com.betaprogrammers.dotcom.user.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserService {

    @Autowired
    UserRepository userRepository

    @Autowired
    CipherService cipherService

    def findAll() {
        def users = userRepository.findAll()
        users.each { it.password = null }
        users
    }

    def userExists(username) {
        userRepository.findOneByUsername(username) != null
    }

    def emailInUse(email) {
        findUserByEmail(email) != null
    }

    def findUserByEmail(email) {
        def user = userRepository.findOneByEmail(email)
        if (!user) throw new EntityNotFoundException()
        user
    }

    def findUserByUsername(username) {
        def user = userRepository.findOneByUsername(username)
        if (!user) throw new EntityNotFoundException()
        user
    }

    def findUserById(id) {
        def user = userRepository.findById(id)
        if (!user) throw new EntityNotFoundException()
        user
    }

    def createUser(user) {
        if (userExists(user.username)) {
            throw new EntityConflictException(".form-group-username", "Username already in use.")
        }

        if (emailInUse(user.email)) {
            throw new EntityConflictException(".form-group-email", "Email already in use.")
        }

        user.password = cipherService.encrypt(user.password)
        def created = userRepository.save(user)
        created.password = null
        created
    }

    def updateUser(user) {
        def exists = userRepository.findOne(user.id)
        user.password = exists.password
        userRepository.save(user)
    }
}
