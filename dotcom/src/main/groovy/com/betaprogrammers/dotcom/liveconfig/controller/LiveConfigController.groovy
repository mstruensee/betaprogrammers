package com.betaprogrammers.dotcom.liveconfig.controller

import com.betaprogrammers.core.liveconfig.service.LiveConfigService
import com.betaprogrammers.core.viewmapping.annotation.ViewMapping
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody

class LiveConfigController {

    @Autowired
    LiveConfigService liveConfigService

    @ViewMapping
    def liveconfig() {}

    @GetMapping("properties")
    def properties() {
        liveConfigService.fetchAll()
    }

    @PutMapping(value = "{id}")
    def update(@RequestBody entity) {
        liveConfigService.update(entity)
    }
}
