package com.betaprogrammers.dotcom.login.controller

import com.betaprogrammers.core.viewmapping.annotation.ViewMapping
import org.springframework.web.bind.annotation.RequestParam

class LoginController {
    @ViewMapping
    def login(@RequestParam(value = "error", required = false) error, @RequestParam(value = "logout", required = false) logout) {
        def map = [:]
        if (error != null) {
            map.put("error", "Invalid username and password.")
        }

        if (logout != null) {
            map.put("logout", "You've been logged out successfully.")
        }
        map
    }
}
