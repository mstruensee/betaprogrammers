package com.betaprogrammers.dotcom.businessunit.service

import com.betaprogrammers.dotcom.businessunit.repository.BusinessUnitRepository
import com.betaprogrammers.dotcom.exception.EntityConflictException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BusinessUnitService {

    @Autowired
    BusinessUnitRepository businessUnitRepository

    def findAll() {
        businessUnitRepository.findAll()
    }

    def findByName(name) {
        businessUnitRepository.findByName(name)
    }

    def findById(id) {
        businessUnitRepository.findById(id)
    }

    def exists(businessUnit) {
        findByName(businessUnit.name) != null
    }

    def create(businessUnit) {
        if (exists(businessUnit)) {
            throw new EntityConflictException(".form-group-name", "Business Unit already exists.")
        }

        businessUnitRepository.save(businessUnit)
    }

    def update(businessUnit) {
        businessUnitRepository.save(businessUnit)
    }
}
