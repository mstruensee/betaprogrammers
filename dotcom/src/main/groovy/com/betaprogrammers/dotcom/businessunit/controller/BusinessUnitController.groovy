package com.betaprogrammers.dotcom.businessunit.controller

import com.betaprogrammers.core.viewmapping.annotation.ViewMapping
import com.betaprogrammers.dotcom.businessunit.domain.BusinessUnit
import com.betaprogrammers.dotcom.businessunit.service.BusinessUnitService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody

@Slf4j
class BusinessUnitController {

    @Autowired
    BusinessUnitService businessUnitService

    @ViewMapping
    def businessUnit() {}

    @GetMapping("initialize")
    def initialize() {}

    @GetMapping("businessunits")
    def businessUnits() {
        businessUnitService.findAll()
    }

    @PostMapping
    def create(@RequestBody BusinessUnit businessUnit) {
        businessUnitService.create(businessUnit)
    }

    @PutMapping(value = "{id}")
    def update(@RequestBody BusinessUnit businessUnit) {
        businessUnitService.update(businessUnit)
    }
}
