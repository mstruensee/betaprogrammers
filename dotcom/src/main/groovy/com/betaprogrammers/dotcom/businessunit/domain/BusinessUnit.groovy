package com.betaprogrammers.dotcom.businessunit.domain

import javax.persistence.*

@Entity
class BusinessUnit {

    @Id
    @SequenceGenerator(name = "businessunit_sequence", sequenceName = "hibernate_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "businessunit_sequence")
    Long id

    String name
}
