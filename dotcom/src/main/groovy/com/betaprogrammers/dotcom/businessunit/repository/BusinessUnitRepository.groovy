package com.betaprogrammers.dotcom.businessunit.repository

import com.betaprogrammers.dotcom.businessunit.domain.BusinessUnit
import org.springframework.data.repository.CrudRepository

interface BusinessUnitRepository extends CrudRepository<BusinessUnit, Long> {
    BusinessUnit findByName(name)
}
