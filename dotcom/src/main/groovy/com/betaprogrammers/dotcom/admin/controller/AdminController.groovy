package com.betaprogrammers.dotcom.admin.controller

import com.betaprogrammers.dotcom.statistics.service.StatisticsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("admin")
class AdminController {

    @Autowired
    StatisticsService statisticsService

    @GetMapping("statistics")
    def findAll() {
        statisticsService.findAll()
    }

}
