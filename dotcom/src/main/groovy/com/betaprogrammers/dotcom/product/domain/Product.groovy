package com.betaprogrammers.dotcom.product.domain

import com.betaprogrammers.dotcom.dynamicenum.domain.DynamicEnumValue

import javax.persistence.*

@Entity
class Product {

    @Id
    @SequenceGenerator(name = "product_sequence", sequenceName = "hibernate_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_sequence")
    Long id

    String name

    String reportingProduct

    String category

    @OneToOne
    DynamicEnumValue statLineOfBusiness

    @OneToOne
    DynamicEnumValue groupOrIndividual

    @OneToOne
    DynamicEnumValue australiaLumpSumDI

    @OneToOne
    DynamicEnumValue productType
}
