package com.betaprogrammers.dotcom.product.repository

import com.betaprogrammers.dotcom.config.CacheConfig
import com.betaprogrammers.dotcom.product.domain.Product
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.repository.CrudRepository

interface ProductRepository extends CrudRepository<Product, Long> {

    static final String CACHE_KEY = "product-cache-key"

    Product findOneByName(name)

    @Cacheable(value = CacheConfig.PRODUCT_CACHE, key = "#root.target.CACHE_KEY")
    List findAll()

    @CacheEvict(value = CacheConfig.PRODUCT_CACHE, key = "#root.target.CACHE_KEY")
    Product save(product)
}
