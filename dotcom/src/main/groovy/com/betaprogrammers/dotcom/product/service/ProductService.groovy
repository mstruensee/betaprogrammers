package com.betaprogrammers.dotcom.product.service

import com.betaprogrammers.dotcom.dynamicenum.service.DynamicEnumService
import com.betaprogrammers.dotcom.product.repository.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class ProductService {

    @Autowired
    ProductRepository productRepository

    @Autowired
    DynamicEnumService dynamicEnumService

    def groupOrIndividualEnum() {
        dynamicEnumService.findByName("Product Group")
    }

    def productTypeEnum() {
        dynamicEnumService.findByName("Product Type")
    }

    def australiaLumpSumDIEnum() {
        dynamicEnumService.findByName("Australia Lump Sum DI")
    }

    def statLineOfBusinessEnum() {
        dynamicEnumService.findByName("Stat Line of Business")
    }

    def productExists(name) {
        productRepository.findOneByName(name) != null
    }

    def createProduct(product) {
        def errors = []
        if (productExists(product.name)) {
            errors << [selector: "#name", BAM: "Product already exists."]
        }

        if (errors.size() > 0) {
            return new ResponseEntity([errors: errors], HttpStatus.CONFLICT)
        }
        productRepository.save(product)
    }

    def updateProduct(product) {
        productRepository.save(product)
    }

    def findAll() {
        productRepository.findAll()
    }

}
