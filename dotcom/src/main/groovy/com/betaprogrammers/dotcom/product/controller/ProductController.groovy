package com.betaprogrammers.dotcom.product.controller

import com.betaprogrammers.core.viewmapping.annotation.ViewMapping
import com.betaprogrammers.dotcom.product.domain.Product
import com.betaprogrammers.dotcom.product.service.ProductService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody

@Slf4j
class ProductController {

    @Autowired
    ProductService productService

    @ViewMapping
    def product() {}

    @GetMapping("initialize")
    def initialize() {
        def groupOrIndividualEnum = []
        def australiaLumpSumDIEnum = []
        def productTypeEnum = []
        def statLineOfBusinessEnum = []

        groupOrIndividualEnum = productService.groupOrIndividualEnum()
        australiaLumpSumDIEnum = productService.australiaLumpSumDIEnum()
        productTypeEnum = productService.productTypeEnum()
        statLineOfBusinessEnum = productService.statLineOfBusinessEnum()

        [
            ("groupOrIndividualEnum") : groupOrIndividualEnum,
            ("australiaLumpSumDIEnum"): australiaLumpSumDIEnum,
            ("productTypeEnum")       : productTypeEnum,
            ("statLineOfBusinessEnum"): statLineOfBusinessEnum
        ]
    }

    @GetMapping("products")
    def products() {
        productService.findAll()
    }

    @PostMapping
    def create(@RequestBody Product product) {
        productService.createProduct(product)
    }

    @PutMapping(value = "{id}")
    def update(@RequestBody Product product) {
        productService.updateProduct(product)
    }
}
