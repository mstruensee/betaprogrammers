package com.betaprogrammers.dotcom.dynamicenum.controller

import com.betaprogrammers.core.viewmapping.annotation.ViewMapping
import com.betaprogrammers.dotcom.dynamicenum.domain.DynamicEnum
import com.betaprogrammers.dotcom.dynamicenum.service.DynamicEnumService
import com.betaprogrammers.dotcom.dynamicenum.service.DynamicEnumValueService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody

@Slf4j
class DynamicEnumController {

    @Autowired
    DynamicEnumService dynamicEnumService

    @Autowired
    DynamicEnumValueService dynamicEnumValueService

    @ViewMapping
    def dynamicEnum() {}

    @GetMapping("initialize")
    def initialize() {
        def values = []

        try {
            values = dynamicEnumValueService.findAll()
        } catch (e) {
            log.error(e.message)
        }

        [
            ("values"): values
        ]
    }

    @GetMapping("enums")
    def enums() {
        dynamicEnumService.findAll()
    }

    @PostMapping
    def create(@RequestBody DynamicEnum dynamicEnum) {
        dynamicEnumService.create(dynamicEnum)
    }

    @PutMapping(value = "{id}")
    def update(@RequestBody DynamicEnum dynamicEnum) {
        dynamicEnumService.update(dynamicEnum)
    }
}
