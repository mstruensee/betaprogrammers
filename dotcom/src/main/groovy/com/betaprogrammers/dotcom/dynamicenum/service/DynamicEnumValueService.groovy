package com.betaprogrammers.dotcom.dynamicenum.service

import com.betaprogrammers.dotcom.dynamicenum.repository.DynamicEnumValueRepository
import com.betaprogrammers.dotcom.exception.EntityConflictException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DynamicEnumValueService {

    @Autowired
    DynamicEnumValueRepository dynamicEnumValueRepository

    def findAll() {
        dynamicEnumValueRepository.findAll()
    }

    def exists(dynamicEnumValue) {
        dynamicEnumValueRepository.findByValue(dynamicEnumValue.value) != null
    }

    def create(dynamicEnumValue) {
        if (exists(dynamicEnumValue)) {
            throw new EntityConflictException(".form-group-value", "Dynamic enum value already exists.")
        }

        dynamicEnumValueRepository.save(dynamicEnumValue)
    }

    def update(dynamicEnumValue) {
        dynamicEnumValueRepository.save(dynamicEnumValue)
    }
}
