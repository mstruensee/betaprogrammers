package com.betaprogrammers.dotcom.dynamicenum.repository

import com.betaprogrammers.dotcom.dynamicenum.domain.DynamicEnumValue
import org.springframework.data.repository.CrudRepository

interface DynamicEnumValueRepository extends CrudRepository<DynamicEnumValue, Long> {
    DynamicEnumValue findByValue(String value)
}