package com.betaprogrammers.dotcom.dynamicenum.domain

import javax.persistence.*

@Entity
class DynamicEnum {

    @Id
    @SequenceGenerator(name = "dynamicenum_sequence", sequenceName = "hibernate_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dynamicenum_sequence")
    Long id

    String name

    @OneToMany
    @JoinTable(
        name = "dynamicenum_dynamicenumvalue",
        joinColumns = @JoinColumn(name = "dynamicenum_id"),
        inverseJoinColumns = @JoinColumn(name = "dynamicenumvalue_id")
    )
    List<DynamicEnumValue> values
}
