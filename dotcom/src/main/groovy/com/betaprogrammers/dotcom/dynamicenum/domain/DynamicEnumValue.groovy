package com.betaprogrammers.dotcom.dynamicenum.domain

import javax.persistence.*

@Entity
class DynamicEnumValue {

    @Id
    @SequenceGenerator(name = "dynamicenumvalue_sequence", sequenceName = "hibernate_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dynamicenumvalue_sequence")
    Long id

    String value

    String code
}
