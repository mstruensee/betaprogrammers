package com.betaprogrammers.dotcom.dynamicenum.service

import com.betaprogrammers.dotcom.dynamicenum.repository.DynamicEnumRepository
import com.betaprogrammers.dotcom.exception.EntityConflictException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DynamicEnumService {

    @Autowired
    DynamicEnumRepository dynamicEnumRepository

    def findAll() {
        dynamicEnumRepository.findAll()
    }

    def findByName(name) {
        dynamicEnumRepository.findByName(name)
    }

    def findById(id) {
        dynamicEnumRepository.findById(id)
    }

    def exists(dynamicEnum) {
        findByName(dynamicEnum.name) != null
    }

    def create(dynamicEnum) {
        if (exists(dynamicEnum)) {
            throw new EntityConflictException(".form-group-name", "Dynamic enum already exists.")
        }

        dynamicEnumRepository.save(dynamicEnum)
    }

    def update(dynamicEnum) {
        dynamicEnumRepository.save(dynamicEnum)
    }
}
