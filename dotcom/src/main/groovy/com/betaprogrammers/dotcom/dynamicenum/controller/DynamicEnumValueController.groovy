package com.betaprogrammers.dotcom.dynamicenum.controller

import com.betaprogrammers.core.viewmapping.annotation.ViewMapping
import com.betaprogrammers.dotcom.dynamicenum.domain.DynamicEnumValue
import com.betaprogrammers.dotcom.dynamicenum.service.DynamicEnumValueService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody

@Slf4j
class DynamicEnumValueController {

    @Autowired
    DynamicEnumValueService dynamicEnumValueService

    @ViewMapping
    def dynamicEnumValue() {}

    @GetMapping("values")
    def values() {
        dynamicEnumValueService.findAll()
    }

    @PostMapping
    def create(@RequestBody DynamicEnumValue dynamicEnumValue) {
        dynamicEnumValueService.create(dynamicEnumValue)
    }

    @PutMapping(value = "{id}")
    def update(@RequestBody DynamicEnumValue dynamicEnumValue) {
        dynamicEnumValueService.update(dynamicEnumValue)
    }
}
