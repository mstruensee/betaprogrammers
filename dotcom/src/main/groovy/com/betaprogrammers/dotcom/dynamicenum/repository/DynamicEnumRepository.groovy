package com.betaprogrammers.dotcom.dynamicenum.repository

import com.betaprogrammers.dotcom.dynamicenum.domain.DynamicEnum
import org.springframework.data.repository.CrudRepository

interface DynamicEnumRepository extends CrudRepository<DynamicEnum, Long> {
    DynamicEnum findByName(name)
}