package com.betaprogrammers.dotcom.exception

class EntityNotFoundException extends RuntimeException {

    EntityNotFoundException() {
        super("Entity not found.")
    }

    EntityNotFoundException(message) {
        super(message)
    }
}
