package com.betaprogrammers.dotcom.exception

class EntityConflictException extends RuntimeException {

    def selector, bam

    EntityConflictException(selector, bam) {
        this.selector = selector
        this.bam = bam
    }

    def toMap() {
        [
            selector: selector,
            bam     : bam
        ]
    }
}
