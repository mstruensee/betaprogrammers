package com.betaprogrammers.dotcom.encryption.service

import com.betaprogrammers.core.encryption.cipher.CipherFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class CipherService extends CipherFactory {
    @Value('${encryption.key}')
        encryptionKey

    def encrypt(decrypted) {
        encrypt(encryptionKey, decrypted)
    }

    def decrypt(encrypted) {
        decrypt(encryptionKey, encrypted)
    }
}
