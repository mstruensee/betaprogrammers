package com.betaprogrammers.dotcom

import com.betaprogrammers.core.liveconfig.context.LiveConfigAnnotationConfigEmbeddedWebApplicationContext
import com.betaprogrammers.core.liveconfig.context.LiveConfigContextInitializer
import org.springframework.boot.ResourceBanner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.cache.annotation.EnableCaching
import org.springframework.core.io.ClassPathResource
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication(scanBasePackages = "com.betaprogrammers.**.**.config")
@EnableJpaRepositories("com.betaprogrammers.**.**.repository")
@EnableConfigurationProperties
@EnableScheduling
@EnableCaching
class Application {
    static void main(String... args) {
        def application = new SpringApplication(Application)
        application.applicationContextClass = LiveConfigAnnotationConfigEmbeddedWebApplicationContext
        application.setBanner(new ResourceBanner(new ClassPathResource("banner.txt")))
        application.addInitializers(new LiveConfigContextInitializer())
        application.run(args)
    }
}
