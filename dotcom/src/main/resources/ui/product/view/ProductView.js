define(function (require) {
    const Global = require("_global/global")
    const ProductTemplate = require("text!product/template/product-template.dust")
    const ProductCreateView = require("product/view/ProductCreateView")

    return Backbone.View.extend({
        render: function () {
            this.$el.html(Global.renderTemplate(this.model.id, ProductTemplate, this.model.attributes))
        },
        events: {
            "click .btn-edit": "edit"
        },
        edit: function (event) {
            new ProductCreateView({collection: this.collection, model: this.model})
        }
    })
})