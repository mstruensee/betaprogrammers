define(function (require) {
    const ProductView = require("product/view/ProductView")

    return Backbone.View.extend({
        el: "product-collection-view",
        initialize: function () {
            this.listenTo(this.collection, "sync", () => this.render())

            this.listenTo(this.collection, "sort", () => this.render())
        },
        render: function () {
            this.$el.empty()
            this.collection.each((model) => {
                const productView = new ProductView({collection: this.collection, model: model})
                productView.render()
                this.$el.append(productView.$el)
            })
        }
    })
})

