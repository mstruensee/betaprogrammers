define(function (require) {
    const Global = require("_global/global")
    const ProductAppTemplate = require("text!product/template/product-app-template.dust")
    const ProductCollection = require("product/collection/ProductCollection")
    const ProductCollectionView = require("product/view/ProductCollectionView")
    const SearchCollectionView = require("_global/search/view/SearchCollectionView")
    const ProductCreateView = require("product/view/ProductCreateView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "product-app-view",
        events: {
            "click .btn-create": "create"
            //"click .sortable": "sort"
        },
        initialize: function () {
            this.spinner = new SpinnerView()
            this.spinner.start()

            const productCollection = new ProductCollection()

            const SearchProductCollectionView = SearchCollectionView.extend({
                createEnabled: true,
                search: function (event) {
                    const filter = $(event.target).val()
                    new ProductCollectionView({collection: new ProductCollection(this.filter(filter))}).render()
                }
            })

            const promise = productCollection.fetch()

            $.when(promise).done(() => {
                this.render()
                new SearchProductCollectionView({collection: productCollection}).render()
                new ProductCollectionView({collection: productCollection}).render()
                this.collection = productCollection
                this.spinner.stop()
            })
        },
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, ProductAppTemplate, null))
            this.hideAllTriangles()
        },
        create: function () {
            new ProductCreateView({collection: this.collection})
        },
        sort: function (event) {
            const target = $(event.target)
            this.collection.sortByField(target.data("sort"))
            this.toggleTriangle(target)
        },
        toggleTriangle: function (target) {
            const top = "glyphicon-triangle-top"
            const bottom = "glyphicon-triangle-bottom"

            this.hideAllTriangles()

            const triangle = target.find("span") // should be 1

            if (triangle.hasClass(top)) {
                triangle.removeClass(top).addClass(bottom)
            } else {
                triangle.removeClass(bottom).addClass(top)
            }
            triangle.removeClass("hidden")
        },
        hideAllTriangles: function () {
            $("[class*=glyphicon-triangle]").each(function () {
                $(this).addClass("hidden")
            })
        }
    })
})