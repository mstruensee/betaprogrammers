define(function (require) {
    const Global = require("_global/global")
    const ProductTemplate = require("text!product/template/product-create-template.dust")
    const ProductModel = require("product/model/ProductModel")
    const UnsavedChangesView = require("_global/modal/view/UnsavedChangesView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "product-create-view",
        C: {
            MODAL_PRODUCT_CREATE: "#modal-product-create",
            MODAL_PRODUCT_CREATE_BACKDROP: "modal-product-create-backdrop",

            NAME: {
                MODEL_VAR: "name",
                SELECTOR: "#name"
            },
            REPORTING_PRODUCT: {
                MODEL_VAR: "reportingProduct",
                SELECTOR: "#reportingProduct"
            },
            CATEGORY: {
                MODEL_VAR: "category",
                SELECTOR: "#category"
            },
            STAT_LINE_OF_BUSINESS: {
                MODEL_VAR: "statLineOfBusiness",
                SELECTOR: "#statLineOfBusiness"
            },
            PRODUCT_GROUP: {
                MODEL_VAR: "groupOrIndividual",
                SELECTOR: "#groupOrIndividual"
            },
            AUSTRALIA_LUMP_SUM_DI: {
                MODEL_VAR: "australiaLumpSumDI",
                SELECTOR: "#australiaLumpSumDI"
            },
            PRODUCT_TYPE: {
                MODEL_VAR: "productType",
                SELECTOR: "#productType"
            },

            SERVER_ERROR: "Unable to edit/save product. Please contact administrator."
        },
        events: {
            "click .btn-save-product": "save",
            "click .btn-save-product-cancel": "cancel",

            "propertychange #name": "validateName",
            "keyup #name": "validateName",
            "input #name": "validateName",
            "paste #name": "validateName",
            "change #name": "validateName",

            "propertychange #reportingProduct": "validateReportingProduct",
            "keyup #reportingProduct": "validateReportingProduct",
            "input #reportingProduct": "validateReportingProduct",
            "paste #reportingProduct": "validateReportingProduct",
            "change #reportingProduct": "validateReportingProduct",

            "propertychange #category": "validateCategory",
            "keyup #category": "validateCategory",
            "input #category": "validateCategory",
            "paste #category": "validateCategory",
            "change #category": "validateCategory",

            "propertychange #statLineOfBusiness": "validateStatLineOfBusiness",
            "keyup #statLineOfBusiness": "validateStatLineOfBusiness",
            "input #statLineOfBusiness": "validateStatLineOfBusiness",
            "paste #statLineOfBusiness": "validateStatLineOfBusiness",
            "change #statLineOfBusiness": "validateStatLineOfBusiness",

            "propertychange #groupOrIndividual": "validategroupOrIndividual",
            "keyup #groupOrIndividual": "validategroupOrIndividual",
            "input #groupOrIndividual": "validategroupOrIndividual",
            "paste #groupOrIndividual": "validategroupOrIndividual",
            "change #groupOrIndividual": "validategroupOrIndividual",

            "propertychange #australiaLumpSumDI": "validateAustraliaLumpSumDI",
            "keyup #australiaLumpSumDI": "validateAustraliaLumpSumDI",
            "input #australiaLumpSumDI": "validateAustraliaLumpSumDI",
            "paste #australiaLumpSumDI": "validateAustraliaLumpSumDI",
            "change #australiaLumpSumDI": "validateAustraliaLumpSumDI",

            "propertychange #productType": "validateProductType",
            "keyup #productType": "validateProductType",
            "input #productType": "validateProductType",
            "paste #productType": "validateProductType",
            "change #productType": "validateProductType"
        },
        initialize: function (options) {
            this.spinner = new SpinnerView()
            this.spinner.start()
            //this is a modal, we want it to be at the BODY so other css does not effect its style
            //and so we can force z-index overlapping easily due to having the same parent element
            // ... the modal-backdrop is added to the body by default.
            if (!this.$el.length) {
                $("body").append("<" + this.$el.selector + ">")
                this.$el = $(this.$el.selector)
                //as a result of this, we need to delegate the events after the $el is set...
                this.delegateEvents()
            }

            this.data = {}
            this.collection = options.collection
            if (!this.model) {
                this.model = new ProductModel()
            }

            const promise = $.ajax({
                url: "/product/initialize",
                type: "GET"
            })
            promise.done((response) => {
                this.data.groupOrIndividualEnum = response.groupOrIndividualEnum
                this.data.australiaLumpSumDIEnum = response.australiaLumpSumDIEnum
                this.data.productTypeEnum = response.productTypeEnum
                this.data.statLineOfBusinessEnum = response.statLineOfBusinessEnum
            })

            $.when(promise).done(() => this.render())
        },
        render: function () {
            if (this.model.get("id")) {
                $.each(this.model.attributes, (key, value) => this.data[key] = value)
            }
            this.$el.html(Global.renderTemplate(this.$el.selector, ProductTemplate, this.data))

            //render select pickers
            this.renderSelectPickers()

            //apply initial validation
            this.validateName()
            this.validateReportingProduct()
            this.validateCategory()
            this.validateStatLineOfBusiness()
            this.validategroupOrIndividual()
            this.validateAustraliaLumpSumDI()
            this.validateProductType()

            this.disable()

            this.show()
        },
        show: function () {
            const modal = $(this.C.MODAL_PRODUCT_CREATE)
            modal.on("shown.bs.modal", function () {
                const focus = $("#reportingProduct")
                focus.focus()
                if (focus.val()) {
                    focus[0].setSelectionRange(focus.val().length, focus.val().length)
                }
            })

            modal.on("show.bs.modal", () => {
                const zIndex = 1040 + (10 * $(".modal:visible").length)
                $(this.C.MODAL_PRODUCT_CREATE).css("z-index", zIndex)

                setTimeout(() => $(".modal-backdrop").not(".modal-backdrop-stacked").css("z-index", zIndex - 1).addClass("modal-backdrop-stacked").attr("id", this.C.MODAL_PRODUCT_CREATE_BACKDROP), 0)

                $(this).unbind("show.bs.modal")
            })

            this.spinner.stop()
            modal.modal("show")
        },
        renderSelectPickers: function () {
            $("[class^=selectpicker]").selectpicker()
        },
        save: function (event) {
            const btnSaveProduct = $(event.target)
            const disabledItems = $("#modal-product-create [class*=btn],#modal-product-create input[type=text]")

            if (this.valid()) {
                btnSaveProduct.button("loading")
                disabledItems.prop("disabled", true)

                this.model.set(this.C.REPORTING_PRODUCT.MODEL_VAR, $(this.C.REPORTING_PRODUCT.SELECTOR).val())
                this.model.set(this.C.NAME.MODEL_VAR, $(this.C.NAME.SELECTOR).val())
                this.model.set(this.C.CATEGORY.MODEL_VAR, $(this.C.CATEGORY.SELECTOR).val())
                this.model.set(this.C.STAT_LINE_OF_BUSINESS.MODEL_VAR, {"id": $(this.C.STAT_LINE_OF_BUSINESS.SELECTOR).find(":selected").attr("data-tokens"), "value": $(this.C.STAT_LINE_OF_BUSINESS.SELECTOR).find(":selected").val()})
                this.model.set(this.C.PRODUCT_GROUP.MODEL_VAR, {"id": $(this.C.PRODUCT_GROUP.SELECTOR).find(":selected").attr("data-tokens"), "value": $(this.C.PRODUCT_GROUP.SELECTOR).find(":selected").val()})
                this.model.set(this.C.AUSTRALIA_LUMP_SUM_DI.MODEL_VAR, {"id": $(this.C.AUSTRALIA_LUMP_SUM_DI.SELECTOR).find(":selected").attr("data-tokens"), "value": $(this.C.AUSTRALIA_LUMP_SUM_DI.SELECTOR).find(":selected").val()})
                this.model.set(this.C.PRODUCT_TYPE.MODEL_VAR, {"id": $(this.C.PRODUCT_TYPE.SELECTOR).find(":selected").attr("data-tokens"), "value": $(this.C.PRODUCT_TYPE.SELECTOR).find(":selected").val()})

                let verb = "created"
                if (this.model.get("id")) {
                    verb = "updated"
                }
                const promise = this.model.save()
                promise.done((response) => {
                    this.collection.add(response)
                    BAM.success("Product successfully " + verb + ".")
                    this.cancel()
                    btnSaveProduct.button("reset")
                    disabledItems.prop("disabled", false)
                })
                promise.fail((response) => {
                    if (response.status === 409) {
                        $(response.responseJSON.selector).addClass("has-error")
                        BAM.error(response.responseJSON.bam)

                        btnSaveProduct.button("reset")
                        disabledItems.prop("disabled", false)

                        this.disable()
                    } else {
                        btnSaveProduct.button("reset")
                        disabledItems.prop("disabled", false)
                        BAM.error(this.C.SERVER_ERROR)
                    }
                })
            }
        },
        cancel: function () {
            if (this.changed()) this.unsavedChanges()
            else this.closeModalAndBackdrop()
        },
        closeModalAndBackdrop: function () {
            const modal = $(this.C.MODAL_PRODUCT_CREATE)
            modal.on("hidden.bs.modal", () => {
                this.$el.html("")
                this.$el.unbind()
                $("#" + this.C.MODAL_PRODUCT_CREATE_BACKDROP).remove()
            })
            modal.modal("hide")
        },
        changed: function () {
            if (!this.model.get("id")) return false

            return $(this.C.NAME.SELECTOR).val() !== this.model.get(this.C.NAME.MODEL_VAR)
                || $(this.C.REPORTING_PRODUCT.SELECTOR).val() !== this.model.get(this.C.REPORTING_PRODUCT.MODEL_VAR)
                || $(this.C.CATEGORY.SELECTOR).val() !== this.model.get(this.C.CATEGORY.MODEL_VAR)
                || $(this.C.STAT_LINE_OF_BUSINESS.SELECTOR).find(":selected").val() !== (this.model.get(this.C.STAT_LINE_OF_BUSINESS.MODEL_VAR) ? this.model.get(this.C.STAT_LINE_OF_BUSINESS.MODEL_VAR).value : "")
                || $(this.C.PRODUCT_GROUP.SELECTOR).find(":selected").val() !== (this.model.get(this.C.PRODUCT_GROUP.MODEL_VAR) ? this.model.get(this.C.PRODUCT_GROUP.MODEL_VAR).value : "")
                || $(this.C.AUSTRALIA_LUMP_SUM_DI.SELECTOR).find(":selected").val() !== (this.model.get(this.C.AUSTRALIA_LUMP_SUM_DI.MODEL_VAR) ? this.model.get(this.C.AUSTRALIA_LUMP_SUM_DI.MODEL_VAR).value : "")
                || $(this.C.PRODUCT_TYPE.SELECTOR).find(":selected").val() !== (this.model.get(this.C.PRODUCT_TYPE.MODEL_VAR) ? this.model.get(this.C.PRODUCT_TYPE.MODEL_VAR).value : "")

        },
        valid: function () {
            const hasError = $(this.C.MODAL_PRODUCT_CREATE).find(".has-error")
            if (this.model.get("id")) return !hasError.length && this.changed()

            return !hasError.length
        },
        disable: function () {
            const save = $(".btn-save-product")
            if (this.valid())  save.prop("disabled", false)
            else save.prop("disabled", true)

        },
        validateName: function (event) {
            if (event && event.which == 13) $(".btn-save-product").click()
            else {
                const field = $(this.C.NAME.SELECTOR)
                if (field.val() == "" && field.hasClass("required")) field.parents(".form-group-name").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-name").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validateCategory: function (event) {
            if (event && event.which == 13) $(".btn-save-product").click()
            else {
                const field = $(this.C.CATEGORY.SELECTOR)
                if (field.val() == "" && field.hasClass("required")) field.parents(".form-group-category").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-category").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validateReportingProduct: function (event) {
            if (event && event.which == 13) $(".btn-save-product").click()
            else {
                const field = $(this.C.REPORTING_PRODUCT.SELECTOR)
                if (field.val() == "" && field.hasClass("required")) field.parents(".form-group-reportingProduct").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-reportingProduct").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validateStatLineOfBusiness: function (event) {
            if (event && event.which == 13) $(".btn-save-product").click()
            else {
                const field = $(this.C.STAT_LINE_OF_BUSINESS.SELECTOR)
                if (field.find("option:selected").val() == "" && field.hasClass("required")) field.parents(".form-group-statLineOfBusiness").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-statLineOfBusiness").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validategroupOrIndividual: function (event) {
            if (event && event.which == 13) $(".btn-save-product").click()
            else {
                const field = $(this.C.PRODUCT_GROUP.SELECTOR)
                if (field.find("option:selected").val() == "" && field.hasClass("required")) field.parents(".form-group-groupOrIndividual").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-groupOrIndividual").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validateAustraliaLumpSumDI: function (event) {
            if (event && event.which == 13) $(".btn-save-product").click()
            else {
                const field = $(this.C.AUSTRALIA_LUMP_SUM_DI.SELECTOR)
                if (field.find("option:selected").val() == "" && field.hasClass("required")) field.parents(".form-group-australiaLumpSumDI").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-australiaLumpSumDI").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validateProductType: function (event) {
            if (event && event.which == 13) $(".btn-save-product").click()
            else {
                const field = $(this.C.PRODUCT_TYPE.SELECTOR)
                if (field.find("option:selected").val() == "" && field.hasClass("required")) field.parents(".form-group-productType").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-productType").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        unsavedChanges: function () {
            new UnsavedChangesView({model: this})
        }
    })
})