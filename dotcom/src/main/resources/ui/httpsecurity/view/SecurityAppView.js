define(function (require) {
    const Global = require("_global/global")

    const SecurityAppTemplate = require("text!httpsecurity/template/security-app-template.dust")

    const SecurityCollection = require("httpsecurity/collection/SecurityCollection")
    const SecurityCollectionView = require("httpsecurity/view/SecurityCollectionView")
    const SearchCollectionView = require("_global/search/view/SearchCollectionView")
    const SecurityCreateView = require("httpsecurity/view/SecurityCreateView")

    return Backbone.View.extend({
        el: "security-app-view",
        initialize: function () {
            const securityCollection = new SecurityCollection()

            const SearchSecurityCollectionView = SearchCollectionView.extend({
                search: function (event) {
                    const filter = $(event.target).val()
                    new SecurityCollectionView({collection: new SecurityCollection(this.filter(filter))}).render()
                }
            })

            const promise = securityCollection.fetch()
            promise.done(function () {
                this.render()
                new SearchSecurityCollectionView({collection: securityCollection, createEnabled: true}).render()
                new SecurityCollectionView({collection: securityCollection}).render()
                this.collection = securityCollection
            }.bind(this))
        },
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, SecurityAppTemplate, null))
            this.hideAllTriangles()
        },
        events: {
            "click .btn-create": "create"
            //"click .sortable": "sort"
        },
        create: function () {
            new SecurityCreateView({collection: this.collection})
        },
        sort: function (event) {
            const target = $(event.target)
            this.collection.sortByField(target.data("sort"))
            this.toggleTriangle(target)
        },
        toggleTriangle: function (target) {
            const top = "glyphicon-triangle-top"
            const bottom = "glyphicon-triangle-bottom"

            this.hideAllTriangles()

            const triangle = target.find("span") // should be 1

            if (triangle.hasClass(top)) {
                triangle.removeClass(top).addClass(bottom)
            } else {
                triangle.removeClass(bottom).addClass(top)
            }
            triangle.removeClass("hidden")
        },
        hideAllTriangles: function () {
            $("[class*=glyphicon-triangle]").each(function () {
                $(this).addClass("hidden")
            })
        }
    })
})