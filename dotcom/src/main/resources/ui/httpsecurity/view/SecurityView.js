define(function (require) {
    const Global = require("_global/global")
    const SecurityTemplate = require("text!httpsecurity/template/security-template.dust")
    const SecurityCreateView = require("httpsecurity/view/SecurityCreateView")

    return Backbone.View.extend({
        render: function () {
            this.$el.html(Global.renderTemplate(this.model.id, SecurityTemplate, this.model.attributes))
        },
        events: {
            "click .btn-edit": "edit"
        },
        edit: function (event) {
            new SecurityCreateView({collection: this.collection, model: this.model})
        }
    })
})