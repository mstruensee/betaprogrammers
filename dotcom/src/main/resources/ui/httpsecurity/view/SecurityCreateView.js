define(function (require) {
    const Global = require("_global/global")
    const SecurityTemplate = require("text!httpsecurity/template/security-create-template.dust")
    const SecurityModel = require("httpsecurity/model/SecurityModel")
    const UnsavedChangesView = require("_global/modal/view/UnsavedChangesView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "security-create-view",
        C: {
            MODAL_SECURITY_CREATE: "#modal-security-create",
            MODAL_SECURITY_CREATE_BACKDROP: "modal-security-create-backdrop",
            MATCHER: "#matcher",
            TYPE: "#type",
            AUTHORITY: "#authority",
            SERVER_ERROR: "Server error. Please contact administrator."
        },
        events: {
            "click .btn-save-security": "save",
            "click .btn-save-security-cancel": "cancel",
            "click .btn-save-security-delete": "delete",

            "propertychange input[type=text]": "empty",
            "keyup input[type=text]": "empty",
            "input input[type=text]": "empty",
            "paste input[type=text]": "empty",
            "change input[type=text]": "empty",

            "propertychange #type": "disable",
            "keyup #type": "disable",
            "input #type": "disable",
            "paste #type": "disable",
            "change #type": "disable",

            "propertychange #authority": "disable",
            "keyup #authority": "disable",
            "input #authority": "disable",
            "paste #authority": "disable",
            "change #authority": "disable",

            "change select": "showAuthority"
        },
        initialize: function (options) {
            this.spinner = new SpinnerView()
            this.spinner.start()
            //this is a modal, we want it to be at the BODY so other css does not effect its style
            //and so we can force z-index overlapping easily due to having the same parent element
            // ... the modal-backdrop is added to the body by default.
            if (!this.$el.length) {
                $("body").append("<" + this.$el.selector + ">")
                this.$el = $(this.$el.selector)
                //as a result of this, we need to delegate the events after the $el is set...
                this.delegateEvents()
            }

            this.data = {}
            this.collection = options.collection
            if (!this.model) {
                this.model = new SecurityModel()
            }

            const typesPromise = $.ajax({
                url: "/httpsecurity/types",
                type: "GET"
            })

            const authorityPromise = $.ajax({
                url: "/role/roles",
                type: "GET"
            })

            typesPromise.done(function (response) {
                this.data.types = response
            }.bind(this))
            typesPromise.fail(function () {
                this.data.types = []
            }.bind(this))

            authorityPromise.done(function (response) {
                this.data.roles = response
            }.bind(this))
            authorityPromise.fail(function () {
                this.data.roles = []
            }.bind(this))

            $.when(authorityPromise, typesPromise).done(function () {
                this.render()
            }.bind(this))
        },
        render: function () {
            if (this.model.get("id")) {
                $.each(this.model.attributes, function (key, value) {
                    this.data[key] = value
                }.bind(this))
            }

            this.$el.html(Global.renderTemplate(this.$el.selector, SecurityTemplate, this.data))

            //apply initial validation
            $(this.C.MODAL_SECURITY_CREATE).find("input").each(function (index, input) {
                if ($(input).val() === "") {
                    $(input).parent().addClass("has-error").removeClass("has-success")
                }
                else {
                    $(input).parent().addClass("has-success").removeClass("has-error")
                }
            })
            this.checked()

            this.show()
        },
        show: function () {
            const modal = $(this.C.MODAL_SECURITY_CREATE)
            modal.on("shown.bs.modal", function () {
                const matcher = $("#matcher")
                matcher.focus()
                if (matcher.val()) {
                    matcher[0].setSelectionRange(matcher.val().length, matcher.val().length)
                }
            })

            modal.on("show.bs.modal", function () {
                const zIndex = 1040 + (10 * $(".modal:visible").length)
                $(this.C.MODAL_SECURITY_CREATE).css("z-index", zIndex)

                setTimeout(function () {
                    $(".modal-backdrop").not(".modal-backdrop-stacked").css("z-index", zIndex - 1).addClass("modal-backdrop-stacked").attr("id", this.C.MODAL_SECURITY_CREATE_BACKDROP)
                }.bind(this), 0)

                $(this).unbind("show.bs.modal")
            }.bind(this))

            this.spinner.stop()
            modal.modal("show")
        },
        save: function (event) {
            const btnSaveSecurity = $(event.target)
            const disabledItems = $("#modal-security-create [class*=btn],#modal-security-create input[type=text],#type,#authority")

            if (this.valid()) {
                btnSaveSecurity.button("loading")
                disabledItems.prop("disabled", true)

                this.model.set("matcher", $("#matcher").val())
                this.model.set("type", $("#type").find(":selected").text())
                this.model.set("authority", null)

                if (this.model.get("type") !== "PERMIT_ALL") {
                    this.model.set("authority", $("#authority").find(":selected").text())
                }

                let created = "created"
                if (this.model.get("id")) {
                    created = "updated"
                }

                const promise = this.model.save()
                promise.done(function (response) {
                    this.collection.add(response)
                    BAM.success("Security successfully " + created + ".")
                    this.cancel()
                    btnSaveSecurity.button("reset")
                    disabledItems.prop("disabled", false)
                }.bind(this))
                promise.fail(function (response) {
                    if (response.status === 409) {
                        $(response.responseJSON.selector).addClass("has-error")
                        BAM.error(response.responseJSON.bam)

                        btnSaveSecurity.button("reset")
                        disabledItems.prop("disabled", false)

                        this.disable()
                    } else {
                        btnSaveSecurity.button("reset")
                        disabledItems.prop("disabled", false)
                        BAM.error(this.C.SERVER_ERROR)
                    }
                }.bind(this))
            }
        },
        cancel: function () {
            if (this.changed()) {
                this.unsavedChanges()
            } else {
                this.closeModalAndBackdrop()
            }
        },
        closeModalAndBackdrop: function () {
            const modal = $(this.C.MODAL_SECURITY_CREATE)
            modal.on("hidden.bs.modal", function () {
                this.$el.html("")
                this.$el.unbind()
                $("#" + this.C.MODAL_SECURITY_CREATE_BACKDROP).remove()
            }.bind(this))
            modal.modal("hide")
        },
        changed: function () {
            if (!this.model.get("id")) {
                return false
            }

            return this.selectedChanged() || this.inputsChanged()
        },
        selectedChanged: function () {
            const type = $("#type")
            const typeChanged = type.find(":selected").text() != this.model.get("type")
            if (type.find(":selected").text() === "PERMIT_ALL") {
                return typeChanged
            }

            return $("#authority").find(":selected").text() != this.model.get("authority") || typeChanged
        },
        inputsChanged: function () {
            let changed = false
            $(this.C.MODAL_SECURITY_CREATE).find("input[type=text]").each(function (index, input) {
                const value = this.model.get($(input).attr("id"))
                //if (value) {
                if ($(input).val() !== value) {
                    changed = true
                    return false//break from each loop
                }
                //}
            }.bind(this))
            return changed
        },
        valid: function () {
            const hasError = $(this.C.MODAL_SECURITY_CREATE).find(".has-error")
            if (this.model.get("id")) {
                const changed = this.changed()
                return !hasError.length && changed
            }
            return !hasError.length
        },
        disable: function () {
            const save = $(".btn-save-security")
            if (this.valid()) {
                save.prop("disabled", false)
            } else {
                save.prop("disabled", true)
            }
        },
        empty: function (event) {
            if (event.which == 13) {
                $(".btn-save-security").click()
            } else {
                let target = $(event.target)
                if (target.val() == "" && target.hasClass("required")) {
                    target.parent().addClass("has-error").removeClass("has-success")
                } else {
                    target.parent().removeClass("has-error").addClass("has-success")
                }
                this.disable()
            }
        },
        checked: function () {
            const roles = $(".form-group-roles")
            if ($(".form-group-roles input[type=checkbox]:checked").length) {
                roles.addClass("has-success").removeClass("has-error")
            } else {
                roles.addClass("has-error").removeClass("has-success")
            }
            this.disable()
        },
        unsavedChanges: function () {
            new UnsavedChangesView({model: this})
        },
        delete: function (event) {
            const btnDeleteSecurity = $(event.target)
            const disabledItems = $("#modal-security-create [class*=btn],#modal-security-create input[type=text],#type,#authority")
            btnDeleteSecurity.button("loading")
            disabledItems.prop("disabled", true)

            this.model.destroy({
                success: function () {
                    BAM.success("Security successfully deleted.")
                    this.cancel()
                    this.collection.remove(this.model)
                    this.collection.trigger("sync") // we are manually calling this so the "remove" trigger doesn't happen until the success from the server.
                    btnDeleteSecurity.button("reset")
                    disabledItems.prop("disabled", false)
                }.bind(this),
                error: function () {
                    disabledItems.prop("disabled", false)
                    BAM.error(this.C.SERVER_ERROR)
                }
            })
        },
        showAuthority: function (event) {
            if ($(event.target).find(":selected").text() === "PERMIT_ALL") {
                $(".security-authority").addClass("hidden")
                $(".security-authority:selected").removeAttr("selected")
            } else {
                $(".security-authority").removeClass("hidden")
            }
        }
    })
})