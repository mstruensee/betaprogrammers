define(function (require) {
    const SecurityView = require("httpsecurity/view/SecurityView")

    return Backbone.View.extend({
        el: "security-collection-view",
        initialize: function () {
            this.listenTo(this.collection, "sync", function () {
                this.render()
            }.bind(this))
            this.listenTo(this.collection, "sort", function () {
                this.render()
            }.bind(this))
        },
        render: function () {
            this.$el.empty()
            this.collection.each(function (model) {
                const securityView = new SecurityView({collection: this.collection, model: model})
                securityView.render()
                this.$el.append(securityView.$el)
            }.bind(this))
        }
    })
})

