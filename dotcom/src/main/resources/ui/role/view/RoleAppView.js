define(function (require) {
    const Global = require("_global/global")
    const RoleAppTemplate = require("text!role/template/role-app-template.dust")
    const RoleCollection = require("role/collection/RoleCollection")
    const RoleCollectionView = require("role/view/RoleCollectionView")
    const SearchCollectionView = require("_global/search/view/SearchCollectionView")
    const RoleCreateView = require("role/view/RoleCreateView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "role-app-view",
        events: {
            "click .btn-create": "create"
            //"click .sortable": "sort"
        },
        initialize: function () {
            this.spinner = new SpinnerView()
            this.spinner.start()

            const roleCollection = new RoleCollection()

            const SearchRoleCollectionView = SearchCollectionView.extend({
                search: function (event) {
                    const filter = $(event.target).val()
                    new RoleCollectionView({collection: new RoleCollection(this.filter(filter))}).render()
                }
            })

            const promise = roleCollection.fetch()

            $.when(promise).done(function () {
                this.render()
                new SearchRoleCollectionView({collection: roleCollection}).render()
                new RoleCollectionView({collection: roleCollection}).render()
                this.collection = roleCollection
                this.spinner.stop()
            }.bind(this))
        },
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, RoleAppTemplate, null))
            this.hideAllTriangles()
        },
        create: function () {
            new RoleCreateView({collection: this.collection})
        },
        sort: function (event) {
            const target = $(event.target)
            this.collection.sortByField(target.data("sort"))
            this.toggleTriangle(target)
        },
        toggleTriangle: function (target) {
            const top = "glyphicon-triangle-top"
            const bottom = "glyphicon-triangle-bottom"

            this.hideAllTriangles()

            const triangle = target.find("span") // should be 1

            if (triangle.hasClass(top)) {
                triangle.removeClass(top).addClass(bottom)
            } else {
                triangle.removeClass(bottom).addClass(top)
            }
            triangle.removeClass("hidden")
        },
        hideAllTriangles: function () {
            $("[class*=glyphicon-triangle]").each(function () {
                $(this).addClass("hidden")
            })
        }
    })
})