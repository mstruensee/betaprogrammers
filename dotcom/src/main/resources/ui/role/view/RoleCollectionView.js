define(function (require) {
    const RoleView = require("role/view/RoleView")

    return Backbone.View.extend({
        el: "role-collection-view",
        initialize: function () {
            this.listenTo(this.collection, "sync", function () {
                this.render()
            }.bind(this))
            this.listenTo(this.collection, "sort", function () {
                this.render()
            }.bind(this))
        },
        render: function () {
            this.$el.empty()
            this.collection.each(function (model) {
                const roleView = new RoleView({collection: this.collection, model: model})
                roleView.render()
                this.$el.append(roleView.$el)
            }.bind(this))
        }
    })
})

