define(function (require) {
    const Global = require("_global/global")
    const RoleTemplate = require("text!role/template/role-create-template.dust")
    const RoleModel = require("role/model/RoleModel")
    const UnsavedChangesView = require("_global/modal/view/UnsavedChangesView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "role-create-view",
        C: {
            MODAL_ROLE_CREATE: "#modal-role-create",
            MODAL_ROLE_CREATE_BACKDROP: "modal-role-create-backdrop",
            NAME: "#name",
            DESCRIPTION: "#description",

            SERVER_ERROR: "Unable to save role. Please contact administrator."
        },
        events: {
            "click .btn-save-role": "save",
            "click .btn-save-role-cancel": "cancel",

            "propertychange input[type=text]": "empty",
            "keyup input[type=text]": "empty",
            "input input[type=text]": "empty",
            "paste input[type=text]": "empty",
            "change input[type=text]": "empty",

            "propertychange #description": "disable",
            "keyup #description": "disable",
            "input #description": "disable",
            "paste #description": "disable",
            "change #description": "disable"

        },
        initialize: function (options) {
            this.spinner = new SpinnerView()
            this.spinner.start()
            //this is a modal, we want it to be at the BODY so other css does not effect its style
            //and so we can force z-index overlapping easily due to having the same parent element
            // ... the modal-backdrop is added to the body by default.
            if (!this.$el.length) {
                $("body").append("<" + this.$el.selector + ">")
                this.$el = $(this.$el.selector)
                //as a result of this, we need to delegate the events after the $el is set...
                this.delegateEvents()
            }

            this.collection = options.collection
            if (!this.model) {
                this.model = new RoleModel()
            }

            //no promises so call render
            this.render()
        },
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, RoleTemplate, this.model.attributes))

            //apply initial validation
            $(this.C.MODAL_ROLE_CREATE).find("input").each(function (index, input) {
                if ($(input).val() === "") {
                    $(input).parent().addClass("has-error").removeClass("has-success")
                }
                else {
                    $(input).parent().addClass("has-success").removeClass("has-error")
                }
            })

            this.disable()

            this.show()
        },
        show: function () {
            const modal = $(this.C.MODAL_ROLE_CREATE)
            modal.on("shown.bs.modal", function () {
                const name = $("#name")
                name.focus()
                if (name.val()) {
                    name[0].setSelectionRange(name.val().length, name.val().length)
                }
            })

            modal.on("show.bs.modal", function () {
                const zIndex = 1040 + (10 * $(".modal:visible").length)
                $(this.C.MODAL_ROLE_CREATE).css("z-index", zIndex)

                setTimeout(function () {
                    $(".modal-backdrop").not(".modal-backdrop-stacked").css("z-index", zIndex - 1).addClass("modal-backdrop-stacked").attr("id", this.C.MODAL_ROLE_CREATE_BACKDROP)
                }.bind(this), 0)

                $(this).unbind("show.bs.modal")
            }.bind(this))

            this.spinner.stop()
            modal.modal("show")
        },
        save: function (event) {
            const btnSaveRole = $(event.target)
            const disabledItems = $("#modal-role-create [class*=btn],#modal-role-create input,textarea")

            if (this.valid()) {
                btnSaveRole.button("loading")
                disabledItems.prop("disabled", true)

                this.model.set("name", $(this.C.NAME).val())
                this.model.set("description", $(this.C.DESCRIPTION).val())

                let created = "created"
                if (this.model.get("id")) {
                    created = "updated"
                }

                const promise = this.model.save()
                promise.done(function (response) {
                    this.collection.add(response)
                    BAM.success("Role successfully " + created + ".")
                    this.cancel()
                    btnSaveRole.button("reset")
                    disabledItems.prop("disabled", false)
                }.bind(this))
                promise.fail(function (response) {
                    if (response.status === 409) {
                        $(response.responseJSON.selector).addClass("has-error")
                        BAM.error(response.responseJSON.bam)

                        btnSaveRole.button("reset")
                        disabledItems.prop("disabled", false)

                        this.disable()
                    } else {
                        btnSaveRole.button("reset")
                        disabledItems.prop("disabled", false)
                        BAM.error(this.C.SERVER_ERROR)
                    }
                }.bind(this))
            }
        },
        cancel: function () {
            if (this.change()) {
                this.unsavedChanges()
            } else {
                this.closeModalAndBackdrop()
            }
        },
        closeModalAndBackdrop: function () {
            const modal = $(this.C.MODAL_ROLE_CREATE)
            modal.on("hidden.bs.modal", function () {
                this.$el.html("")
                this.$el.unbind()
                $("#" + this.C.MODAL_ROLE_CREATE_BACKDROP).remove()
            }.bind(this))
            modal.modal("hide")
        },
        change: function () {
            if (!this.model.get("id")) {
                return false
            }

            return this.inputsChanged()
        },
        inputsChanged: function () {
            let changed = false
            $(this.C.MODAL_ROLE_CREATE).find("input[type=text],textarea").each(function (index, input) {
                const value = this.model.get($(input).attr("id")) ? this.model.get($(input).attr("id")) : "" // setting to blank handles undefined, since description is not required during create
                //if (value) {
                if ($(input).val() !== value) {
                    changed = true
                    return false//break from each loop
                }
                //}
            }.bind(this))
            return changed
        },
        valid: function () {
            const hasError = $(this.C.MODAL_ROLE_CREATE).find(".has-error")
            if (this.model.get("id")) {
                const changed = this.change()
                return !hasError.length && changed
            }
            return !hasError.length
        },
        disable: function () {
            const save = $(".btn-save-role")
            if (this.valid()) {
                save.prop("disabled", false)
            } else {
                save.prop("disabled", true)
            }
        },
        empty: function (event) {
            if (event.which == 13) {
                $(".btn-save-role").click()
            } else {
                let target = $(event.target)
                if (target.val() == "" && target.hasClass("required")) {
                    target.parent().addClass("has-error").removeClass("has-success")
                } else {
                    target.parent().removeClass("has-error").addClass("has-success")
                }
                this.disable()
            }
        },
        unsavedChanges: function () {
            new UnsavedChangesView({model: this})
        }
    })
})