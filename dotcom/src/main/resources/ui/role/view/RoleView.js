define(function (require) {
    const Global = require("_global/global")
    const RoleTemplate = require("text!role/template/role-template.dust")
    const RoleCreateView = require("role/view/RoleCreateView")

    return Backbone.View.extend({
        render: function () {
            this.$el.html(Global.renderTemplate(this.model.id, RoleTemplate, this.model.attributes))
        },
        events: {
            "click .btn-edit": "edit"
        },
        edit: function (event) {
            new RoleCreateView({collection: this.collection, model: this.model})
        }
    })
})