[#assign module="role"]
[#assign css="/ui/${module}/css/${module}.css"]
[#assign js=""]
[#include "../_global/template/header.ftl"]

<role-app-view></role-app-view>

[#include "../_global/template/footer.ftl"]