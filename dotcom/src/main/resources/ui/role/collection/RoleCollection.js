define(function (require) {
    const RoleModel = require("role/model/RoleModel")

    return Backbone.Collection.extend({
        model: RoleModel,
        url: "/role/roles",
        sort_key: "name",
        sort_desc: false,
        comparator: function (item) {
            return item.get(this.sort_key)
        },
        sortByField: function (fieldName) {
            this.sort_key = fieldName
            this.sort()
            if (this.sort_desc) {
                this.models.reverse()
            }
            this.sort_desc = !this.sort_desc
            this.trigger("sort")
        }
    })
})