define(function (require) {
    var Global = require("_global/global")

    return Backbone.View.extend({
        el: "view1",
        initialize: function () {
            console.log("view1 initialized")
        },
        render: function () {
            this.$el.html("view1")
            return this
        }
    })
})