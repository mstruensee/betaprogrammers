define(function (require) {
    var Backbone = require("backbone")
    var Global = require("_global/global")
    var Template = require("text!test/dust/danger-view.dust")

    return Backbone.View.extend({
        el: "danger-view",
        initialize: function () {
            console.log("DangerView.initialize()")
        },
        render: function () {
            console.log("DangerView.render()")
            this.$el.html(Global.renderTemplate(this.$el.selector, Template, null))
        }
    })
})