define(function (require) {
    var Backbone = require("backbone")
    var SecurityView = require("test/view/SecurityView")

    return Backbone.View.extend({
        el: "security-collection-view",
        initialize: function () {
            this.listenTo(this.collection, "updated", this.render)
        },
        render: function () {
            this.$el.empty()

            //$.each(this.collection, function (model) {
            //    var securityView = new SecurityView({model: model}).render()
            //    this.$el.append(securityView.$el)
            //}.bind(this))
            this.collection.each(function (model) {
                const securityView = new SecurityView({model: model})
                securityView.render()
                this.$el.append(securityView.$el)
            }.bind(this))
        }
    })
})

