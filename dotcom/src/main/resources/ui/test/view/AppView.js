define(function (require) {
    var Backbone = require("backbone")
    var Global = require("_global/global")
    var AppViewTemplate = require("text!test/dust/app-view.dust")

    var SecurityCollection = require("test/collection/SecurityCollection")
    var SecurityCollectionView = require("test/view/SecurityCollectionView")
    var SearchCollectionView = require("_global/search/view/SearchCollectionView")
    var PrimaryView = require("test/view/PrimaryView")
    var WarningView = require("test/view/WarningView")
    var DangerView = require("test/view/DangerView")

    return Backbone.View.extend({
        el: "app-view",
        initialize: function () {
            var securityCollection = new SecurityCollection()

            var SearchSecurityCollectionView = SearchCollectionView.extend({
                search: function (event) {
                    var filter = $(event.target).val()
                    new SecurityCollectionView({collection: new SecurityCollection(this.filter(filter))}).render()
                }
                //create: function () {
                //    new CreateView({collection: securityCollection}).render()
                //}
            })

            securityCollection.fetch({
                success: function () {
                    new SearchSecurityCollectionView({collection: securityCollection}).render()
                    new SecurityCollectionView({collection: securityCollection}).render()
                }
            })
        },
        render: function () {
            this.$el.html(Global.renderTemplate("app-view", AppViewTemplate, null))
            new PrimaryView().render()
            new WarningView().render()
            new DangerView().render()
        }
    })
})