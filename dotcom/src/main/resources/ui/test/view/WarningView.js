define(function (require) {
    var Backbone = require("backbone")
    var Global = require("_global/global")
    var Template = require("text!test/dust/warning-view.dust")

    return Backbone.View.extend({
        el: "warning-view",
        initialize: function () {
            console.log("WarningView.initialize()")
        },
        render: function () {
            console.log("WarningView.render()")
            this.$el.html(Global.renderTemplate(this.$el.selector, Template, null))
        }
    })
})