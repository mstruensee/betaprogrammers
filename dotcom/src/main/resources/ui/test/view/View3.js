define(function (require) {
    var Global = require("_global/global")

    return Backbone.View.extend({
        el: "view3",
        initialize: function () {
            console.log("view3 initialized")
        },
        render: function () {
            this.$el.html("view3")
            return this
        }
    })
})