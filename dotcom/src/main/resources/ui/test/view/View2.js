define(function (require) {
    var Global = require("_global/global")

    return Backbone.View.extend({
        el: "view2",
        initialize: function () {
            console.log("view2 initialized")
        },
        render: function () {
            this.$el.html("view2")
            return this
        }
    })
})