define(function (require) {
    var Backbone = require("backbone")
    var Global = require("_global/global")
    var Template = require("text!test/dust/primary-view.dust")

    return Backbone.View.extend({
        el: "workflow-view",
        initialize: function () {
            this.currentStep = 1
            this.steps = {}

        },

        render: function () {
            console.log("PrimaryView.render()")
            this.$el.html(Global.renderTemplate("primary-view", Template, null))
            return this
        },
        events: {
            "click btn-workflow-next": "next"
        },
        next: function () {

        }
    })
})