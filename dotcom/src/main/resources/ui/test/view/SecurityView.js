define(function (require) {
    require("colorbox")
    var Global = require("_global/global")
    var Template = require("text!test/dust/security-view.dust")
//    var EditView = require("httpsecurity/views/SecurityEditView")

    return Backbone.View.extend({
        events: {
            "click .btn-edit": "edit"
        },
        render: function () {
            this.$el.html(Global.renderTemplate("security", Template, this.model.attributes))
        }
        //edit: function () {
        //    console.log("SecurityView.edit called")
        //    new EditView({model: this.model}).render()
        //}
    })
})