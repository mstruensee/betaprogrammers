define(function (require) {
    var Backbone = require("backbone")
    var Global = require("_global/global")
    var Template = require("text!test/dust/primary-view.dust")

    return Backbone.View.extend({
        el: "primary-view",
        initialize: function () {
            console.log("PrimaryView.initialize()")
        },
        render: function () {
            console.log("PrimaryView.render()")
            this.$el.html(Global.renderTemplate(this.$el.selector, Template, null))
        }
    })
})