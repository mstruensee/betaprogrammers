define(function (require) {
    var Backbone = require("backbone")
    var Model = require("test/model/SecurityModel")

    return Backbone.Collection.extend({
        model: Model,
        url: "/httpsecurity/security"
    })
})