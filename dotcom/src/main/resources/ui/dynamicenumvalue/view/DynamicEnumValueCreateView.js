define(function (require) {
    const Global = require("_global/global")
    const DynamicEnumValueTemplate = require("text!dynamicenumvalue/template/dynamicenumvalue-create-template.dust")
    const DynamicEnumValueModel = require("dynamicenumvalue/model/DynamicEnumValueModel")
    const UnsavedChangesView = require("_global/modal/view/UnsavedChangesView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "dynamicenumvalue-create-view",
        C: {
            MODAL_DYNAMIC_ENUM_VALUE_CREATE: "#modal-dynamicenumvalue-create",
            MODAL_DYNAMIC_ENUM_VALUE_CREATE_BACKDROP: "modal-dynamicenumvalue-create-backdrop",

            VALUE: {
                MODEL_VAR: "value",
                SELECTOR: "#value"
            },
            CODE: {
                MODEL_VAR: "code",
                SELECTOR: "#code"
            },

            SERVER_ERROR: "Please contact administrator."
        },
        events: {
            "click .btn-save-dynamicenumvalue": "save",
            "click .btn-save-dynamicenumvalue-cancel": "cancel",

            "propertychange #value": "validateValue",
            "keyup #value": "validateValue",
            "input #value": "validateValue",
            "paste #value": "validateValue",
            "change #value": "validateValue",

            "propertychange #code": "validateCode",
            "keyup #code": "validateCode",
            "input #code": "validateCode",
            "paste #code": "validateCode",
            "change #code": "validateCode"

        },
        initialize: function (options) {
            this.spinner = new SpinnerView()
            this.spinner.start()
            //this is a modal, we want it to be at the BODY so other css does not effect its style
            //and so we can force z-index overlapping easily due to having the same parent element
            // ... the modal-backdrop is added to the body by default.
            if (!this.$el.length) {
                $("body").append("<" + this.$el.selector + ">")
                this.$el = $(this.$el.selector)
                //as a result of this, we need to delegate the events after the $el is set...
                this.delegateEvents()
            }

            this.data = {}
            this.collection = options.collection
            if (!this.model) this.model = new DynamicEnumValueModel()
            this.render()

        },
        render: function () {
            if (this.model.get("id")) $.each(this.model.attributes, (key, value) => this.data[key] = value)
            this.$el.html(Global.renderTemplate(this.$el.selector, DynamicEnumValueTemplate, this.data))

            //apply initial validation
            this.validateValue()
            this.validateCode()

            this.disable()

            this.show()
        },
        show: function () {
            const modal = $(this.C.MODAL_DYNAMIC_ENUM_VALUE_CREATE)
            modal.on("shown.bs.modal", function () {
                const focus = $("#value")
                focus.focus()
                if (focus.val()) {
                    focus[0].setSelectionRange(focus.val().length, focus.val().length)
                }
            })

            modal.on("show.bs.modal", () => {
                const zIndex = 1040 + (10 * $(".modal:visible").length)
                $(this.C.MODAL_DYNAMIC_ENUM_VALUE_CREATE).css("z-index", zIndex)

                setTimeout(() => $(".modal-backdrop").not(".modal-backdrop-stacked").css("z-index", zIndex - 1).addClass("modal-backdrop-stacked").attr("id", this.C.MODAL_DYNAMIC_ENUM_VALUE_CREATE_BACKDROP), 0)

                $(this).unbind("show.bs.modal")
            })

            this.spinner.stop()
            modal.modal("show")
        },
        save: function (event) {
            const btnSaveDynamicEnumValue = $(event.target)
            const disabledItems = $("#modal-dynamicenumvalue-create [class*=btn],#modal-dynamicenumvalue-create input[type=text]")

            if (this.valid()) {
                btnSaveDynamicEnumValue.button("loading")
                disabledItems.prop("disabled", true)

                let verb = "created"
                if (this.model.get("id")) verb = "updated"

                this.model.set(this.C.VALUE.MODEL_VAR, $(this.C.VALUE.SELECTOR).val())
                this.model.set(this.C.CODE.MODEL_VAR, $(this.C.CODE.SELECTOR).val())

                const promise = this.model.save()
                promise.done((response) => {
                    this.collection.add(response)
                    BAM.success("Dynamic enum value successfully " + verb + ".")
                    this.cancel()
                    btnSaveDynamicEnumValue.button("reset")
                    disabledItems.prop("disabled", false)
                })
                promise.fail((response) => {
                    if (response.status === 409) {
                        $(response.responseJSON.selector).addClass("has-error")
                        BAM.error(response.responseJSON.bam)

                        btnSaveDynamicEnumValue.button("reset")
                        disabledItems.prop("disabled", false)

                        this.disable()
                    } else {
                        btnSaveDynamicEnumValue.button("reset")
                        disabledItems.prop("disabled", false)
                        BAM.error(this.C.SERVER_ERROR)
                    }
                })
            }
        },
        cancel: function () {
            if (this.changed()) this.unsavedChanges()
            else this.closeModalAndBackdrop()

        },
        closeModalAndBackdrop: function () {
            const modal = $(this.C.MODAL_DYNAMIC_ENUM_VALUE_CREATE)
            modal.on("hidden.bs.modal", () => {
                this.$el.html("")
                this.$el.unbind()
                $("#" + this.C.MODAL_DYNAMIC_ENUM_VALUE_CREATE_BACKDROP).remove()
            })
            modal.modal("hide")
        },
        changed: function () {
            if (!this.model.get("id")) return false
            return $(this.C.VALUE.SELECTOR).val() !== this.model.get(this.C.VALUE.MODEL_VAR)
                || $(this.C.CODE.SELECTOR).val() !== this.model.get(this.C.CODE.MODEL_VAR)
        },
        valid: function () {
            const hasError = $(this.C.MODAL_DYNAMIC_ENUM_VALUE_CREATE).find(".has-error")
            if (this.model.get("id")) {
                return !hasError.length && this.changed()
            }
            return !hasError.length
        },
        disable: function () {
            const save = $(".btn-save-dynamicenumvalue")
            if (this.valid()) save.prop("disabled", false)
            else save.prop("disabled", true)
        },
        validateValue: function (event) {
            if (event && event.which == 13) $(".btn-save-dynamicenumvalue").click()
            else {
                const value = $(this.C.VALUE.SELECTOR)
                if (value.val() == "" && value.hasClass("required")) value.parents(".form-group-value").addClass("has-error").removeClass("has-success")
                else value.parents(".form-group-value").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validateCode: function (event) {
            if (event && event.which == 13) $(".btn-save-dynamicenumvalue").click()
            else this.disable()

        },
        unsavedChanges: function () {
            new UnsavedChangesView({model: this})
        }
    })
})