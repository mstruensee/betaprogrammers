define(function (require) {
    const Global = require("_global/global")
    const DynamicEnumValueAppTemplate = require("text!dynamicenumvalue/template/dynamicenumvalue-app-template.dust")
    const DynamicEnumValueCollection = require("dynamicenumvalue/collection/DynamicEnumValueCollection")
    const DynamicEnumValueCollectionView = require("dynamicenumvalue/view/DynamicEnumValueCollectionView")
    const SearchCollectionView = require("_global/search/view/SearchCollectionView")
    const DynamicEnumValueCreateView = require("dynamicenumvalue/view/DynamicEnumValueCreateView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "dynamicenumvalue-app-view",
        events: {
            "click .btn-create": "create"
            //"click .sortable": "sort"
        },
        initialize: function () {
            this.spinner = new SpinnerView()
            this.spinner.start()

            const dynamicEnumValueCollection = new DynamicEnumValueCollection()

            const SearchDynamicEnumValueCollectionView = SearchCollectionView.extend({
                createEnabled: true,
                search: function (event) {
                    const filter = $(event.target).val()
                    new DynamicEnumValueCollectionView({collection: new DynamicEnumValueCollection(this.filter(filter))}).render()
                }
            })

            const promise = dynamicEnumValueCollection.fetch()

            $.when(promise).done(() => {
                this.render()
                new SearchDynamicEnumValueCollectionView({collection: dynamicEnumValueCollection}).render()
                new DynamicEnumValueCollectionView({collection: dynamicEnumValueCollection}).render()
                this.collection = dynamicEnumValueCollection
                this.spinner.stop()
            })
        },
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, DynamicEnumValueAppTemplate, null))
            this.hideAllTriangles()
        },
        create: function () {
            new DynamicEnumValueCreateView({collection: this.collection})
        },
        sort: function (event) {
            const target = $(event.target)
            this.collection.sortByField(target.data("sort"))
            this.toggleTriangle(target)
        },
        toggleTriangle: function (target) {
            const top = "glyphicon-triangle-top"
            const bottom = "glyphicon-triangle-bottom"

            this.hideAllTriangles()

            const triangle = target.find("span") // should be 1

            if (triangle.hasClass(top)) {
                triangle.removeClass(top).addClass(bottom)
            } else {
                triangle.removeClass(bottom).addClass(top)
            }
            triangle.removeClass("hidden")
        },
        hideAllTriangles: function () {
            $("[class*=glyphicon-triangle]").each(function () {
                $(this).addClass("hidden")
            })
        }
    })
})