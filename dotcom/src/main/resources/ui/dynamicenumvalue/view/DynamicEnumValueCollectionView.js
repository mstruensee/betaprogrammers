define(function (require) {
    const DynamicEnumValueView = require("dynamicenumvalue/view/DynamicEnumValueView")

    return Backbone.View.extend({
        el: "dynamicenumvalue-collection-view",
        initialize: function () {
            this.listenTo(this.collection, "sync", () => this.render())

            this.listenTo(this.collection, "sort", () => this.render())
        },
        render: function () {
            this.$el.empty()
            this.collection.each((model) => {
                const dynamicEnumValueView = new DynamicEnumValueView({collection: this.collection, model: model})
                dynamicEnumValueView.render()
                this.$el.append(dynamicEnumValueView.$el)
            })
        }
    })
})

