define(function (require) {
    const Global = require("_global/global")
    const DynamicEnumValueTemplate = require("text!dynamicenumvalue/template/dynamicEnumValue-template.dust")
    const DynamicEnumValueCreateView = require("dynamicenumvalue/view/DynamicEnumValueCreateView")

    return Backbone.View.extend({
        render: function () {
            this.$el.html(Global.renderTemplate(this.model.id, DynamicEnumValueTemplate, this.model.attributes))
        },
        events: {
            "click .btn-edit": "edit"
        },
        edit: function (event) {
            new DynamicEnumValueCreateView({collection: this.collection, model: this.model})
        }
    })
})