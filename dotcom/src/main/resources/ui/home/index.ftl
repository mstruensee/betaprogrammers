[#ftl]
[#assign module="home"]
[#include "../_global/template/header.ftl"]

[#if _global.applicationName??]
<h1>${_global.applicationName}</h1>
[/#if]

<br/>
[#if time??]time: ${time}<br/>[/#if]
[#if liveConfigMe??]liveConfigMe: ${liveConfigMe}<br/>[/#if]

[#if _global.roles??]
Roles: [#list _global.roles as role]${role} [/#list]
[/#if]

[#include "../_global/template/footer.ftl"]
