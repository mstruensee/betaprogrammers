define(function (require) {
    const LiveConfigView = require("liveconfig/view/LiveConfigView")

    return Backbone.View.extend({
        el: "liveconfig-collection-view",
        initialize: function () {
            this.listenTo(this.collection, "sync", function () {
                this.render()
            }.bind(this))
            this.listenTo(this.collection, "sort", function () {
                this.render()
            }.bind(this))
        },
        render: function () {
            this.$el.empty()
            this.collection.each(function (model) {
                const liveconfigView = new LiveConfigView({collection: this.collection, model: model})
                liveconfigView.render()
                this.$el.append(liveconfigView.$el)
            }.bind(this))
        }
    })
})

