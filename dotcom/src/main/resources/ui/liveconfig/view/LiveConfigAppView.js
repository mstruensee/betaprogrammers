define(function (require) {
    const Global = require("_global/global")
    const LiveConfigAppTemplate = require("text!liveconfig/template/liveconfig-app-template.dust")
    const LiveConfigCollection = require("liveconfig/collection/LiveConfigCollection")
    const LiveConfigCollectionView = require("liveconfig/view/LiveConfigCollectionView")
    const SearchCollectionView = require("_global/search/view/SearchCollectionView")
    const LiveConfigCreateView = require("liveconfig/view/LiveConfigCreateView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "liveconfig-app-view",
        events: {
            //"click .sortable": "sort"
        },
        initialize: function () {
            this.spinner = new SpinnerView()
            this.spinner.start()

            const liveconfigCollection = new LiveConfigCollection()

            const SearchLiveConfigCollectionView = SearchCollectionView.extend({
                createEnabled: false,
                search: function (event) {
                    const filter = $(event.target).val()
                    new LiveConfigCollectionView({collection: new LiveConfigCollection(this.filter(filter))}).render()
                }
            })

            const promise = liveconfigCollection.fetch()

            $.when(promise).done(function () {
                this.render()
                new SearchLiveConfigCollectionView({collection: liveconfigCollection}).render()
                new LiveConfigCollectionView({collection: liveconfigCollection}).render()
                this.collection = liveconfigCollection
                this.spinner.stop()
            }.bind(this))
        },
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, LiveConfigAppTemplate, null))
            this.hideAllTriangles()
        },
        sort: function (event) {
            const target = $(event.target)
            this.collection.sortByField(target.data("sort"))
            this.toggleTriangle(target)
        },
        toggleTriangle: function (target) {
            const top = "glyphicon-triangle-top"
            const bottom = "glyphicon-triangle-bottom"

            this.hideAllTriangles()

            const triangle = target.find("span") // should be 1

            if (triangle.hasClass(top)) {
                triangle.removeClass(top).addClass(bottom)
            } else {
                triangle.removeClass(bottom).addClass(top)
            }
            triangle.removeClass("hidden")
        },
        hideAllTriangles: function () {
            $("[class*=glyphicon-triangle]").each(function () {
                $(this).addClass("hidden")
            })
        }
    })
})