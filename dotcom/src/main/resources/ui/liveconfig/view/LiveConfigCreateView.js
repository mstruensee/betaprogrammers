define(function (require) {
    const Global = require("_global/global")
    const LiveConfigTemplate = require("text!liveconfig/template/liveconfig-create-template.dust")
    const LiveConfigModel = require("liveconfig/model/LiveConfigModel")
    const UnsavedChangesView = require("_global/modal/view/UnsavedChangesView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "liveconfig-create-view",
        C: {
            MODAL_LIVECONFIG_CREATE: "#modal-liveconfig-create",
            MODAL_LIVECONFIG_CREATE_BACKDROP: "modal-liveconfig-create-backdrop",
            NAME: "#name",
            LIVE: "#liveValue",

            SERVER_ERROR: "Unable to edit property. Please contact administrator."
        },
        events: {
            "click .btn-save-liveconfig": "save",
            "click .btn-save-liveconfig-cancel": "cancel",
            "click .btn-reset-liveconfig": "reset",

            "propertychange input[type=text]": "empty",
            "keyup input[type=text]": "empty",
            "input input[type=text]": "empty",
            "paste input[type=text]": "empty",
            "change input[type=text]": "empty"

        },
        initialize: function (options) {
            this.spinner = new SpinnerView()
            this.spinner.start()
            //this is a modal, we want it to be at the BODY so other css does not effect its style
            //and so we can force z-index overlapping easily due to having the same parent element
            // ... the modal-backdrop is added to the body by default.
            if (!this.$el.length) {
                $("body").append("<" + this.$el.selector + ">")
                this.$el = $(this.$el.selector)
                //as a result of this, we need to delegate the events after the $el is set...
                this.delegateEvents()
            }

            this.collection = options.collection
            if (!this.model) {
                this.model = new LiveConfigModel()
            }

            //no promises so call render
            this.render()
        },
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, LiveConfigTemplate, this.model.attributes))

            //apply initial validation
            $(this.C.MODAL_LIVECONFIG_CREATE).find("input").each(function (index, input) {
                if ($(input).val() === "") {
                    $(input).parent().addClass("has-error").removeClass("has-success")
                }
                else {
                    $(input).parent().addClass("has-success").removeClass("has-error")
                }
            })

            this.disable()

            this.show()
        },
        show: function () {
            const modal = $(this.C.MODAL_LIVECONFIG_CREATE)
            modal.on("shown.bs.modal", function () {
                const liveValue = $("#liveValue")
                liveValue.focus()
                if (liveValue.val()) {
                    liveValue[0].setSelectionRange(liveValue.val().length, liveValue.val().length)
                }
            })

            modal.on("show.bs.modal", function () {
                const zIndex = 1040 + (10 * $(".modal:visible").length)
                $(this.C.MODAL_LIVECONFIG_CREATE).css("z-index", zIndex)

                setTimeout(function () {
                    $(".modal-backdrop").not(".modal-backdrop-stacked").css("z-index", zIndex - 1).addClass("modal-backdrop-stacked").attr("id", this.C.MODAL_LIVECONFIG_CREATE_BACKDROP)
                }.bind(this), 0)

                $(this).unbind("show.bs.modal")
            }.bind(this))

            this.spinner.stop()
            modal.modal("show")
        },
        reset: function (event) {
            $(this.C.LIVE).val("")
            this.save(event)
        },
        save: function (event) {
            const btnSaveLiveConfig = $(event.target)
            const disabledItems = $("#modal-liveconfig-create [class*=btn],#modal-liveconfig-create input[type=text]")

            if (this.valid()) {
                btnSaveLiveConfig.button("loading")
                disabledItems.prop("disabled", true)
                const liveValue = $(this.C.LIVE).val()
                let verb = liveValue !== "" ? "updated" : "reset"

                this.model.set("liveValue", liveValue)

                const promise = this.model.save()
                promise.done(function (response) {
                    BAM.success("Property successfully " + verb + ".")
                    this.cancel()
                    btnSaveLiveConfig.button("reset")
                    disabledItems.prop("disabled", false)
                }.bind(this))
                promise.fail(function (response) {
                    btnSaveLiveConfig.button("reset")
                    disabledItems.prop("disabled", false)
                    BAM.error(this.C.SERVER_ERROR)
                }.bind(this))
            }
        },
        cancel: function () {
            if (this.change()) {
                this.unsavedChanges()
            } else {
                this.closeModalAndBackdrop()
            }
        },
        closeModalAndBackdrop: function () {
            const modal = $(this.C.MODAL_LIVECONFIG_CREATE)
            modal.on("hidden.bs.modal", function () {
                this.$el.html("")
                this.$el.unbind()
                $("#" + this.C.MODAL_LIVECONFIG_CREATE_BACKDROP).remove()
            }.bind(this))
            modal.modal("hide")
        },
        change: function () {
            if (!this.model.get("id")) {
                return false
            }

            return this.inputsChanged()
        },
        inputsChanged: function () {
            let changed = false
            $(this.C.MODAL_LIVECONFIG_CREATE).find("input[type=text],textarea").each(function (index, input) {
                const value = this.model.get($(input).attr("id")) ? this.model.get($(input).attr("id")) : "" // setting to blank handles undefined, since live value is not required during create
                if ($(input).val() !== value) {
                    changed = true
                    return false//break from each loop
                }
            }.bind(this))
            return changed
        },
        valid: function () {
            const hasError = $(this.C.MODAL_LIVECONFIG_CREATE).find(".has-error")
            if (this.model.get("id")) {
                const changed = this.change()
                return !hasError.length && changed
            }
            return !hasError.length
        },
        disable: function () {
            const save = $(".btn-save-liveconfig, .btn-reset-liveconfig")
            if (this.valid()) {
                save.prop("disabled", false)
            } else {
                save.prop("disabled", true)
            }

            const reset = $(".btn-reset-liveconfig")
            if (this.model.get("liveValue")) {
                reset.prop("disabled", false)
            } else {
                reset.prop("disabled", true)
            }
        },
        empty: function (event) {
            if (event.which == 13) {
                $(".btn-save-liveconfig").click()
            } else {
                let target = $(event.target)
                if (target.val() == "" && target.hasClass("required")) {
                    target.parent().addClass("has-error").removeClass("has-success")
                } else {
                    target.parent().removeClass("has-error").addClass("has-success")
                }
                this.disable()
            }
        },
        unsavedChanges: function () {
            new UnsavedChangesView({model: this})
        }
    })
})