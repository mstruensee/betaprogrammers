define(function (require) {
    const Global = require("_global/global")
    const LiveConfigTemplate = require("text!liveconfig/template/liveconfig-template.dust")
    const LiveConfigCreateView = require("liveconfig/view/LiveConfigCreateView")

    return Backbone.View.extend({
        render: function () {
            this.$el.html(Global.renderTemplate(this.model.id, LiveConfigTemplate, this.model.attributes))
        },
        events: {
            "click .btn-edit": "edit"
        },
        edit: function (event) {
            new LiveConfigCreateView({collection: this.collection, model: this.model})
        }
    })
})