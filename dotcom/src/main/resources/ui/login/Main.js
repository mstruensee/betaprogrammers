define(function (require) {
    require("bootstrap")

    const error = $("#error-message")
    const logout = $("#logout-message")

    if (error.length) {
        BAM.window({type: "danger", message: error.html()})
        error.remove()
    }

    if (logout.length) {
        BAM.window({type: "success", message: logout.html()})
        logout.remove()
    }

    $("input[name=username]").focus()

    $("input[name=username], input[name=password]").bind("propertychange keyup input paste change", function () {
        let disabled = true
        if (allFilled()) {
            disabled = false
        }
        $(".btn-login").prop("disabled", disabled)
    })

    function allFilled() {
        let filled = true
        $("input").each(function () {
            if ($(this).val() == "") filled = false
        })
        return filled
    }
})