[#ftl]
[#assign module="login"]
<html>
<head>
    <title>[#if module??]${module?capitalize}[#else]${_global.applicationName}[/#if]</title>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/webjars/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="/ui/login/css/login.css">

    <script>var module = "${module}/Main";</script>
    <script data-main="/ui/_global/requireConfig" src="/webjars/requirejs/2.2.0/require.min.js"></script>

    <script src="/ui/login/js/login.js"></script>
</head>
<body>
[#if error??]
<div id="error-message" hidden>${error}</div>
[/#if]
[#if logout??]
<div id="logout-message" hidden>${logout}</div>
[/#if]

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="wrap">
                <form class="login" action="/login" method="POST">
                    <input type="text" placeholder="Username" name="username" autocomplete="off"/>
                    <input type="password" placeholder="Password" name="password"/>
                    <input type="submit" value="Sign In" class="btn btn-danger btn-sm btn-login" disabled/>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>





