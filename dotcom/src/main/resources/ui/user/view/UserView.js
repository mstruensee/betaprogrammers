define(function (require) {
    const Global = require("_global/global")
    const UserTemplate = require("text!user/template/user-template.dust")
    const UserCreateView = require("user/view/UserCreateView")

    return Backbone.View.extend({
        render: function () {
            this.$el.html(Global.renderTemplate(this.model.id, UserTemplate, this.model.attributes))
        },
        events: {
            "click .btn-edit": "edit"
        },
        edit: function (event) {
            new UserCreateView({collection: this.collection, model: this.model})
        }
    })
})