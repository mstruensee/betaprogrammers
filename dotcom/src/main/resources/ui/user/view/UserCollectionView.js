define(function (require) {
    const UserView = require("user/view/UserView")

    return Backbone.View.extend({
        el: "user-collection-view",
        initialize: function () {
            this.listenTo(this.collection, "sync", () => this.render())
            this.listenTo(this.collection, "sort", () => this.render())
        },
        render: function () {
            this.$el.empty()
            this.collection.each((model) => {
                const userView = new UserView({collection: this.collection, model: model})
                userView.render()
                this.$el.append(userView.$el)
            })
        }
    })
})

