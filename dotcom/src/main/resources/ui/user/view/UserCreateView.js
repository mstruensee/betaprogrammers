define(function (require) {
    const Global = require("_global/global")
    const UserTemplate = require("text!user/template/user-create-template.dust")
    const UserModel = require("user/model/UserModel")
    const UnsavedChangesView = require("_global/modal/view/UnsavedChangesView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "user-create-view",
        C: {
            MODAL_USER_CREATE: "#modal-user-create",
            MODAL_USER_CREATE_BACKDROP: "modal-user-create-backdrop",

            USERNAME: {
                MODEL_VAR: "username",
                SELECTOR: "#username"
            },
            PASSWORD: {
                MODEL_VAR: "password",
                SELECTOR: "#password"
            },
            CONFIRM_PASSWORD: {
                SELECTOR: "#confirm-password"
            },
            FIRSTNAME: {
                MODEL_VAR: "firstname",
                SELECTOR: "#firstname"
            },
            LASTNAME: {
                MODEL_VAR: "lastname",
                SELECTOR: "#lastname"
            },
            EMAIL: {
                MODEL_VAR: "email",
                SELECTOR: "#email"
            },
            DISABLED: {
                MODEL_VAR: "disabled",
                SELECTOR: "#disabled"
            },
            ROLES: {
                MODEL_VAR: "roles",
                SELECTOR: "#roles"
            },

            SERVER_ERROR: "Unable to save/update user. Please contact administrator."
        },
        events: {
            "click .btn-save-user": "save",
            "click .btn-save-user-cancel": "cancel",

            "propertychange #username": "validateUsername",
            "keyup #username": "validateUsername",
            "input #username": "validateUsername",
            "paste #username": "validateUsername",
            "change #username": "validateUsername",

            "propertychange #password": "validatePassword",
            "keyup #password": "validatePassword",
            "input #password": "validatePassword",
            "paste #password": "validatePassword",
            "change #password": "validatePassword",

            "propertychange #confirm-password": "validateConfirmPassword",
            "keyup #confirm-password": "validateConfirmPassword",
            "input #confirm-password": "validateConfirmPassword",
            "paste #confirm-password": "validateConfirmPassword",
            "change #confirm-password": "validateConfirmPassword",

            "propertychange #firstname": "validateFirstname",
            "keyup #firstname": "validateFirstname",
            "input #firstname": "validateFirstname",
            "paste #firstname": "validateFirstname",
            "change #firstname": "validateFirstname",

            "propertychange #lastname": "validateLastname",
            "keyup #lastname": "validateLastname",
            "input #lastname": "validateLastname",
            "paste #lastname": "validateLastname",
            "change #lastname": "validateLastname",

            "propertychange #email": "validateEmail",
            "keyup #email": "validateEmail",
            "input #email": "validateEmail",
            "paste #email": "validateEmail",
            "change #email": "validateEmail",

            "propertychange #disabled": "validateDisabled",
            "keyup #disabled": "validateDisabled",
            "input #disabled": "validateDisabled",
            "paste #disabled": "validateDisabled",
            "change #disabled": "validateDisabled",

            "itemAdded #roles": "validateRoles",
            "itemRemoved #roles": "validateRoles"
        },
        initialize: function (options) {
            this.spinner = new SpinnerView()
            this.spinner.start()
            //this is a modal, we want it to be at the BODY so other css does not effect its style
            //and so we can force z-index overlapping easily due to having the same parent element
            // ... the modal-backdrop is added to the body by default.
            if (!this.$el.length) {
                $("body").append("<" + this.$el.selector + ">")
                this.$el = $(this.$el.selector)
                //as a result of this, we need to delegate the events after the $el is set...
                this.delegateEvents()
            }

            this.data = {}
            this.collection = options.collection
            if (!this.model) {
                this.model = new UserModel()
            }
            const promise = $.ajax({
                url: "/user/initialize",
                type: "GET"
            })
            promise.done((response) => this.data.availableRoles = response.roles)

            $.when(promise).done(() => this.render())
        },
        render: function () {
            if (this.model.get("id")) {
                $.each(this.model.attributes, (key, value) => this.data[key] = value)
            }
            this.$el.html(Global.renderTemplate(this.$el.selector, UserTemplate, this.data))

            //render tags
            this.renderTags()

            //apply initial validation
            this.validateUsername()
            this.validatePassword()
            this.validateConfirmPassword()
            this.validateFirstname()
            this.validateLastname()
            this.validateEmail()
            this.validateDisabled()
            this.validateRoles()

            this.disable()

            this.show()
        },
        show: function () {
            const modal = $(this.C.MODAL_USER_CREATE)
            modal.on("shown.bs.modal", function () {
                const username = $("#username")
                username.focus()
                const length = username.val().length
                username[0].setSelectionRange(length, length)
            })

            modal.on("show.bs.modal", () => {
                const zIndex = 1040 + (10 * $(".modal:visible").length)
                $(this.C.MODAL_USER_CREATE).css("z-index", zIndex)

                setTimeout(() => $(".modal-backdrop").not(".modal-backdrop-stacked").css("z-index", zIndex - 1).addClass("modal-backdrop-stacked").attr("id", this.C.MODAL_USER_CREATE_BACKDROP), 0)

                $(this).unbind("show.bs.modal")
            })

            this.spinner.stop()
            modal.modal("show")
        },
        renderTags: function () {
            const roles = $(this.C.ROLES.SELECTOR)

            roles.tagsinput({
                itemValue: "id",
                itemText: "name",
                tagClass: "label label-primary",
                typeahead: {
                    displayKey: "name",
                    source: this.data.availableRoles,
                    afterSelect: function () {
                        this.$element[0].value = ""
                    }
                },
                freeInput: false
            })

            if (this.model.get("id")) {
                $.each(this.model.get(this.C.ROLES.MODEL_VAR), function (index, value) {
                    roles.tagsinput("add", value)
                })
            }
        },
        save: function (event) {
            const btnSaveUser = $(".btn-save-user")
            const disabledItems = $("#modal-user-create [class*=btn],#modal-user-create input")
            const tags = $(".bootstrap-tagsinput")

            if (this.valid()) {
                btnSaveUser.button("loading")
                disabledItems.prop("disabled", true)
                tags.addClass("disabled")

                this.model.set(this.C.USERNAME.MODEL_VAR, $(this.C.USERNAME.SELECTOR).val())
                this.model.set(this.C.PASSWORD.MODEL_VAR, $(this.C.PASSWORD.SELECTOR).val())
                this.model.set(this.C.FIRSTNAME.MODEL_VAR, $(this.C.FIRSTNAME.SELECTOR).val())
                this.model.set(this.C.LASTNAME.MODEL_VAR, $(this.C.LASTNAME.SELECTOR).val())
                this.model.set(this.C.DISABLED.MODEL_VAR, $(this.C.DISABLED.SELECTOR).is(':checked'))
                this.model.set(this.C.EMAIL.MODEL_VAR, $(this.C.EMAIL.SELECTOR).val())
                this.model.set(this.C.ROLES.MODEL_VAR, $(this.C.ROLES.SELECTOR).tagsinput("items"))

                let created = "created"
                if (this.model.get("id")) {
                    created = "updated"
                }

                // TODO : refactor ... defer ??? fix save button not getting disabled ... this seems to be happening on all create 409s
                const promise = this.model.save()
                promise.done((response) => {
                    this.collection.add(response)
                    BAM.success("User successfully " + created + ".")
                    this.cancel()
                    btnSaveUser.button("reset")
                    disabledItems.prop("disabled", false)
                    tags.removeClass("disabled")
                })
                promise.fail((response) => {
                    if (response.status === 409) {
                        $(response.responseJSON.selector).addClass("has-error")
                        BAM.error(response.responseJSON.bam)

                        btnSaveUser.button("reset")
                        disabledItems.prop("disabled", false)
                        tags.removeClass("disabled")

                        this.disable()
                    } else {
                        btnSaveUser.button("reset")
                        disabledItems.prop("disabled", false)
                        tags.removeClass("disabled")

                        BAM.error(this.C.SERVER_ERROR)
                    }
                })
            }
        },
        cancel: function () {
            if (this.changed()) this.unsavedChanges()
            else this.closeModalAndBackdrop()
        },
        closeModalAndBackdrop: function () {
            const modal = $(this.C.MODAL_USER_CREATE)
            modal.on("hidden.bs.modal", () => {
                this.$el.html("")
                this.$el.unbind()
                $("#" + this.C.MODAL_USER_CREATE_BACKDROP).remove()
            })
            modal.modal("hide")
        },
        changed: function () {
            if (!this.model.get("id")) return false
            const modelRoles = this.model.get(this.C.ROLES.MODEL_VAR)
            const formRoles = $(this.C.ROLES.SELECTOR).tagsinput("items")

            return $(this.C.USERNAME.SELECTOR).val() !== this.model.get(this.C.USERNAME.MODEL_VAR)
                || $(this.C.FIRSTNAME.SELECTOR).val() !== this.model.get(this.C.FIRSTNAME.MODEL_VAR)
                || $(this.C.LASTNAME.SELECTOR).val() !== this.model.get(this.C.LASTNAME.MODEL_VAR)
                || $(this.C.EMAIL.SELECTOR).val() !== this.model.get(this.C.EMAIL.MODEL_VAR)
                || $(this.C.DISABLED.SELECTOR).is(':checked') !== this.model.get(this.C.DISABLED.MODEL_VAR)
                || JSON.stringify(modelRoles) !== JSON.stringify(formRoles)
        },
        valid: function () {
            const hasError = $(this.C.MODAL_USER_CREATE).find(".has-error")
            if (this.model.get("id")) return !hasError.length && this.changed()

            return !hasError.length
        },
        disable: function (event) {
            const save = $(".btn-save-user")
            if (this.valid()) save.prop("disabled", false)
            else save.prop("disabled", true)
        },
        validateUsername: function (event) {
            if (event && event.which == 13) $(".btn-save-user").click()
            else {
                const field = $(this.C.USERNAME.SELECTOR)
                if (field.val() == "" && field.hasClass("required")) field.parents(".form-group-username").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-username").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validatePassword: function (event) {
            if (event && event.which == 13) $(".btn-save-user").click()
            else {
                //TODO: add password requirements ... what are they going to be?
                const field = $(this.C.PASSWORD.SELECTOR)
                if (field.val() == "" && field.hasClass("required")) field.parents(".form-group-password").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-password").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validateConfirmPassword: function (event) {
            if (event && event.which == 13) $(".btn-save-user").click()
            else {
                const field = $(this.C.CONFIRM_PASSWORD.SELECTOR)
                const password = $(this.C.PASSWORD.SELECTOR)
                if ((field.val() != password.val() || field.val() == "" ) && field.hasClass("required")) field.parents(".form-group-confirm-password").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-confirm-password").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validateFirstname: function (event) {
            if (event && event.which == 13) $(".btn-save-user").click()
            else {
                const field = $(this.C.FIRSTNAME.SELECTOR)
                if (field.val() == "" && field.hasClass("required")) field.parents(".form-group-firstname").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-firstname").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validateLastname: function (event) {
            if (event && event.which == 13) $(".btn-save-user").click()
            else {
                const field = $(this.C.LASTNAME.SELECTOR)
                if (field.val() == "" && field.hasClass("required")) field.parents(".form-group-lastname").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-lastname").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validateEmail: function (event) {
            if (event && event.which == 13) $(".btn-save-user").click()
            else {
                const field = $(this.C.EMAIL.SELECTOR)
                const pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i)
                const valid = pattern.test(field.val())

                if (!valid && field.hasClass("required")) field.parents(".form-group-email").addClass("has-error").removeClass("has-success")
                else field.parents(".form-group-email").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validateDisabled: function (event) {
            if (event && event.which == 13) $(".btn-save-user").click()
            else {
                //nothing to do, not required ... still need to call disable in case it was changed
                this.disable()
            }

        },
        validateRoles: function (event) {
            //enter key is used to add the tags, so we dont want to submit the form here
            //if (event && event.which == 13) $(".btn-save-user").click()
            //else {
            const field = $(this.C.ROLES.SELECTOR)
            if (field.tagsinput("items").length == "" && field.hasClass("required")) field.parents(".form-group-roles").addClass("has-error").removeClass("has-success")
            else field.parents(".form-group-roles").removeClass("has-error").addClass("has-success")

            this.disable()
            //}
        },
        unsavedChanges: function () {
            new UnsavedChangesView({model: this})
        }
    })
})