define(function (require) {
    const Global = require("_global/global")
    const UserAppTemplate = require("text!user/template/user-app-template.dust")
    const UserCollection = require("user/collection/UserCollection")
    const UserCollectionView = require("user/view/UserCollectionView")
    const SearchCollectionView = require("_global/search/view/SearchCollectionView")
    const UserCreateView = require("user/view/UserCreateView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "user-app-view",
        events: {
            "click .btn-create": "create"
            //"click .sortable": "sort"
        },
        initialize: function () {
            this.spinner = new SpinnerView()
            this.spinner.start()

            const userCollection = new UserCollection()

            const SearchUserCollectionView = SearchCollectionView.extend({
                search: function (event) {
                    const filter = $(event.target).val()
                    new UserCollectionView({collection: new UserCollection(this.filter(filter))}).render()
                }
            })

            const promise = userCollection.fetch()

            $.when(promise).done(() => {
                    this.render()
                    new SearchUserCollectionView({collection: userCollection, createEnabled: true}).render()
                    new UserCollectionView({collection: userCollection}).render()
                    this.collection = userCollection
                    this.spinner.stop()
                }
            )
        },
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, UserAppTemplate, null))
            this.hideAllTriangles()
        },
        create: function () {
            new UserCreateView({collection: this.collection})
        },
        sort: function (event) {
            const target = $(event.target)
            this.collection.sortByField(target.data("sort"))
            this.toggleTriangle(target)
        },
        toggleTriangle: function (target) {
            const top = "glyphicon-triangle-top"
            const bottom = "glyphicon-triangle-bottom"

            this.hideAllTriangles()

            const triangle = target.find("span") // should be 1

            if (triangle.hasClass(top)) {
                triangle.removeClass(top).addClass(bottom)
            } else {
                triangle.removeClass(bottom).addClass(top)
            }
            triangle.removeClass("hidden")
        },
        hideAllTriangles: function () {
            $("[class*=glyphicon-triangle]").each(function () {
                $(this).addClass("hidden")
            })
        }
    })
})