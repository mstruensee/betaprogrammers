define(function (require) {
    const UserModel = require("user/model/UserModel")

    return Backbone.Collection.extend({
        model: UserModel,
        url: "/user/users",
        sort_key: "id",
        sort_desc: false,
        comparator: function (item) {
            return item.get(this.sort_key)
        },
        sortByField: function (fieldName) {
            this.sort_key = fieldName
            this.sort()
            if (this.sort_desc) {
                this.models.reverse()
            }
            this.sort_desc = !this.sort_desc
            //trigger sort at the end because "desc" is handled AFTER the initial sort/sort trigger event
            this.trigger("sort")
        }
    })
})