[#ftl]
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2 col-md-2">
            <div class="panel-group" id="accordion">
            [#include "navbar-organization.ftl"]
            [#include "navbar-product.ftl"]
            [#include "navbar-contract.ftl"]
            [#include "navbar-billing.ftl"]
            [#include "navbar-deduction.ftl"]
            [#include "navbar-claim.ftl"]
            [#include "navbar-experience-refund.ftl"]
            [#include "navbar-accounts-receivable.ftl"]
            [#include "navbar-accounts-payable.ftl"]
            [#include "navbar-account.ftl"]
            [#include "navbar-point-in-time-reserve.ftl"]
            [#include "navbar-acturial-reserve.ftl"]
            [#include "navbar-non-contract-transactions.ftl"]
            [#include "navbar-general-ledger.ftl"]
            [#include "navbar-imaging.ftl"]
            [#if _global.roles?seq_contains("admin")][#include "navbar-admin.ftl"][/#if]
            [#if _global.roles?seq_contains("admin")][#include "navbar-dynamicenum.ftl"][/#if]
            </div>
        </div>
        <div class="col-sm-9 col-md-9">