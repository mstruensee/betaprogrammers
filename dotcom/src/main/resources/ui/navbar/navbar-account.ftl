[#ftl]
[#assign collapseName="Account"]

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse${collapseName?replace(" ", "")}"><span class="glyphicon glyphicon-folder-close"></span><!--${collapseName}-->TODO</a>
        </h4>
    </div>
    <div id="collapse${collapseName?replace(" ", "")}" class="panel-collapse collapse">
        <div class="panel-body">
            <table class="table">
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="#">TODO</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>