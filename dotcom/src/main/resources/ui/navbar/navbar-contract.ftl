[#ftl]
[#assign collapseName="Contract"]

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse${collapseName?replace(" ", "")}"><span class="glyphicon glyphicon-folder-close"></span><!--${collapseName}-->TODO</a>
        </h4>
    </div>
    <div id="collapse${collapseName?replace(" ", "")}" class="panel-collapse collapse">
        <div class="panel-body">
            <table class="table">
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="#">Contract</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="#">Subscribed Coverage</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="#">Linked Assumed - Ceded Coverages</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="#">Activity Tracking</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="#">Legacy Narrative</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="#">Annualized Premium Line</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="#">Plan Annualized Premium Line</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="#">Plan Income Statement Line</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="#">Billing Service Date Reset</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>