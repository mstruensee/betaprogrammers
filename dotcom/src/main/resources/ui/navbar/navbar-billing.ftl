[#ftl]
<!-- Add all components ("module") here, everything else is taken care of for you-->
[#assign subMenu = ["Census"]]

<!-- Display Logic -->
[#assign menu = "TODO"]
[#assign subMenuIn=false]

[#list subMenu as sub]
    [#if sub?replace(" ", "")?lower_case == module?replace(" ", "")?lower_case]
        [#assign subMenuIn=true]
    [/#if]
[/#list]

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse${menu?replace(" ", "")}"><span class="glyphicon glyphicon-folder-close"></span>${menu}</a>
        </h4>
    </div>

    <div id="collapse${menu?replace(" ", "")}" class="panel-collapse collapse[#if subMenuIn] in[/#if]">
        <div class="panel-body">
            <table class="table">
            [#list subMenu as sub]
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-pencil text-primary[#if module?replace(" ", "")?lower_case=sub] active[/#if]"></span>
                        <a href="/${sub?replace(" ", "")?lower_case}">${sub}</a>
                    </td>
                </tr>
            [/#list]
            </table>
        </div>
    </div>
</div>