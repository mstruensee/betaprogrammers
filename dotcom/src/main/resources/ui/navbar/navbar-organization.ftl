[#ftl]
[#assign collapseName="Organization"]

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse${collapseName?replace(" ", "")}"><span class="glyphicon glyphicon-folder-close"></span><!--${collapseName}-->TODO</a>
        </h4>
    </div>
    <div id="collapse${collapseName?replace(" ", "")}" class="panel-collapse collapse">
        <div class="panel-body">
            <table class="table">
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-pencil text-primary"></span><a href="#">Company</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-flash text-success"></span><a href="#">Activity tracking</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-file text-info"></span><a href="#">Letter of Credit</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-comment text-success"></span><a href="#">Contact</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-comment text-success"></span><a href="#">Company relationship</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-comment text-success"></span><a href="#">Import NAIC file</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>