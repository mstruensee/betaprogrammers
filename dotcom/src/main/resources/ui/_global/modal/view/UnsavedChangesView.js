define(function (require) {
    const Global = require("_global/global")
    const UnsavedChangesTemplate = require("text!_global/modal/template/unsaved-changes-template.dust")

    return Backbone.View.extend({
        el: "unsaved-changes-view",
        C: {
            MODAL_UNSAVED_CHANGES: "#modal-unsaved-changes",
            MODAL_UNSAVED_CHANGES_BACKDROP: "modal-unsaved-changes-backdrop"
        },
        events: {
            "click .btn-yes": "yes",
            "click .btn-no": "no"
        },
        initialize: function () {
            //this is a modal, we want it to be at the BODY so other css does not effect its style
            //and so we can force z-index overlapping easily due to having the same parent element
            // ... the modal-backdrop is added to the body by default.
            if (!this.$el.length) {
                $("body").append("<" + this.$el.selector + ">")
                this.$el = $(this.$el.selector)
                //as a result of this, we need to delegate the events after the $el is set...
                this.delegateEvents()
            }
            this.render()
        },
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, UnsavedChangesTemplate, null))
            this.show()
        },
        show: function () {
            const modal = $(this.C.MODAL_UNSAVED_CHANGES)
            modal.on("show.bs.modal", function () {
                const zIndex = 1040 + (10 * $(".modal:visible").length)
                $(this.C.MODAL_UNSAVED_CHANGES).css("z-index", zIndex)

                setTimeout(function () {
                    $(".modal-backdrop").not(".modal-backdrop-stacked").css("z-index", zIndex - 1).addClass("modal-backdrop-stacked").attr("id", this.C.MODAL_UNSAVED_CHANGES_BACKDROP)
                }.bind(this), 0)

                $(this).unbind("show.bs.modal")
            }.bind(this))

            modal.modal("show")
        },
        yes: function () {
            this.closeModalAndBackdrop(true)
        },
        no: function () {
            this.closeModalAndBackdrop(false)
        },
        closeModalAndBackdrop: function (removeParent) {
            const modal = $(this.C.MODAL_UNSAVED_CHANGES)
            modal.on("hidden.bs.modal", function () {
                this.$el.html("")
                this.$el.unbind()
                if (removeParent) {
                    this.model.closeModalAndBackdrop()
                }
                $(".modal-backdrop").not(".modal-backdrop-stacked").remove()
                $("body").addClass("modal-open")//this is a 2nd open popup, so we want to keep this class on the body to keep hiding the scroll.
            }.bind(this))
            modal.modal("hide")
        }
    })
})