define(function (require) {
    const Global = require("_global/global")
    var SpinnerTemplate = require("text!_global/spinner/template/spinner-template.dust")

    return Backbone.View.extend({
        //https://codepen.io/collection/HtAne/ - other css spinners
        el: "spinner",
        initialize: function () {
            $("head").append("<link rel='stylesheet' href='/ui/_global/spinner/css/spinner.css' type='text/css' />")

            if (!this.$el.length) {
                $("body").append("<" + this.$el.selector + ">")
                this.$el = $(this.$el.selector)
                this.delegateEvents()
            }
        },
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, SpinnerTemplate, null))
        },
        start: function () {
            this.spin()
        },
        spin: function () {
            this.render()
        },
        stop: function () {
            clearTimeout(this.timeout)
            this.remove()
            $("overlay").remove()
            $("link[href*=spinner]").attr("disabled", "disabled")
        }
    })
})
