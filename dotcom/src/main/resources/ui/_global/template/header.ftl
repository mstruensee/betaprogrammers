[#ftl]
<html>
<head>
    <title>[#if module??]${module?capitalize}[#else]${_global.applicationName}[/#if]</title>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/webjars/bootstrap-select/1.12.0/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/webjars/bootstrap-tagsinput/0.8.0/dist/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="/webjars/bootstrap-tagsinput/0.8.0/dist/bootstrap-tagsinput-typeahead.css">
    <link rel="stylesheet" href="/webjars/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="/ui/_global/css/header.css">
    <link rel="stylesheet" href="/ui/_global/css/footer.css">
    <link rel="stylesheet" href="/ui/_global/css/navbar.css">
    <link rel="stylesheet" href="/ui/_global/modal/css/unsaved-changes.css">
    <link rel="stylesheet" href="/ui/_global/css/default.css">

[#if css??]
    [#list css?split(",") as stylesheet]
        <link rel="stylesheet" href="${stylesheet}">
    [/#list]
[/#if]

[#if js??]
    [#list js?split(",") as script]
        <script src="${script}"></script>
    [/#list]
[/#if]

[#if module??]
    <script>const module = "${module}/Main";</script>
    <script data-main="/ui/_global/requireConfig" src="/webjars/requirejs/2.2.0/require.min.js"></script>
[/#if]

</head>
<body>
[#include "../../navbar/navbar.ftl"]
<div class="container">