define(function (require) {
    const Global = require("_global/global")
    const Backbone = require("backbone")
    const SearchCollectionTemplate = require("text!_global/search/template/search-collection-template.dust")

    return Backbone.View.extend({
        createEnabled: true,
        el: "search-collection",
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, SearchCollectionTemplate, {createEnabled: this.createEnabled.toString()}))
        },
        events: {
            "keyup .input-group-search-box": "search"
        },
        search: function () {
            console.log("search is not implemented.")
        },
        filter: function (query) {
            if (query === "") {
                return this.collection.models
            } else {
                const models = []
                $.each(this.collection.models, function (index, model) {
                    $.each(model.attributes, function (field, value) {
                        if (field !== "id") {
                            if (typeof value === "string") {
                                if (value.toString().indexOf(query) > -1) {
                                    models.push(model)
                                    return false
                                }
                            }
                            //console.log(field + " -> " + JSON.stringify(value))
                            //TODO handle other scenarios like Object, boolean
                        }
                    }.bind(this))
                }.bind(this))
                return models
            }
        }
    })
})