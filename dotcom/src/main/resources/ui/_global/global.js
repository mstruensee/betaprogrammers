define(function (require) {
    var dust = require("dust")
    require("dust-helpers") //simply requiring this is enough to make it available to dust

    /*
     * This is intended for developer use to see the actual value of anything within a dust template
     * Usage: {@log x=OBJECT /}
     * The passed item (x in above example) will be within the argument params
     */
    dust.helpers.log = function (chunk, context, bodies, params) {
        console.debug(params)
        return chunk.write("")
    }

    dust.helpers.booleanAsString = function (chunk, context, bodies, params) {
        if (context.resolve(params.key) === true) {
            return "True"
        }
        return "False"
    }

    dust.helpers.random = function (chunk, context, bodies, params) {
        return Math.floor((Math.random() * 1000) + 1)
    }

    /*
     * Dust partials are resolved at runtime, not compile time. So, we need to tell dust what to do when loading a template
     * Partials are loaded through here, so we can say to require in the template html, and pass that to the callback to get compiled
     *
     * http://stackoverflow.com/questions/29304979/compiling-and-rendering-complex-dust-js-templates-on-the-client
     */
    dust.onLoad = function (templateName, callback) {
        require(["text!" + templateName + ".dust"], function (templateHtml) {
            callback(null, templateHtml)
        })
    }

    return {
        renderTemplate: function renderTemplate(templateName, templateHtml, data) {

            if (arguments.length != 3) {
                alert("BARF. You must call renderTemplate with 3 arguments\n\nrenderTemplate(templateHtml, templateName, data)")
                return
            }

            if (!dust.cache[templateName]) {
                dust.loadSource(dust.compile(templateHtml, templateName))
            }

            var renderedHtml = ""
            dust.render(templateName, data, function (error, out) {
                if (error) {
                    console.error(error)
                }
                renderedHtml = out
            })

            return renderedHtml
        },
        renderTemplateSynchronously: function renderTemplate(templateName, templateHtml, data, successCallback) {

            if (arguments.length != 4) {
                alert("BARF. You must call renderTemplateSynchronously with 4 arguments\n\nrenderTemplate(templateHtml, templateName, data)")
                return
            }

            if (!dust.cache[templateName]) {
                dust.loadSource(dust.compile(templateHtml, templateName))
            }

            var renderedHtml = ""
            dust.render(templateName, data, function (error, out) {
                if (error) {
                    console.error(error)
                }
                renderedHtml = out
                successCallback(renderedHtml)
            })

            return renderedHtml
        }
    }
})
