var webjars = {
    versions: {
        "jquery": "1.11.1",
        "bootstrap": "3.3.7",
        "bootstrap-select": "1.12.0",
        "bootstrap-notify": "3.1.3",
        "bootstrap-3-typeahead": "4.0.2",
        "typeahead.js": "0.11.1",
        "backbonejs": "1.3.2",
        "bootstrap-tagsinput": "0.8.0",
        "underscorejs": "1.8.3",
        "dustjs-linkedin": "2.7.2",
        "dustjs-helpers": "1.7.3",
        "requirejs-text": "2.0.15",
        "chartjs": "2.4.0"
    },
    path: function (webjarid, path) {
        return "/webjars/" + webjarid + "/" + webjars.versions[webjarid] + "/" + path
    }
}

require.config({
    baseUrl: "/ui",

    /*
     * Only shim stuff that do not call define
     *
     * When upgrading lib, check if the below new versions are AMD-compliant and remove shim if so
     * I"m pretty sure that latest Backbone, $, _ and Dust all support AMD
     * Updating Dust from 2.5 to 2.6 has BREAKING CHANGES (@if no longer supported)
     */
    shim: {
        "dust": {
            exports: "dust"
        },
        "bootstrap": ["jquery"],
        "typeahead": ["jquery"],
        "bootstrap-select": ["bootstrap"],
        "bootstrap-tagsinput": ["jquery", "bootstrap", "typeahead"],
        "dust-helpers": ["dust"]
    },
    paths: {
        "dust": webjars.path("dustjs-linkedin", "dist/dust-full.min"),
        "dust-helpers": webjars.path("dustjs-helpers", "dist/dust-helpers.min"),
        "jquery": webjars.path("jquery", "jquery.min"),
        "underscore": webjars.path("underscorejs", "underscore-min"),
        "bootstrap": webjars.path("bootstrap", "js/bootstrap.min"),
        "bootstrap-select": webjars.path("bootstrap-select", "js/bootstrap-select.min"),
        "bootstrap-tagsinput": webjars.path("bootstrap-tagsinput", "dist/bootstrap-tagsinput.min"),
        "backbone": webjars.path("backbonejs", "backbone"),
        "notify": webjars.path("bootstrap-notify", "bootstrap-notify.min"),
        "chartjs": webjars.path("chartjs", "dist/Chart.min"),
        "typeahead": webjars.path("bootstrap-3-typeahead", "bootstrap3-typeahead"),
        /*
         * Text is a plugin made by require to be able to require in text files of any extension
         * I use this to be able to use .dust files for templates instead of multi-line strings
         */
        text: webjars.path("requirejs-text", "text")
    }
})

if (module) {
    //if (module.indexOf("login") !== -1) {
    //    require(["notify", "_global/js/BAM"], function (z, BAM) {
    //        window.BAM = BAM
    //        require([module])
    //    })
    //}
    //else {
    require(["bootstrap", "bootstrap-select", "bootstrap-tagsinput", "backbone", "notify", "_global/js/BAM"], function (a, b, c, d, e, BAM) {
        window.BAM = BAM
        require([module])
    })
    //}
}


