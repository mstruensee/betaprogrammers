define(function () {
    return {
        success: function (message) {
            this.window({type: "success", message: message, icon: "glyphicon glyphicon-ok-circle"})
        },

        info: function (message) {
            this.window({type: "info", message: message, icon: "glyphicon glyphicon-info-sign"})
        },

        warning: function (message) {
            this.window({type: "warning", message: message, icon: "glyphicon glyphicon-warning-sign"})
        },

        error: function (message) {
            this.window({type: "danger", message: message, icon: "glyphicon glyphicon-exclamation-sign"})
        },

        window: function (options) {
            $.notify({
                icon: options.icon,
                message: options.message
            }, {
                type: options.type,
                allow_dismiss: true,
                newest_on_top: true,
                mouse_over: "pause",
                z_index: 9999
            })
        }
    }
})
