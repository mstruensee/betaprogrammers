define(function (require) {
    const Global = require("_global/global")
    const DynamicEnumTemplate = require("text!dynamicenum/template/dynamicEnum-template.dust")
    const DynamicEnumCreateView = require("dynamicenum/view/DynamicEnumCreateView")

    return Backbone.View.extend({
        render: function () {
            this.$el.html(Global.renderTemplate(this.model.id, DynamicEnumTemplate, this.model.attributes))
        },
        events: {
            "click .btn-edit": "edit"
        },
        edit: function (event) {
            new DynamicEnumCreateView({collection: this.collection, model: this.model})
        }
    })
})