define(function (require) {
    const Global = require("_global/global")
    const DynamicEnumAppTemplate = require("text!dynamicenum/template/dynamicenum-app-template.dust")
    const DynamicEnumCollection = require("dynamicenum/collection/DynamicEnumCollection")
    const DynamicEnumCollectionView = require("dynamicenum/view/DynamicEnumCollectionView")
    const SearchCollectionView = require("_global/search/view/SearchCollectionView")
    const DynamicEnumCreateView = require("dynamicenum/view/DynamicEnumCreateView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "dynamicenum-app-view",
        events: {
            "click .btn-create": "create"
            //"click .sortable": "sort"
        },
        initialize: function () {
            this.spinner = new SpinnerView()
            this.spinner.start()

            const dynamicEnumCollection = new DynamicEnumCollection()

            const SearchDynamicEnumCollectionView = SearchCollectionView.extend({
                createEnabled: true,
                search: function (event) {
                    const filter = $(event.target).val()
                    new DynamicEnumCollectionView({collection: new DynamicEnumCollection(this.filter(filter))}).render()
                }
            })

            const promise = dynamicEnumCollection.fetch()

            $.when(promise).done(() => {
                this.render()
                new SearchDynamicEnumCollectionView({collection: dynamicEnumCollection}).render()
                new DynamicEnumCollectionView({collection: dynamicEnumCollection}).render()
                this.collection = dynamicEnumCollection
                this.spinner.stop()
            })
        },
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, DynamicEnumAppTemplate, null))
            this.hideAllTriangles()
        },
        create: function () {
            new DynamicEnumCreateView({collection: this.collection})
        },
        sort: function (event) {
            const target = $(event.target)
            this.collection.sortByField(target.data("sort"))
            this.toggleTriangle(target)
        },
        toggleTriangle: function (target) {
            const top = "glyphicon-triangle-top"
            const bottom = "glyphicon-triangle-bottom"

            this.hideAllTriangles()

            const triangle = target.find("span") // should be 1

            if (triangle.hasClass(top)) {
                triangle.removeClass(top).addClass(bottom)
            } else {
                triangle.removeClass(bottom).addClass(top)
            }
            triangle.removeClass("hidden")
        },
        hideAllTriangles: function () {
            $("[class*=glyphicon-triangle]").each(function () {
                $(this).addClass("hidden")
            })
        }
    })
})