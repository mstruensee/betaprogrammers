define(function (require) {
    const DynamicEnumView = require("dynamicenum/view/DynamicEnumView")

    return Backbone.View.extend({
        el: "dynamicenum-collection-view",
        initialize: function () {
            this.listenTo(this.collection, "sync", () => this.render())

            this.listenTo(this.collection, "sort", () => this.render())
        },
        render: function () {
            this.$el.empty()
            this.collection.each((model) => {
                const dynamicEnumView = new DynamicEnumView({collection: this.collection, model: model})
                dynamicEnumView.render()
                this.$el.append(dynamicEnumView.$el)
            })
        }
    })
})

