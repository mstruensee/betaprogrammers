define(function (require) {
    const Global = require("_global/global")
    const DynamicEnumTemplate = require("text!dynamicenum/template/dynamicenum-create-template.dust")
    const DynamicEnumModel = require("dynamicenum/model/DynamicEnumModel")
    const UnsavedChangesView = require("_global/modal/view/UnsavedChangesView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "dynamicenum-create-view",
        C: {
            MODAL_DYNAMIC_ENUM_CREATE: "#modal-dynamicenum-create",
            MODAL_DYNAMIC_ENUM_CREATE_BACKDROP: "modal-dynamicenum-create-backdrop",

            NAME: {
                MODEL_VAR: "name",
                SELECTOR: "#name"
            },
            VALUES: {
                MODEL_VAR: "values",
                SELECTOR: "#values"
            },
            SERVER_ERROR: "Please contact administrator."
        },
        events: {
            "click .btn-save-dynamicenum": "save",
            "click .btn-save-dynamicenum-cancel": "cancel",

            "propertychange #name": "validateName",
            "keyup #name": "validateName",
            "input #name": "validateName",
            "paste #name": "validateName",
            "change #name": "validateName",

            "itemAdded #values": "validateValues",
            "itemRemoved #values": "validateValues"

        },
        initialize: function (options) {
            this.spinner = new SpinnerView()
            this.spinner.start()
            //this is a modal, we want it to be at the BODY so other css does not effect its style
            //and so we can force z-index overlapping easily due to having the same parent element
            // ... the modal-backdrop is added to the body by default.
            if (!this.$el.length) {
                $("body").append("<" + this.$el.selector + ">")
                this.$el = $(this.$el.selector)
                //as a result of this, we need to delegate the events after the $el is set...
                this.delegateEvents()
            }

            this.data = {}
            this.collection = options.collection
            if (!this.model) this.model = new DynamicEnumModel()

            const promise = $.ajax({
                url: "/dynamicenum/initialize",
                type: "GET"
            })
            promise.done((response) => this.data.available = response.values)
            promise.fail(() => this.data.available = [])
            $.when(promise).done(() => this.render())
        },
        render: function () {
            //if (this.model.get("id")) $.each(this.model.attributes, (key, value) => this.data[key] = value)
            this.$el.html(Global.renderTemplate(this.$el.selector, DynamicEnumTemplate, this.model.attributes))

            //render tags right after el rendered, if you try to do validation on the dom, it will initialize it with defaults
            this.renderTags()

            //apply initial validation
            this.validateName()
            this.validateValues()

            this.disable()

            this.show()
        },
        show: function () {
            const modal = $(this.C.MODAL_DYNAMIC_ENUM_CREATE)
            modal.on("shown.bs.modal", () => {
                const focus = $("#name")
                focus.focus()
                if (focus.val()) focus[0].setSelectionRange(focus.val().length, focus.val().length)
            })

            modal.on("show.bs.modal", () => {
                const zIndex = 1040 + (10 * $(".modal:visible").length)
                $(this.C.MODAL_DYNAMIC_ENUM_CREATE).css("z-index", zIndex)

                setTimeout(() => $(".modal-backdrop").not(".modal-backdrop-stacked").css("z-index", zIndex - 1).addClass("modal-backdrop-stacked").attr("id", this.C.MODAL_DYNAMIC_ENUM_CREATE_BACKDROP), 0)

                $(this).unbind("show.bs.modal")
            })

            this.spinner.stop()

            modal.modal("show")
        },
        renderTags: function () {
            const values = $(this.C.VALUES.SELECTOR)

            values.tagsinput({
                itemValue: "id",
                itemText: "value",
                tagClass: "label label-primary",
                typeahead: {
                    displayKey: "value",
                    source: this.data.available,
                    afterSelect: function () {
                        this.$element[0].value = ""
                    }
                },
                freeInput: false
            })

            if (this.model.get("id")) {
                $.each(this.model.get(this.C.VALUES.MODEL_VAR), function (index, value) {
                    values.tagsinput("add", value)
                })
            }
        },
        save: function (event) {
            const btnSaveDynamicEnum = $(event.target)
            const disabledItems = $("#modal-dynamicenum-create [class*=btn],#modal-dynamicenum-create input")
            const tags = $(".bootstrap-tagsinput")

            if (this.valid()) {
                btnSaveDynamicEnum.button("loading")
                disabledItems.prop("disabled", true)
                tags.addClass("disabled")

                let verb = "created"
                if (this.model.get("id")) verb = "updated"

                this.model.set(this.C.NAME.MODEL_VAR, $(this.C.NAME.SELECTOR).val())
                this.model.set(this.C.VALUES.MODEL_VAR, $(this.C.VALUES.SELECTOR).tagsinput("items"))

                const promise = this.model.save()
                promise.done((response) => {
                    this.collection.add(response)
                    BAM.success("Dynamic enum successfully " + verb + ".")
                    this.cancel()
                    btnSaveDynamicEnum.button("reset")
                    disabledItems.prop("disabled", false)
                })
                promise.fail((response) => {
                    if (response.status === 409) {
                        $(response.responseJSON.selector).addClass("has-error")
                        BAM.error(response.responseJSON.bam)

                        btnSaveDynamicEnum.button("reset")
                        disabledItems.prop("disabled", false)
                        tags.removeClass("disabled")

                        this.disable()
                    } else {
                        btnSaveDynamicEnum.button("reset")
                        disabledItems.prop("disabled", false)
                        tags.removeClass("disabled")
                        BAM.error(this.C.SERVER_ERROR)
                    }
                })
            }
        },
        cancel: function () {
            if (this.changed()) this.unsavedChanges()
            else this.closeModalAndBackdrop()

        },
        closeModalAndBackdrop: function () {
            const modal = $(this.C.MODAL_DYNAMIC_ENUM_CREATE)
            modal.on("hidden.bs.modal", () => {
                this.$el.html("")
                this.$el.unbind()
                $("#" + this.C.MODAL_DYNAMIC_ENUM_CREATE_BACKDROP).remove()
            })
            modal.modal("hide")
        },
        changed: function () {
            if (!this.model.get("id")) return false
            const model = this.model.get(this.C.VALUES.MODEL_VAR)
            const values = $(this.C.VALUES.SELECTOR).tagsinput("items")

            return $(this.C.NAME.SELECTOR).val() !== this.model.get(this.C.NAME.MODEL_VAR)
                || JSON.stringify(model) !== JSON.stringify(values)
        },
        valid: function () {
            const hasError = $(this.C.MODAL_DYNAMIC_ENUM_CREATE).find(".has-error")
            if (this.model.get("id")) return !hasError.length && this.changed()

            return !hasError.length
        },
        disable: function () {
            const save = $(".btn-save-dynamicenum")
            if (this.valid()) save.prop("disabled", false)
            else save.prop("disabled", true)
        },
        validateName: function (event) {
            if (event && event.which == 13) $(".btn-save-dynamicenum").click()
            else {
                const name = $(this.C.NAME.SELECTOR)
                if (name.val() == "" && name.hasClass("required")) name.parents(".form-group-name").addClass("has-error").removeClass("has-success")
                else name.parents(".form-group-name").removeClass("has-error").addClass("has-success")

                this.disable()
            }
        },
        validateValues: function (event) {
            //enter key is used to add the tags, so we dont want to submit the form here
            //if (event && event.which == 13) $(".btn-save-dynamicenum").click()
            //else {
            const values = $(this.C.VALUES.SELECTOR)
            if (values.tagsinput("items").length == "" && values.hasClass("required")) values.parents(".form-group-values").addClass("has-error").removeClass("has-success")
            else values.parents(".form-group-values").removeClass("has-error").addClass("has-success")

            this.disable()
            //}
        },
        unsavedChanges: function () {
            new UnsavedChangesView({model: this})
        }
    })
})