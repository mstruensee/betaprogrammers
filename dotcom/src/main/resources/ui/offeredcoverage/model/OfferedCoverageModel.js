define(function (require) {
    return Backbone.Model.extend({
        urlRoot: "/offeredcoverage"
    })
})