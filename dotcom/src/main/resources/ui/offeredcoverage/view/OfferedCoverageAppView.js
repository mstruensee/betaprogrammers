define(function (require) {
    const Global = require("_global/global")
    const OfferedCoverageAppTemplate = require("text!offeredcoverage/template/offeredcoverage-app-template.dust")
    const OfferedCoverageCollection = require("offeredcoverage/collection/OfferedCoverageCollection")
    const OfferedCoverageCollectionView = require("offeredcoverage/view/OfferedCoverageCollectionView")
    const SearchCollectionView = require("_global/search/view/SearchCollectionView")
    const OfferedCoverageCreateView = require("offeredcoverage/view/OfferedCoverageCreateView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "offeredcoverage-app-view",
        events: {
            "click .btn-create": "create"
            //"click .sortable": "sort"
        },
        initialize: function () {
            this.spinner = new SpinnerView()
            this.spinner.start()

            const offeredCoverageCollection = new OfferedCoverageCollection()

            const SearchOfferedCoverageCollectionView = SearchCollectionView.extend({
                createEnabled: true,
                search: function (event) {
                    const filter = $(event.target).val()
                    new OfferedCoverageCollectionView({collection: new OfferedCoverageCollection(this.filter(filter))}).render()
                }
            })

            const promise = offeredCoverageCollection.fetch()

            $.when(promise).done(() => {
                this.render()
                new SearchOfferedCoverageCollectionView({collection: offeredCoverageCollection}).render()
                new OfferedCoverageCollectionView({collection: offeredCoverageCollection}).render()
                this.collection = offeredCoverageCollection
                this.spinner.stop()
            })
        },
        render: function () {
            this.$el.html(Global.renderTemplate(this.$el.selector, OfferedCoverageAppTemplate, null))
            this.hideAllTriangles()
        },
        create: function () {
            new OfferedCoverageCreateView({collection: this.collection})
        },
        sort: function (event) {
            const target = $(event.target)
            this.collection.sortByField(target.data("sort"))
            this.toggleTriangle(target)
        },
        toggleTriangle: function (target) {
            const top = "glyphicon-triangle-top"
            const bottom = "glyphicon-triangle-bottom"

            this.hideAllTriangles()

            const triangle = target.find("span") // should be 1

            if (triangle.hasClass(top)) {
                triangle.removeClass(top).addClass(bottom)
            } else {
                triangle.removeClass(bottom).addClass(top)
            }
            triangle.removeClass("hidden")
        },
        hideAllTriangles: function () {
            $("[class*=glyphicon-triangle]").each(function () {
                $(this).addClass("hidden")
            })
        }
    })
})