define(function (require) {
    const Global = require("_global/global")
    const OfferedCoverageTemplate = require("text!offeredcoverage/template/offeredCoverage-template.dust")
    const OfferedCoverageCreateView = require("offeredcoverage/view/OfferedCoverageCreateView")

    return Backbone.View.extend({
        render: function () {
            this.$el.html(Global.renderTemplate(this.model.id, OfferedCoverageTemplate, this.model.attributes))
        },
        events: {
            "click .btn-edit": "edit"
        },
        edit: function (event) {
            new OfferedCoverageCreateView({collection: this.collection, model: this.model})
        }
    })
})