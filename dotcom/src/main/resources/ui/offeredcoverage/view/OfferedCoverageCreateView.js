define(function (require) {
    const Global = require("_global/global")
    const OfferedCoverageTemplate = require("text!offeredcoverage/template/offeredcoverage-create-template.dust")
    const StepOneTemplate = require("text!offeredcoverage/template/offeredcoverage-step-1-template.dust")
    const StepTwoTemplate = require("text!offeredcoverage/template/offeredcoverage-step-2-template.dust")
    const OfferedCoverageModel = require("offeredcoverage/model/OfferedCoverageModel")
    const UnsavedChangesView = require("_global/modal/view/UnsavedChangesView")
    const SpinnerView = require("_global/spinner/view/SpinnerView")

    return Backbone.View.extend({
        el: "offeredcoverage-create-view",
        currentView: 1,
        views: 2,
        C: {
            MODAL_DYNAMIC_ENUM_CREATE: "#modal-offeredcoverage-create",
            MODAL_DYNAMIC_ENUM_CREATE_BACKDROP: "modal-offeredcoverage-create-backdrop",

            NAME: {
                MODEL_VAR: "name",
                SELECTOR: "#name"
            },
            COVERAGE_CATEGORY: {
                MODEL_VAR: "coverageCategory",
                SELECTOR: "#coverageCategory"
            },
            SERVER_ERROR: "Please contact administrator."
        },
        events: {
            "click .btn-save-offeredcoverage": "save",
            "click .btn-next-offeredcoverage": "next",
            "click .btn-previous-offeredcoverage": "previous",
            "click .btn-cancel-offeredcoverage": "cancel",

            "propertychange #coverageCategory": "validateCoverageCategory",
            "keyup #coverageCategory": "validateCoverageCategory",
            "input #coverageCategory": "validateCoverageCategory",
            "paste #coverageCategory": "validateCoverageCategory",
            "change #coverageCategory": "validateCoverageCategory"
        },
        initialize: function (options) {
            this.spinner = new SpinnerView()
            this.spinner.start()
            //this is a modal, we want it to be at the BODY so other css does not effect its style
            //and so we can force z-index overlapping easily due to having the same parent element
            // ... the modal-backdrop is added to the body by default.
            if (!this.$el.length) {
                $("body").append("<" + this.$el.selector + ">")
                this.$el = $(this.$el.selector)
                //as a result of this, we need to delegate the events after the $el is set...
                this.delegateEvents()
            }

            this.data = {}
            this.collection = options.collection
            if (!this.model) this.model = new OfferedCoverageModel()

            const promise = $.ajax({
                url: "/offeredcoverage/initialize",
                type: "GET"
            })
            promise.done((response) => this.data.coverageCategoryEnum = response.coverageCategoryEnum)
            $.when(promise).done(() => this.render())
        },
        render: function () {
            //with linear workflow, most of the render/validation stuff is tied to each "step", not in this render
            this.$el.html(Global.renderTemplate(this.$el.selector, OfferedCoverageTemplate, null))
            this.showView(this.currentView)
            this.disable()

            this.show()
        },
        show: function () {
            const modal = $(this.C.MODAL_DYNAMIC_ENUM_CREATE)
            modal.on("shown.bs.modal", () => {
                const focus = $("#name")
                focus.focus()
                if (focus.val()) focus[0].setSelectionRange(focus.val().length, focus.val().length)
            })

            modal.on("show.bs.modal", () => {
                const zIndex = 1040 + (10 * $(".modal:visible").length)
                $(this.C.MODAL_DYNAMIC_ENUM_CREATE).css("z-index", zIndex)

                setTimeout(() => $(".modal-backdrop").not(".modal-backdrop-stacked").css("z-index", zIndex - 1).addClass("modal-backdrop-stacked").attr("id", this.C.MODAL_DYNAMIC_ENUM_CREATE_BACKDROP), 0)

                $(this).unbind("show.bs.modal")
            })

            this.spinner.stop()

            modal.modal("show")
        },
        showView: function () {
            const view = $("linear-workflow-view")
            let template

            switch (this.currentView) {
                case 1:
                    template = StepOneTemplate
                    break;
                case 2:
                    template = StepTwoTemplate
                    break;
                default:
                    console.log("Step " + this.currentView + " not implemented")
            }
            $.each(this.model.attributes, (key, value) => this.data[key] = value)
            view.empty()
            view.html(Global.renderTemplate(this.currentView, template, this.data))
            this.validateView()

            //render select pickers
            this.renderSelectPickers()

            this.setupButtons()
        },
        validateView: function () {
            switch (this.currentView) {
                case 1:
                    this.validateCoverageCategory()
                    break;
                case 2:
                    break;
                default:
                    console.log("Validate view " + this.currentView + " not implemented")
                    viewNumberFound = false
            }
        },
        setupButtons: function () {
            const next = $(".btn-next-offeredcoverage")
            const previous = $(".btn-previous-offeredcoverage")

            if (this.currentView == this.views) {
                previous.prop("disabled", false)
                next.prop("disabled", true)
            }
            else if (this.currentView == 1) {
                previous.prop("disabled", true)
                next.prop("disabled", false)
            } else {
                next.prop("disabled", false)
                previous.prop("disabled", false)
            }
        },
        next: function () {
            this.currentView++
            this.showView()
        },
        previous: function () {
            this.currentView--
            this.showView()
        },
        save: function (event) {
            const btnSaveOfferedCoverage = $(event.target)
            const disabledItems = $("#modal-offeredcoverage-create [class*=btn],#modal-offeredcoverage-create input")
            const tags = $(".bootstrap-tagsinput")

            if (this.valid()) {
                btnSaveOfferedCoverage.button("loading")
                disabledItems.prop("disabled", true)
                tags.addClass("disabled")

                let verb = "created"
                if (this.model.get("id")) verb = "updated"

                this.model.set(this.C.NAME.MODEL_VAR, $(this.C.NAME.SELECTOR).val())

                const promise = this.model.save()
                promise.done((response) => {
                    this.collection.add(response)
                    BAM.success("Dynamic enum successfully " + verb + ".")
                    this.cancel()
                    btnSaveOfferedCoverage.button("reset")
                    disabledItems.prop("disabled", false)
                })
                promise.fail((response) => {
                    if (response.status === 409) {
                        $(response.responseJSON.selector).addClass("has-error")
                        BAM.error(response.responseJSON.bam)

                        btnSaveOfferedCoverage.button("reset")
                        disabledItems.prop("disabled", false)
                        tags.removeClass("disabled")

                        this.disable()
                    } else {
                        btnSaveOfferedCoverage.button("reset")
                        disabledItems.prop("disabled", false)
                        tags.removeClass("disabled")
                        BAM.error(this.C.SERVER_ERROR)
                    }
                })
            }
        },
        cancel: function () {
            if (this.changed()) this.unsavedChanges()
            else this.closeModalAndBackdrop()
        },
        closeModalAndBackdrop: function () {
            const modal = $(this.C.MODAL_DYNAMIC_ENUM_CREATE)
            modal.on("hidden.bs.modal", () => {
                this.$el.html("")
                this.$el.unbind()
                $("#" + this.C.MODAL_DYNAMIC_ENUM_CREATE_BACKDROP).remove()
            })
            modal.modal("hide")
            this.model = null
        },
        changed: function () {
            if (!this.model.get("id")) return false

            return $(this.C.NAME.SELECTOR).val() !== this.model.get(this.C.NAME.MODEL_VAR)
        },
        valid: function () {
            const hasError = $(this.C.MODAL_DYNAMIC_ENUM_CREATE).find(".has-error")
            if (this.model.get("id")) return !hasError.length && this.changed()

            return !hasError.length
        },
        disable: function () {
            const save = $(".btn-save-offeredcoverage")
            if (this.valid()) save.prop("disabled", false)
            else save.prop("disabled", true)
        },
        validateCoverageCategory: function (event) {
            if (event && event.which == 13) $(".btn-save-offeredcoverage").click()
            else {
                const field = $(this.C.COVERAGE_CATEGORY.SELECTOR)
                if (field.find("option:selected").val() == "" && field.hasClass("required")) {
                    field.parents(".form-group-coverageCategory").addClass("has-error").removeClass("has-success")
                    this.model.set(this.C.COVERAGE_CATEGORY.MODEL_VAR, null)
                    console.log("COVERAGE_CATEGORY has-error")
                }
                else {
                    field.parents(".form-group-coverageCategory").removeClass("has-error").addClass("has-success")
                    this.model.set(this.C.COVERAGE_CATEGORY.MODEL_VAR, {"id": $(this.C.COVERAGE_CATEGORY.SELECTOR).find(":selected").attr("data-tokens"), "value": $(this.C.COVERAGE_CATEGORY.SELECTOR).find(":selected").val()})
                    console.log("COVERAGE_CATEGORY has-error")
                }
                console.log("COVERAGE_CATEGORY model_var: " + this.model.get(this.C.COVERAGE_CATEGORY.MODEL_VAR))
                this.disable()
            }
        },
        renderSelectPickers: function () {
            $("[class^=selectpicker]").selectpicker()
        },
        unsavedChanges: function () {
            new UnsavedChangesView({model: this})
        }
    })
})