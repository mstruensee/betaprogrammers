define(function (require) {
    const OfferedCoverageView = require("offeredcoverage/view/OfferedCoverageView")

    return Backbone.View.extend({
        el: "offeredcoverage-collection-view",
        initialize: function () {
            this.listenTo(this.collection, "sync", () => this.render())

            this.listenTo(this.collection, "sort", () => this.render())
        },
        render: function () {
            this.$el.empty()
            this.collection.each((model) => {
                const offeredCoverageView = new OfferedCoverageView({collection: this.collection, model: model})
                offeredCoverageView.render()
                this.$el.append(offeredCoverageView.$el)
            })
        }
    })
})

